{ python3, fetchFromGitHub, ... }:

python3.pkgs.buildPythonApplication rec {
  pname = "vcardtool";
  version = "dev";
  src = fetchFromGitHub {
    owner = "jakeogh";
    repo = pname;
    rev = "f9d98c1baeaca06b9e13d8d94daf5e5f42a8840d";
    hash  = "sha256-3TCC68woelY33b0SqhHxJjwS29ozDJ3/pGNsqXCNGhU=";
  };
  propagatedBuildInputs = [ python3.pkgs.click ];
}
