# Generate zeal docsets from a html folder with dashing
{
  buildGoModule,
  fetchFromGitHub,
  runCommand,
  writeText,
  lib,
}
: let
  name = "dashing";
  version = "0.4.0";
  dashing = buildGoModule {
    pname = name;
    version = version;
    src = fetchFromGitHub {
      owner = "technosophos";
      repo = name;
      rev = version;
      hash = "sha256-CcEgGPnJGrTXrgo82u5dxQTB/YjFBhHdsv7uggsHG1Y=";
    };
    vendorHash = "sha256-XeUFmzf6y0S82gMOzkj4AUNFkVvkVOwauYpqY4jeWLM=";
    passthru = {inherit buildDocSet;};
  };
  buildDocSet = {
    src,
    name,
    package,
    index ? "index.html",
    selectors ? {},
    ignore ? ["ABOUT"],
    allowJS ? false,
    ExternalURL ? "",
    icon32x32 ? "",
    preBuild ? "",
    ...
  } @ args: let
    # these are the json keys for the dashing config file
    config-data = {inherit name package index selectors ignore allowJS ExternalURL icon32x32;};
    config-file = writeText "dashing.json" (builtins.toJSON config-data);
    env = lib.attrsets.filterAttrs (n: v: ! builtins.elem n ((builtins.attrNames config-data) ++ ["src"])) args;
  in
    runCommand "${package}.docset" env ''
      rm *
      test -d "${src}"
      cp -r ${src}/* .
      ${preBuild}
      ${dashing}/bin/dashing build --config ${config-file}
      mv ${package}.docset $out
    '';
in
  dashing
