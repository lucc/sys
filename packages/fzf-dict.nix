{ dict
, dictdDBs
, lib
, ncurses
, runCommand
, writeShellApplication
, dictionaries ? null
}:

let
  dicts = if dictionaries != null
    then dictionaries
    else builtins.filter lib.attrsets.isDerivation (builtins.attrValues dictdDBs);
  words = runCommand "dictd-keywords-list" {} ''
    cat ${lib.strings.concatMapStringsSep " " (d: "${d}/share/dictd/*.index") dicts} \
      | cut -f 1 -d $'\t' \
      | grep -v '^\s*$' \
      | sort -u > $out
  '';
in
  writeShellApplication {
    name = "dict";
    text = ''
      c=$(($(${ncurses}/bin/tput cols) / 2 - 5))
      q=$(xclip -selection primary -out || true)
      if [[ "''${#q}" -gt 0 ]]; then
        opts=( --query="$q" )
      else
        opts=()
      fi
      if [ $# -eq 0 ]; then
        fzf --preview "${dict}/bin/dict {} | fmt --width=$c" "''${opts[@]}" < ${words} \
          | xargs --no-run-if-empty ${dict}/bin/dict
      else
        exec ${dict}/bin/dict "$@"
      fi
    '';
  }
