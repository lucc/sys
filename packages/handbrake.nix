{
  lib,
  handbrake,
  util-linux,
  writeShellApplication,
}: let
  handbrake' = lib.getExe handbrake;
in
  writeShellApplication {
    name = "handbrake";
    text = ''
      extract=( --main-feature )
      case $1 in
        -h) echo help; exit;;
        -l) exec ${handbrake'} --title 0 --input /dev/sr0;;
        -t) extract=( --title "''${2?-t needs a numeric argument}" ); shift 2;;
      esac
      ${handbrake'} --input /dev/sr0 "''${extract[@]}" --all-audio --all-subtitles --output "$@"
      ${util-linux}/bin/eject
    '';
  }
