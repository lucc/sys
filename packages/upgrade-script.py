#!/usr/bin/env python3

import argparse
import contextlib
import getpass
import logging
import os
import shlex
import shutil
import subprocess
import sys
import tempfile
from datetime import date, datetime
from os import getpid
from pathlib import Path
from typing import Generator

# some constants to prevent typos
MAIN = "main"
UPDATES = "updates"
PATTERN = "update/[2-9][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]-*"


def run(*args: str | Path, check=True, **kwargs) -> subprocess.CompletedProcess:
    logging.debug("Running command: %s, %s", args, kwargs)
    return subprocess.run(args, check=check, **kwargs)


def su(*args: str | Path, user, **kwargs) -> subprocess.CompletedProcess:
    return run(
        "su", "--command", shlex.join(str(x) for x in args), "--", user, **kwargs
    )


@contextlib.contextmanager
def git_worktree(
    git_dir: Path, base_branch: str, new_branch: str
) -> Generator[Path, None, None]:
    user = git_dir.owner()

    def git(*args: str) -> None:
        su("git", "-C", git_dir, *args, user=user)

    with tempfile.TemporaryDirectory() as dir, contextlib.chdir(dir):
        shutil.chown(dir, user)
        try:
            git("worktree", "add", "--quiet", "-b", new_branch, dir, base_branch)
            yield Path(dir)
        finally:
            git("worktree", "remove", "--force", dir)
            git("branch", "--quiet", "--delete", "--force", new_branch)


def update_action(flake: Path) -> int:
    user: str = flake.owner()
    branch = f"update/{date.today().isoformat()}-{getpid()}"

    def git(*args: str | Path, **kwargs) -> subprocess.CompletedProcess:
        return su("git", *args, user=user, **kwargs)

    def git_out(*args_: str) -> list[str]:
        return (
            git("-C", flake, *args_, capture_output=True).stdout.decode().splitlines()
        )

    def build(ref: str) -> bool:
        if git("diff", "--quiet", UPDATES, ref, check=False).returncode != 1:
            sys.exit()
        if (
            su(
                "nixos-rebuild",
                "build",
                "--flake",
                f"{flake}?ref={ref}",
                check=False,
                user=user,
            ).returncode
            != 0
        ):
            return False
        git("tag", branch + "-build", ref)
        return True

    with git_worktree(flake, UPDATES, branch) as dir:
        logging.debug("Switched to worktree %s", dir)
        exit_status = 0
        try:
            git("merge", "--quiet", "--no-edit", MAIN)
            git("tag", branch + "-merge", branch)
            # update the flake.lock file
            su("nix", "flake", "update", "--commit-lock-file", user=user)
            if not (build(branch) or build(branch + "-merge")):
                sys.exit(1)
            su("nix", "flake", "check", f"{flake}?ref={branch}-build", user=user)
            # install the build result as the latest generation of the system profile
            run(
                "nix-env",
                "--profile",
                "/nix/var/nix/profiles/system",
                "--set",
                dir / "result",
            )
            # switch to the new system and install it's boot entry
            run(dir / "result/bin/switch-to-configuration", "switch")

            git("tag", branch + "-switch", branch + "-build")
            git("switch", "--quiet", UPDATES)
            git("merge", "--quiet", "--no-edit", branch + "-switch")
            if tags := git_out("tag", "--list", PATTERN + "-build", PATTERN + "-merge"):
                git("tag", "--delete", *tags)
            branches = git_out(
                "for-each-ref", "--format=%(refname:short)", "refs/heads/" + PATTERN
            )
            branches = [b for b in branches if b != branch]
            if branches:
                git(
                    "branch",
                    "--quiet",
                    "--delete",
                    "--force",
                    *branches,
                )
        except Exception as e:
            logging.debug("caught exception %s", e)
            exit_status = 1
        finally:
            logging.debug("Cleanup")
            if tags := git_out("tag", "--list", branch + "-merge"):
                git("-C", flake, "tag", "--delete", *tags)
        return exit_status


def reset_action(
    rebase: bool, delete: datetime | None, update: bool, dry_run: bool
) -> None:
    run("git", "switch", MAIN)
    # update all flake inputs
    if update:
        if dry_run:
            run("nix", "flake", "update", "--output-lock-file", "/dev/null")
        else:
            run("nix", "flake", "update", "--commit-lock-file")
    if rebase:
        cmd = ("git", "rebase", "--interactive", MAIN, UPDATES)
        if dry_run:
            logging.info("Would run %s", cmd)
        else:
            out = run(*cmd, env={"EDITOR": "sed -in '/flake.lock: Update/p'"})
            while out.returncode != 0:
                out = run("git", "rebase", "--skip")
        run("git", "switch", MAIN)
    else:
        cmd = ("git", "branch", "--force", UPDATES, MAIN)
        if dry_run:
            logging.info("Would run %s", cmd)
        else:
            run(*cmd)
    # delete old branches and tags
    branches = run(
        "git", "for-each-ref", "--format=%(refname:short)", "refs/heads/" + PATTERN,
        capture_output=True
    ).stdout.splitlines()
    if branches:
        cmd = ("git", "branch", "--delete", "--force", *branches)
        if dry_run:
            logging.info("Would run %s", cmd)
        else:
            run(*cmd)
    if delete:
        tags = run("git", "tag", "--list", PATTERN, capture_output=True).stdout.splitlines()
        tags = [t for t in tags if date(*t.split("/")[1].split("-")[:3]) < delete]
        if tags:
            cmd = ("git", "tag", "--delete", *tags)
            if dry_run:
                logging.info("Would run %s", cmd)
            else:
                run(*cmd)


def parse_command_line() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action="store_true")
    subs = parser.add_subparsers(dest="subcommand", required=True)
    update = subs.add_parser("update")
    update.set_defaults(action=lambda args: update_action(args.flake))
    update.add_argument("--flake", type=lambda x: Path(x).resolve(), required=True)
    reset = subs.add_parser("reset-branches")
    reset.set_defaults(
        action=lambda a: reset_action(a.rebase, a.delete, a.update, a.dry_run)
    )
    reset.add_argument("--update", "-u", action="store_true")
    reset.add_argument(
        "--delete", "-d", type=lambda a: datetime.strptime(a, "%Y-%m-%d")
    )  # TODO better parser
    reset.add_argument("--rebase", "-r", action="store_true")
    reset.add_argument("--dry-run", "-n", action="store_true")
    args = parser.parse_args()
    if args.debug or True:  # FIXME always debugging during development
        logging.basicConfig(level=logging.DEBUG)
    logging.debug("Parsed command line: %s", args)
    if args.subcommand == "update" and os.getuid() != 0:
        parser.error("Please run this command as root.")
    elif (
        args.subcommand == "reset-branches"
        and getpass.getuser() != Path(".git").owner()
    ):
        parser.error("Please run this command as " + Path(".git").owner())
    return args


def main() -> int:
    args = parse_command_line()
    return args.action(args)


if __name__ == "__main__":
    sys.exit(main())
