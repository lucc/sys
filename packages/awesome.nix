{ awesome
, lib
, luaPackages
, awesome-solarized
, lain
}:

awesome.overrideAttrs (old: {
  buildCommand = let mapper = lib.concatMapStringsSep "\n"; in
    ''
      set -euo pipefail
      ${
        # Copy original files, for each split-output (`out`, `dev` etc.).
        # E.g. `${package.dev}` to `$dev`, and so on. If none, just "out".
        # Symlink all files from the original package to here (`cp -rs`),
        # to save disk space.
        # We could alternatively also copy (`cp -a --no-preserve=mode`).
        mapper (outputName: ''
          echo "Copying output ${outputName}"
          cp -rs --no-preserve=mode "${awesome.${outputName}}" "''$${outputName}"
        '')
        (old.outputs or ["out"])
      }
      # Make the file to be not a symlink by full copying using `install` first.
      # This also makes it writable (files in the nix store have `chmod -w`).
      install -v "${awesome}"/bin/awesome "$out"/bin/awesome
      ${ # add several search arguments to the end of the command line
        mapper (path: ''
          echo Appending ${path}... to awesome wrapper
          sed -i -e '$s#"\$@"\s*$#--search ${path}/lua/${awesome.lua.luaversion} "$@"#' "$out"/bin/awesome
        '')
        [
          "${luaPackages.vicious}/lib"
          "${luaPackages.inspect}/share"
          "${lain}/share"
          "${awesome-solarized}/share"
          "${luaPackages.luaposix}/share"
          "${luaPackages.luaposix}/lib"
        ]
      }
    '';
})
