
        # Generated by ansible2nix from file bootstrap/requirements.yml
        [
        (builtins.fetchurl {
            name = "ansible.posix";
            url = "https://galaxy.ansible.com/download/ansible-posix-1.5.4.tar.gz";
            sha256 = "1s8pjacqqp8mcsjhbaxnar8gvss139ih81yw53k58z0f6p4p1jbd";
        })
     
        (builtins.fetchurl {
            name = "community.crypto";
            url = "https://galaxy.ansible.com/download/community-crypto-2.21.1.tar.gz";
            sha256 = "0s90djlckjr5sxrpamb67mkhdwqrgr84a5m4g29agmcz605bxp9z";
        })
     
        (builtins.fetchurl {
            name = "community.general";
            url = "https://galaxy.ansible.com/download/community-general-9.2.0.tar.gz";
            sha256 = "1gawj9452wr6p7z7zpd8g0mxikayv5cz940lv5dldln8ygw9jbkz";
        })
     
        (builtins.fetchurl {
            name = "community.mysql";
            url = "https://galaxy.ansible.com/download/community-mysql-3.9.0.tar.gz";
            sha256 = "0mv24sbfn0zjzhxcvcpy4xw5z696d2hkb69cl8ni09x68mr6nlbq";
        })
    ]
