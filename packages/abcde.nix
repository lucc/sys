{
  abcde,
  writeText,
  writeShellScriptBin,
}: let
  # This file is a bash script, it will be sourced by abcde, see abcde(1)
  config = ''
    EJECTCD=y
    CDDBMETHOD=musicbrainz

    OUTPUTTYPE=flac
    FLACOPTS=--best

    OUTPUTDIR=$HOME/audio
    OUTPUTFORMAT='$OUTPUT/$ARTISTFILE/$ALBUMFILE/$TRACKNUM $TRACKFILE'
    VAOUTPUTFORMAT='$OUTPUT/Various Artists/$ALBUMFILE/$TRACKNUM $TRACKFILE'

    mungefilename ()
    {
      #echo "$@" | sed s,:,\ -,g | tr \ /\* __+ | tr -d \'\"\?\[:cntrl:\]
      #echo "$@" | sed s,:,\ -,g | tr \ / __ | tr -d \'\"\?\[:cntrl:\]
      echo "$@" | tr / : | tr -d \'\"\?\[:cntrl:\]
    }
  '';
in
  # The -c option can be repeated and all config files will be read (during
  # command line parsing already).  This means that specifying a config file
  # here is "non obtrusive".
  writeShellScriptBin "abcde" ''
    ${abcde}/bin/abcde -c ${writeText "abcde-config" config} "$@"
  ''
