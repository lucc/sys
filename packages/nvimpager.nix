{
  neovim,
  self,
  stdenv,
  system,
  vimPlugins,
}:
# We can not inject this version of neovim with overrideAttrs as the
# customRC will break the tests, so we build a new package to get rid of the
# reference to the plain neovim derivation.
let
  upstream = self.inputs.nvimpager.packages.${system}.default;
  nvim = neovim.override {
    configure = {
      customRC = ''
        " my search options
        set ignorecase
        set smartcase
        " add some plugins from the editor
        set runtimepath+=${vimPlugins.vim-pandoc-syntax}
        set runtimepath+=${vimPlugins.nvim-solarized-lua}
        " my normal colour settings
        set background=dark
        colorscheme solarized
        set termguicolors
      '';
    };
    withNodeJs = false;
    withPython3 = false;
    withRuby = false;
  };
in
  stdenv.mkDerivation {
    inherit (upstream) pname version;
    src = upstream;
    buildPhase = ''
      cp -r . $out
      sed -i 's|\(NVIMPAGER_NVIM:-\)/nix/store/.*\(/bin/nvim}\)|\1${nvim}\2|' $out/bin/nvimpager
    '';
  }
