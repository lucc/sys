{ stdenv, fetchFromGitHub, ... }:

stdenv.mkDerivation rec {
  name = "vcard2to3";
  src = fetchFromGitHub {
    owner = "jowave";
    repo = name;
    rev = "a9c93c267dad6dd83396a687e843b22d750dd9f9";
    hash = "sha256-GbNsxIpnS0/HYE0LLA3DXdeb8oBavWlev7oAHYcSqNM=";
  };
  buildPhase = "patchShebangs vcard2to3.py";
  installPhase = "install -D vcard2to3.py $out/bin/vcard2to3";
}
