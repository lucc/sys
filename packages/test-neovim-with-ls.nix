{writeScriptBin}:
writeScriptBin "build-neovim-with-ls" ''
  export NIXPKGS_ALLOW_UNFREE=1
  result=$(nix build --no-link --print-out-paths --impure \
    --expr '
      (builtins.getFlake "'$PWD'").packages.x86_64-linux.neovim.withLS
      (ls: with ls; [ '"$*"' ])
    ')
  $result/bin/nvim
''
