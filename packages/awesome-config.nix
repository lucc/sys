{ stdenv, writeText, lib, awesome
, system-constants
, widgets ? null
}:

let
  system-constants-lua = writeText "system-constants.lua" ''
    return ${lib.generators.toLua {} system-constants}
  '';
in

stdenv.mkDerivation {
  pname = "awesome-config";
  version = "scm-1";

  src = ../awesome;

  dontBuild = true;
  installPhase = ''
    mkdir -p $out
    cp -r $src/* $out
    cp ${system-constants-lua} $out/system-constants.lua
    ${if !isNull widgets then "cp -f ${widgets} $out/widgets.lua" else ""}

    substituteInPlace $out/functions.lua --replace \
      "os.getenv('HOME') .. '/.config/awesome" "'$out"
  '';
  doInstallCheck = true;
  nativeInstallCheckInputs = [ awesome ];
  # do a simpe syntax check
  installCheckPhase = ''
    find "$out" -name '*.lua' -print0 | xargs -0n1 awesome --check --config
  '';
}
