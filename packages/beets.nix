{
  beets,
  beetsPackages,
  writeShellScriptBin,
  writeText,
  keyfinder-cli,
}: let
  package = beets.override {
    pluginOverrides = {
      alternatives = {
        enable = true;
        propagatedBuildInputs = [beetsPackages.alternatives];
      };
    };
  };

  # These plugins are listed at http://docs.beets.io/en/latest/plugins/index.html
  used-builtin-plugins = [
    "chroma" # Use acoustic fingerprinting to identify audio files with missing or incorrect metadata.
    "spotify" # Search for releases in the Spotify database.
    "fromfilename" # Guess metadata for untagged tracks from their filenames.
    "edit" # Edit metadata from a text editor.
    "fetchart" # Fetch album cover art from various sources.
    "keyfinder" # Use the KeyFinder program to detect the musical key from the audio.
    "importadded" # Use file modification times for guessing the value for the added field in the database.
    "lastgenre" # Fetch genres based on Last.fm tags.
    "lyrics" # Automatically fetch song lyrics.
    "mbsync" # Fetch updated metadata from MusicBrainz.
    "metasync" # Fetch metadata from local or remote sources
    "mpdstats" # Connect to MPD and update the beets library with play statistics (last_played, play_count, skip_count, rating).
    "parentwork" # Fetch work titles and works they are part of.
    "replaygain" # Calculate volume normalization for players that support it.
    "badfiles" # Check audio file integrity.
    "importfeeds" # Keep track of imported files via .m3u playlist file(s) or symlinks.
    "mpdupdate" # Automatically notifies MPD whenever the beets library changes.
    "play" # Play beets queries in your music player.
    "bareasc" # Search albums and tracks with bare ASCII string matching.
    "convert" # Transcode music and embed album art while exporting to a different directory.
    "duplicates" # List duplicate tracks or albums.
    "export" # Export data from queries to a format.
    "filefilter" # Automatically skip files during the import process based on regular expressions.
    "fuzzy" # Search albums and tracks with fuzzy string matching.
    "hook" # Run a command when an event is emitted by beets.
    "info" # Print music files’ tags to the console.
    "loadext" # Load SQLite extensions.
    "missing" # List missing tracks.
  ];
  unused-builtin-plugins = [
    "absubmit" # Analyse audio with the streaming_extractor_music program and submit the metadata to an AcousticBrainz server
    # deprecated
    "acousticbrainz" # Fetch various AcousticBrainz metadata
    # FIXME this is very slow
    "autobpm" # Use Librosa to calculate the BPM from the audio.
    "bpm" # Measure tempo using keystrokes.
    # FIXME this hangs the startup of beets
    "bpsync" # Fetch updated metadata from Beatport.
    "embedart" # Embed album art images into files’ metadata.
    "ftintitle" # Move “featured” artists from the artist field to the title field.
    "lastimport" # Collect play counts from Last.fm.
    "scrub" # Clean extraneous metadata from music files.
    "zero" # Nullify fields by pattern or unconditionally.
    "albumtypes" # Format album type in path formats.
    "bucket" # Group your files into bucket directories that cover different field values ranges.
    "inline" # Use Python snippets to customize path format strings.
    "rewrite" # Substitute values in path formats.
    "advancedrewrite" # Substitute field values for items matching a query.
    "substitute" # As an alternative to rewrite, use this plugin. The main difference between them is that this plugin never modifies the files metadata.
    "the" # Move patterns in path formats (i.e., move “a” and “the” to the end).
    "aura" # A server implementation of the AURA specification.
    "embyupdate" # Automatically notifies Emby whenever the beets library changes.
    "fish" # Adds Fish shell tab autocompletion to beet commands.
    "ipfs" # Import libraries from friends and get albums from them via ipfs.
    "kodiupdate" # Automatically notifies Kodi whenever the beets library changes.
    "playlist" # Use M3U playlists to query the beets library.
    "plexupdate" # Automatically notifies Plex whenever the beets library changes.
    "smartplaylist" # Generate smart playlists based on beets queries.
    "sonosupdate" # Automatically notifies Sonos whenever the beets library changes.
    "thumbnails" # Get thumbnails with the cover art on your album folders.
    "subsonicupdate" # Automatically notifies Subsonic whenever the beets library changes.
    "bpd" # A music player for your beets library that emulates MPD and is compatible with MPD clients.
    "ihate" # Automatically skip albums and tracks during the import process.
    "mbcollection" # Maintain your MusicBrainz collection list.
    "mbsubmit" # Print an album’s tracks in a MusicBrainz-friendly format.
    "mstream" # A music streaming server + webapp that can be used alongside beets.
    "random" # Randomly choose albums and tracks from your library.
    "spotify" # Create Spotify playlists from the Beets library.
    "types" # Declare types for flexible attributes.
    "web" # An experimental Web-based GUI for beets.
    # needs a login
    "discogs" # Search for releases in the Discogs database.
    # generates many http errors and exceptions
    "deezer" # Search for releases in the Deezer database.
  ];
  # TODO
  interesting-community-plugins = [
    "drop2beets" # Automatically imports singles as soon as they are dropped in a folder (using Linux’s inotify). You can also set a sub-folders hierarchy to set flexible attributes by the way.
  ];
  other-community-plugins = [
    #"beets-alternatives" # Manages external files.
    "beet-amazon" # Adds Amazon.com as a tagger data source.
    #"beets-artistcountry" # Fetches the artist’s country of origin from MusicBrainz.
    #"beets-autofix" # Automates repetitive tasks to keep your library in order.
    "beets-autogenre" # Assigns genres to your library items using the lastgenre and beets-xtractor plugins as well as additional rules.
    "beets-audible" # Adds Audible as a tagger data source and provides other features for managing audiobook collections.
    #"beets-barcode" # Lets you scan or enter barcodes for physical media to search for their metadata.
    "beetcamp" # Enables bandcamp.com autotagger with a fairly extensive amount of metadata.
    #"beetstream" # Server implementation of the Subsonic API specification, serving the beets library and (smartplaylist plugin generated) M3U playlists, allowing you to stream your music on a multitude of clients.
    "beets-bpmanalyser" # Analyses songs and calculates their tempo (BPM).
    "beets-check" # Automatically checksums your files to detect corruption.
    #"beets-copyartifacts" # Helps bring non-music files along during import.
    "beets-describe" # Gives you the full picture of a single attribute of your library items.
    #"dsedivec" # Has two plugins: edit and moveall.
    #"beets-follow" # Lets you check for new albums from artists you like.
    #"beetFs" # Is a FUSE filesystem for browsing the music in your beets library. (Might be out of date.)
    #"beets-goingrunning" # Generates playlists to go with your running sessions.
    #"beets-ibroadcast" # Uploads tracks to the iBroadcast cloud service.
    "beets-importreplace" # Lets you perform regex replacements on incoming metadata.
    "beets-jiosaavn" # Adds JioSaavn.com as a tagger data source.
    "beets-more" # Finds versions of indexed releases with more tracks, like deluxe and anniversary editions.
    #"beets-mosaic" # Generates a montage of a mosaic from cover art.
    "beets-mpd-utils" # Plugins to interface with MPD. Comes with mpd_tracker (track play/skip counts from MPD) and mpd_dj (auto-add songs to your queue.)
    #"beets-noimport" # Adds and removes directories from the incremental import skip list.
    "beets-originquery" # Augments MusicBrainz queries with locally-sourced data to improve autotagger results.
    #"beets-plexsync" # Allows you to sync your Plex library with your beets library, create smart playlists in Plex, and import online playlists (from services like Spotify) into Plex.
    #"beets-setlister" # Generate playlists from the setlists of a given artist.
    "beet-summarize" # Can compute lots of counts and statistics about your music library.
    "beets-usertag" # Lets you use keywords to tag and organize your music.
    #"beets-webm3u" # Serves the (smartplaylist plugin generated) M3U playlists via HTTP.
    #"beets-webrouter" # Serves multiple beets webapps (e.g. web, beets-webm3u, beetstream, aura) using a single command/process/host/port, each under a different path.
    "whatlastgenre" # Fetches genres from various music sites.
    "beets-xtractor" # Extracts low- and high-level musical information from your songs.
    "beets-ydl" # Downloads audio from youtube-dl sources and import into beets.
    #"beets-ytimport" # Download and import your liked songs from YouTube into beets.
    #"beets-yearfixer" # Attempts to fix all missing original_year and year fields.
    "beets-youtube" # Adds YouTube Music as a tagger data source.
  ];

  config = {
    import = {
      write = false;
      copy = false;
      move = false;
      link = false;
      hardlink = false;
      log = "beets-import.log";
      languages = "de en";
      timid = true;
    };
    plugins = used-builtin-plugins;
    keyfinder.bin = "${keyfinder-cli}/bin/keyfinder-cli";
  };

  config-file = writeText "beets-config.yaml" (builtins.toJSON config);

  package-with-config = writeShellScriptBin "beet" ''
    ${package}/bin/beet --config ${config-file} "$@"
  '';
in {inherit package config package-with-config;}
