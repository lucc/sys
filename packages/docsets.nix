{
  awesome,
  nix,
  nixpkgs-docs,
  runCommand,
  dashing,
  writeScriptBin,
}: let
  inherit (dashing) buildDocSet;

  awesome-ds = buildDocSet {
    src = "${awesome.doc}/share/doc/awesome/doc/";
    name = "Awesome WM";
    package = "awesome";
    icon32x32 = "${awesome}/share/awesome/icons/awesome32.png";
    selectors = {
      dt = "function";
      h1 = "module";
    };
  };

  nix-ds = buildDocSet {
    src = "${nix.doc}/share/doc/nix/manual";
    name = "Nix";
    package = "nix";
    selectors = {
      h1 = "section";
      h2 = "section";
      h3 = "section";
      "dt a code" = "function";
    };
    preBuild = "rm  print.html";
    ignore = ["print.html"];
  };

  nixpkgs-ds = buildDocSet {
    src = "${nixpkgs-docs}/share/doc/nixpkgs";
    name = "Nixpkgs";
    package = "nixpkgs";
    index = "manual.html";
    selectors = {
      h1 = "section";
      h2 = "section";
      h3 = "section";
    };
  };

  nixos-ds = buildDocSet {
    src = "";
    name = "NixOS";
    package = "nixos";
  };
  #ln -s ${nixos-ds} $out/nixos.docset

  docsets =
    runCommand "docsets" {
      passthru = {
        awesome = awesome-ds;
        nix = nix-ds;
        nixpkgs = nixpkgs-ds;
      };
    } ''
      mkdir $out
      ln -s ${awesome-ds} $out/awesome.docset
      ln -s ${nix-ds} $out/nix.docset
      ln -s ${nixpkgs-ds} $out/nixpkgs.docset
    '';

  install-docsets = writeScriptBin "install-zeal-docsets" ''
    restart=false
    if pgrep zeal; then
      restart=true
      kill $(pgrep zeal)
    fi
    ln -vLfs ${docsets}/* ~/.local/share/Zeal/Zeal/docsets/
    if $restart; then
      (zeal &) &
    fi
  '';
in {inherit docsets install-docsets;}
