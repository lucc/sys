{
  fetchFromGitHub,
  python3,
  runCommand,
}:
runCommand "led-name-badge" {
  name = "badge";
  version = "2024-12-30";
  src = fetchFromGitHub {
    owner = "fossasia";
    repo = "led-name-badge-ls32";
    rev = "a432514db49454b22aabc43bfd5d319b8adcb3c8";
    hash = "sha256-ykuHwKmlVDVt/FqsR90C9FflGEKN+MBF+3MwinUFGio=";
  };
  buildInputs = [
    (python3.withPackages (ps: [
      ps.pillow
      #ps.pyhidapi
      ps.pyusb
    ]))
  ];
  meta.mainProgram = "lednamebadge.py";
} ''
  install -D -t $out/bin $src/lednamebadge.py
  patchShebangs $out/bin
''
