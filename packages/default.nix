{
  self,
  system,
  pkgs,
}: let
  sh = name: cmd: pkgs.writeScriptBin name "#!/bin/sh\nexec -a ${name} ${cmd}\n";
  docs = import "${self.inputs.nixpkgs}/doc" {inherit pkgs;};
  dashing = pkgs.callPackage ./dashing.nix {};
  docsets' = pkgs.callPackage ./docsets.nix {
    nixpkgs-docs = docs;
    inherit dashing;
  };
in rec {
  abcde = pkgs.callPackage ./abcde.nix {};
  awesome = pkgs.callPackage ./awesome.nix {inherit awesome-solarized lain;};
  awesome-config = pkgs.callPackage ./awesome-config.nix {
    system-constants.lainPath = "${lain.outPath}/share/lua/${awesome.lua.luaversion}/lain";
  };
  awesome-solarized = self.inputs.awesome-solarized.packages.${system}.default;
  # development version of alot, exposed from the upstream flake for
  # convenience
  alot = self.inputs.alot.packages.${system}.default;
  ansible2nix = pkgs.writeScriptBin "ansible2nix" ''
    ${self.inputs.ansible2nix.packages.${system}.ansible2nix}/bin/ansible2nix \
      bootstrap/requirements.yml > packages/ansible-collections.nix
  '';
  backup-phone-contacts = pkgs.callPackage ./backup-phone-contacts.nix {
    inherit gammu vcard2to3 vcardtool;
  };
  beets = (pkgs.callPackage ./beets.nix {}).package-with-config;
  code2flow = pkgs.callPackage ./code2flow.nix {};
  docsets = docsets'.docsets;
  handbrake = pkgs.callPackage ./handbrake.nix {};
  install-docsets = docsets'.install-docsets;
  firefox-on-demand = sh "firefox" ''
    nix run ${self.inputs.nixpkgs}\#firefox -- --safe-mode --private-window "$@"
  '';
  fzf-dict = pkgs.callPackage ./fzf-dict.nix {};
  # this is a pined version of gammu because of nixpkgs#261994
  gammu = self.inputs.nixpkgs-gammu.legacyPackages.${system}.gammu;
  # development version of khard, exposed from the upstream flake for
  # convenience
  khard = self.inputs.khard.packages.${system}.default;
  lain = pkgs.callPackage ./lain.nix {};
  led-name-badge = pkgs.callPackage ./led-name-badge.nix {};
  neovim = pkgs.callPackage ../neovim {inherit self;};
  test-neovim-with-ls = pkgs.callPackage ./test-neovim-with-ls.nix {};
  nix-help = sh "nix-help" "xdg-open ${pkgs.nix.doc}/share/doc/nix/manual/index.html";
  nixpkgs-help = sh "nixpkgs-help" "xdg-open ${docs}/share/doc/nixpkgs/manual.html";
  nvimpager = pkgs.callPackage ./nvimpager.nix {inherit self system;};
  upgrade-script = pkgs.callPackage ./upgrade-script.nix {};
  vcard2to3 = pkgs.callPackage ./vcard2to3.nix {};
  vcardtool = pkgs.callPackage ./vcardtool.nix {};
  #weechat = pkgs.callPackage ./weechat.nix {}; # insecure
  test-awesome-config = pkgs.callPackage ./test-awesome-config.nix {
    inherit system self awesome lain;
  };
  tex-env = pkgs.callPackage ./tex-env.nix {};
}
