#!/usr/bin/env bash

set -euo pipefail

cleanup_rm () { local e=$?; rm -rf "$dir"; exit $e; }
cleanup_worktree () { local e=$?; git worktree remove --force .; exit $e; }
cleanup_full () {
  local e=$?
  cd
  git -C "$flake" worktree remove --force "$dir"
  git -C "$flake" branch --quiet --delete --force "$branch"
  git -C "$flake" tag --list "$branch"-merge \
    | xargs --no-run-if-empty git -C "$flake" tag --delete
  exit $e
}
cleanup_user () { local e=$?; user cleanup -b "$branch" -d "$dir" -f "$flake"; exit $e; }
build () {
  local ref=$1
  if git diff --quiet updates "$ref"; then
    exit
  fi
  nixos-rebuild build --flake "$flake?ref=$ref" && git tag "$branch-build" "$ref"
}
user () { # execute an action from this script as unprivileged user
  # shellcheck disable=SC2016
  su --command '"$0" "$@"' -- "$user" "$0" "-${-//[^x]/}a" "$@"
}

action=main
while getopts a:b:d:f:x opt; do
  case $opt in
    a) action=$OPTARG;;
    b) branch=$OPTARG;;
    d) dir=$OPTARG;;
    f) flake=$(realpath "$OPTARG");;
    x) set -x;;
    *) echo Bad command line arguments >&2; exit 2;;
  esac
done

case $action in
  main)
    user=$(stat --printf %U "$flake/.git")
    # create a temp dir to work in
    dir=$(mktemp -d)
    trap cleanup_rm EXIT
    chown "$user" "$dir"
    cd "$dir"

    # create a branch name
    branch=update/$(date +%F)-$$

    user build -b "$branch" -d "$dir" -f "$flake"
    trap cleanup_user EXIT

    # install the build result as the latest generation of the system profile
    nix-env --profile /nix/var/nix/profiles/system --set ./result
    # switch to the new system and install it's boot entry
    result/bin/switch-to-configuration switch

    # clean up after switch
    user post-switch -b "$branch" -d "$dir"
    ;;
  build)
    # create our worktree in the temp dir
    git -C "$flake" worktree add --quiet -b "$branch" "$dir" updates
    trap cleanup_worktree EXIT

    # merge main branch into our updates branch
    git merge --quiet --no-edit main
    git tag "$branch"-merge "$branch"

    # update the flake.lock file
    nix flake update --commit-lock-file

    trap cleanup_full EXIT

    # try to build the updated or the merged rev
    build "$branch" || build "$branch-merge"

    nix flake check "$flake?ref=$branch-build"
    trap - EXIT
    ;;
  post-switch)
    git tag "$branch"-switch
    git switch --quiet updates
    git merge --quiet --no-edit "$branch"-switch

    pattern='update/[2-9][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]-*'
    git tag --list "$pattern-build" "$pattern-merge" \
      | xargs --no-run-if-empty git tag --delete
    git for-each-ref --format="%(refname:short)" "refs/heads/$pattern" \
      | (grep -v -F "$branch" || true) \
      | xargs --no-run-if-empty git branch --quiet --delete --force
    ;;
  cleanup) cleanup_full;;
esac
