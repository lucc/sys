{ python3Packages, fetchPypi, ... }:

   python3Packages.buildPythonPackage rec {
      pname = "code2flow";
      version = "2.5.1";
      src = fetchPypi {
        inherit pname version;
        sha256 = "sha256-rZhxeSDaZZjaw4oZUfOLhIkfSqS9LzbBGbDbrHKfR4U=";
      };
      doCheck = true;
    }
