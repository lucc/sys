{ fetchFromGitHub, lua }:
lua.pkgs.buildLuaPackage {
  pname = "lain";
  version = "scm-1";

  src = fetchFromGitHub {
    owner = "lcpz";
    repo = "lain";
    rev = "0df20e38bbd047c1afea46ab25e8ecf758bb5d45";
    sha256 = "sha256-SjlUbiWdLQrxhNIB7p9JvQGwxwcu/4f9LtwjW+LNu78=";
  };

  dontBuild = true;
  installPhase = ''
    mkdir -p $out/share/lua/${lua.luaversion}
    cp -r $src $out/share/lua/${lua.luaversion}/lain
  '';

  #disabled = lua.pkgs.luaOlder "5.3";
  propagatedBuildInputs = [
    lua
    lua.pkgs.dkjson
  ];

  meta = {
    homepage = "https://github.com/lcpz/lain";
    description = "Layout, widgets and utilities for Awesome WM";
    license.fullName = "GPL-2.0";
  };
}
