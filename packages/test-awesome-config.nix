{ self
, system
  # packages from nixpkgs or local packages
, awesome
, lain
, writeShellApplication
, xorg
}:

let
  config = self.packages.${system}.awesome-config.override {
    system-constants = {
      battery = "foo";
      flake = "bar";
      wifiDevices = [ "baz" ];
      lainPath = "${lain.outPath}/share/lua/${awesome.lua.luaversion}/lain";
      menuKey = "Print";
    };
  };
in

writeShellApplication {
  name = "test-awesome-config";
  text = ''
    # Create virtual screens with Xephyr
    screens=''${1:-1}
    d=:$RANDOM
    for _ in $(seq "$screens"); do
      options+=(-screen 640x480)
    done
    ${xorg.xorgserver}/bin/Xephyr +xinerama -ac "''${options[@]}" $d &
    pid=$!

    # start awesome
    if [[ "$screens" -gt 1 ]]; then
      # this hack seems to trigger awesome to occupy both screens
      (sleep 1 && DISPLAY=$d ${xorg.xrandr}/bin/xrandr --output default --off) &
    fi
    DISPLAY=$d ${awesome}/bin/awesome --search ${config} --config ${config}/rc.lua

    # cleanup Xephyr
    kill $pid
  '';
}
