{ writeShellApplication, git, nixos-rebuild, su }:

writeShellApplication {
  name = "upgrade-script";
  runtimeInputs = [ git nixos-rebuild su ];
  text = builtins.readFile ./upgrade-script.sh;
}
