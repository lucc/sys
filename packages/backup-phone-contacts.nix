# for backing up phone contacts
{
  writeShellApplication,
  gammu,
  vcard2to3,
  vcardtool,
  fdupes,
}:

writeShellApplication {
  name = "backup-phone-contacts";
  text = ''
    base=$HOME/.local/share/khard
    dir=$base/backup-$(date +%F)
    mkdir -p "$dir"
    cd "$dir"

    # the bluetooth service is already running because we have set
    # hardware.bluetooth.enable = true.
    ${gammu}/bin/gammu -s 0 backup contacts.vcf -yes

    ${vcard2to3}/bin/vcard2to3 --remove_dollar contacts.vcf
    ${vcardtool}/bin/vcardtool split contacts.vcf.converted .

    for file in contacts.vcf.converted__*.vcf; do
      mv "$file" "$(awk -F: '/UID/ { print $2 }' "$file").vcf"
    done
    rm contacts.vcf contacts.vcf.converted

    ${fdupes}/bin/fdupes -rdNq "$base"/backup-* > /dev/null
  '';
}
