# This weechat override is moved to a separate file in order to make it
# importable by callPackage from different locations.  This is the easiest
# solution I could come up with to have it pick up the override of the
# permittedInsecurePackages.
{
  weechat,
  weechatScripts,
}:
weechat.override {
  configure = _: {
    scripts = with weechatScripts; [
      multiline
      wee-slack # depends on insecre olm
      weechat-matrix
      weechat-notify-send
    ];
  };
}
