{
  buildEnv,
  latexrun,
  pdfpc,
  texlive,
}:
buildEnv {
  name = "tex-env";
  paths = [
    latexrun
    pdfpc
    #texlive.combined.scheme-full
    (texlive.combine {
      inherit
        (texlive)
        scheme-full
        algorithms
        collection-fontsextra
        babel
        babel-german
        collection-fontsrecommended
        ;
    })
  ];
}
