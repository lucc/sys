let
  luc = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC8dre1UO1yDVjtoRXu/HfeaALeBh1WIqcaxkEeQBuSJh5FWtC50uoS0/fI1NeeMWOO4qREWwpt4XdrGGnqmo8G0nOxbJkFqXJ4KxuCi1znBmZQbQJ5oclWLW6RsQHvie8V8hPWs0MsOmamAzhVCfHb0xGshatzd88g3I3D+o8j1FgvP++1EKbEVYMI9FOr8BAlRhLeI8FnIrX/pDy+buPH6MmnEvgUSKWZCjBeBxxMyX6P/Ivcg7DKmWdzbtcqOD3z5FFlmBqRJA0MjwBW4MLut9NnG9O1uHNtcJ7GwXk6vaZKP6OJKM6yi+/lihe+DoG5Md/C8cn0FaZZagRhnYTg9JRw6DigryXKxqZP8L0zJTOjipk+6liGEaIwSPpnaNgNH9ALhmzyeeBOzVbRXpUlwiR6AEke40xyseUEDKouTVAJMnshMNh7UeRGGQfvSP1ibfz1+kdA4hbPUCxzdPrqQmsy4YyjrGe3T4xm1rY0+LXtz3zwMQDAVE3+/swu5DM= luc@yoga";
  users = [ luc ];

  yoga = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILml1Qg5IfCLSlezulw9IkHhiAuy6GuFRBDq7alokqn4";
  terra = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICW9LRlR4G8Cif0TO8f6vvhz/c4ryF9fbYikkKi3Fp8x";
  systems = [ yoga terra ];
in
{
  "wpa_supplicant.conf".publicKeys = users ++ systems;
  msmtprc.publicKeys = users ++ [ yoga ];
  msmtprc-work.publicKeys = users ++ [ terra ];
}
