{
  description = "system configurations";

  inputs = {
    abq.url = "github:lucc/abq";
    agenix.url = "github:ryantm/agenix";
    alot.url = "github:pazz/alot";
    ansible2nix.url = "github:lucc/ansible2nix";
    awesome-solarized.url = "github:lucc/awesome-solarized";
    home-manager.url = "github:nix-community/home-manager";
    khard.url = "github:lucc/khard";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    # a pinned version of nixpkgs where gammu still works, see
    # https://github.com/NixOS/nixpkgs/issues/261994
    nixpkgs-gammu.url = "github:nixos/nixpkgs/6ddd81bf116f33f66d1a72747bb75bd3e30d3e61";
    nvimpager.url = "github:lucc/nvimpager";
    purebred.url = "github:purebred-mua/purebred";
    scripts.url = "github:lucc/shell";
    utils.url = "github:numtide/flake-utils";
    #neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";

    abq.inputs.nixpkgs.follows = "nixpkgs";
    abq.inputs.utils.follows = "utils";
    agenix.inputs.nixpkgs.follows = "nixpkgs";
    # Temporary disabled because the gpg python module does not work with
    # python3.12, see https://github.com/NixOS/nixpkgs/issues/354166
    #alot.inputs.nixpkgs.follows = "nixpkgs";
    alot.inputs.flake-utils.follows = "utils";
    ansible2nix.inputs.flake-utils.follows = "utils";
    ansible2nix.inputs.nixpkgs.follows = "nixpkgs";
    awesome-solarized.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    khard.inputs.nixpkgs.follows = "nixpkgs";
    nvimpager.inputs.flake-utils.follows = "utils";
    nvimpager.inputs.nixpkgs.follows = "nixpkgs";
    purebred.inputs.utils.follows = "utils";
    scripts.inputs.nixpkgs.follows = "nixpkgs";

    # neovim plugin that are not packaged in nixpkgs
    nvim-plugin-applescript = { url = "github:vim-scripts/applescript.vim"; flake = false; };
    nvim-plugin-blade = { url = "github:jwalton512/vim-blade"; flake = false; };
    nvim-plugin-diffchar = { url = "github:rickhowe/diffchar.vim"; flake = false; };
    nvim-plugin-haproxy = { url = "github:Joorem/vim-haproxy"; flake = false; };
    nvim-plugin-icalendar = { url = "github:vim-scripts/icalendar.vim"; flake = false; };
    nvim-plugin-known-hosts = { url = "github:pix/vim-known_hosts"; flake = false; };
    nvim-plugin-linediff = { url = "github:AndrewRadev/linediff.vim"; flake = false; };
    nvim-plugin-markdown-folding = { url = "github:nelstrom/vim-markdown-folding"; flake = false; };
    nvim-plugin-paredit = { url = "github:kovisoft/paredit"; flake = false; };
    nvim-plugin-pass = { url = "github:fourjay/vim-password-store"; flake = false; };
    nvim-plugin-php-foldexpr = { url = "github:swekaj/php-foldexpr.vim"; flake = false; };
    nvim-plugin-php-manual = { url = "github:alvan/vim-php-manual"; flake = false; };
    nvim-plugin-signjump = { url = "github:ZeroKnight/vim-signjump"; flake = false; };
    nvim-plugin-systemd = { url = "git+https://fedorapeople.org/cgit/wwoods/public_git/vim-scripts.git?ref=master"; flake = false; };
    pycfg = { url = "git+https://gist.github.com/jettero/4a619004fb23ada1e5cf302211eee8ae"; flake = false; };
    vcard = { url = "github:vim-scripts/VCard-syntax"; flake = false; };
    vdebug = { url = "github:vim-vdebug/vdebug"; flake = false; };
  };

  outputs = { self, utils, nixpkgs, ... }: {
    nixosConfigurations = import ./nixosConfigurations { inherit self; };
    nixosModules.default = import ./modules;
  }
  // utils.lib.eachSystem [ utils.lib.system.x86_64-linux ] (system: {
      packages = import ./packages { inherit self system; pkgs = import nixpkgs { inherit system; };
      };
    devShells = import ./devShells.nix { inherit self system; };
    checks = import ./checks.nix { inherit self system; };
  });
}
