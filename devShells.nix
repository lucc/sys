{ self, system }:

let
  # This function creates a shell environment with ansible and some
  # dependencies to run the playbooks in this repository.
  mkAnsibleShell = withMitogen: let
    pkgs = import self.inputs.nixpkgs {
      inherit system;
      overlays = [self.inputs.ansible2nix.overlays.default];
    };
    py = pkgs.python3;
    mitogen =
      (pkgs.lib.mkIf withMitogen {
        ANSIBLE_STRATEGY = "mitogen_linear";
        ANSIBLE_STRATEGY_PLUGINS = "${py.pkgs.mitogen}/lib/python${py.pythonVersion}/site-packages/ansible_mitogen/plugins/strategy";
      })
      .content;
  in
    pkgs.mkShell ({
        buildInputs = with pkgs; [
          ansible
          (py.withPackages (ps:
            with ps; [
              cryptography
              jmespath
            ]))
          git
          openssh
          parted
          sshpass
        ];
        ANSIBLE_COLLECTIONS_PATH = pkgs.ansibleGenerateCollection pkgs.ansible (import ./packages/ansible-collections.nix);
      }
      // mitogen);
in {
  default = mkAnsibleShell true;
  ansible = mkAnsibleShell false;
  ansible-mitogen = mkAnsibleShell true;
}
