{ self, ... }:

let
  inherit (self.inputs.nixpkgs) lib;
  system = self.inputs.utils.lib.system.x86_64-linux;
  dirs = lib.filterAttrs (n: v: v == "directory");
  mkSys = name: lib.nixosSystem {
    inherit system;
    modules = [
      ./${name}
      ./${name}/hardware-configuration.nix
      self.inputs.agenix.nixosModules.age
      self.inputs.home-manager.nixosModules.home-manager
      ({pkgs, ...}:{
        networking.hostName = name;
        _module.args = {
          # hand this flake itself to all our modules as argument
          inherit self;
          # Construct the packages of this flake with the modified nixpkgs
          # version for this system.  This ensures that settings to
          # nixpkgs.config are passed along.
          pkgs' = import ../packages { inherit self pkgs system; };
        };
        home-manager.useGlobalPkgs = true;
        home-manager.useUserPackages = true;
      })
    ];
  };
in
builtins.mapAttrs (name: _: mkSys name) (dirs (builtins.readDir ./.))
