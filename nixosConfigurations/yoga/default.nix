# settings specific to the yoga, including hardware stuff copied over from the
# generated hardware-configuration.nix
{ config, lib, pkgs, pkgs', self, ... }:
let
  neovim = pkgs.callPackage ../../neovim { inherit pkgs self; };
  beets = pkgs.callPackage ../../packages/beets.nix {};
in
{

  imports = [
    ../../modules
    ./gammu.nix
    ./gomuks.nix
  ];

  # Put the upstream development version of khard into an overlay to have
  # home-manager pick it up.
  nixpkgs.overlays = [(final: prev: { khard = pkgs'.khard; })];

  lucc.mainUser = "luc";
  lucc.autoUpgrade.enable = true;
  lucc.autoUpgrade.flake = "/home/${config.lucc.mainUser}/src/sys";
  lucc.gui = [ "awesome" "extras" ];
  lucc.awesome.widgets = ./widgets.lua;
  lucc.awesome.menu-key = "Print";
  lucc.xrandr = "srandrd";
  lucc.laptop.enable = true;
  lucc.laptop.battery = "BAT1";
  lucc.laptop.wifiDevices = [ "wlp1s0" ];
  lucc.locale = true;
  lucc.mail.enable = true;
  lucc.mail.userInterfaces = [ "alot" "purebred" "emacs" ];
  lucc.nas = true;
  age.secrets.msmtprc.file = ../../secrets/msmtprc;
  lucc.mpd = true;
  lucc.virtualisation.podman = true;

  home-manager.users.${config.lucc.mainUser} = { pkgs, config, ... }: {
    programs.taskwarrior = {
      enable = true;
      package = pkgs.taskwarrior3;
      colorTheme = "solarized-dark-256";
      config = {
        search.case.sensitive = "no";
        verbose = "footnote,label,new-id,affected,edit,special,project,sync,unwait,override";
      };
    };
    programs.beets = {
      enable = true;
      package = beets.package;
      settings = beets.config;
      mpdIntegration.enableStats = true;
      mpdIntegration.enableUpdate = true;
    };

    programs.khard = {
      enable = true;
      settings = {
        general = {
          editor = "nvim";
          merge_editor = ["nvim" "-d" "-c" "windo setfiletype yaml"];
        };
        "contact table" = {
          sort = "last_name";
          group_by_addressbook = false;
          show_nicknames = false;
          show_uids = true;
          localize_dates = false;
        };
        vcard = {
          private_objects = ["Jabber" "Skype" "Twitter" "ICQ" "Bday" "ABDATE"];
          preferred_version = "4.0";
          #search_in_source_files = true
        };
      };
    };
    accounts.contact.accounts.main.khard.enable = true;
    accounts.contact.accounts.main.local.path = "${config.home.homeDirectory}/.local/share/khard/main";
    accounts.contact.accounts.backup.khard.enable = true;
    accounts.contact.accounts.backup.local.path = "${config.home.homeDirectory}/.local/share/khard/backup-merged";

    xdg.userDirs = {
      enable = true;
      desktop = "${config.home.homeDirectory}/";
      documents = "${config.home.homeDirectory}/";
      music = "${config.home.homeDirectory}/audio";
      pictures = "${config.home.homeDirectory}/img";
      publicShare = "${config.home.homeDirectory}/";
      templates = "${config.home.homeDirectory}/";
      videos = "${config.home.homeDirectory}/vid";
    };

    # The state version is required and should stay at the version you
    # originally installed.
    home.stateVersion = "23.11";
  };

  boot.initrd.availableKernelModules = [ "usb_storage" ];
  boot.kernelModules = [ "kvm-intel" "wl" ];

  nix.settings.max-jobs = lib.mkDefault 4;

  hardware.cpu.intel.updateMicrocode = true;
  #hardware.acpilight.enable = true;
  #hardware.brightnessctl.enable = true;

  nixpkgs.config.allowUnfree = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.consoleMode = "auto";
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.luks.devices = {
    decrypt = {
      device = "/dev/disk/by-partlabel/crypt.part";
      preLVM = true;
    };
  };

  networking.resolvconf.dnsExtensionMechanism = false;

  age.identityPaths = [ "/home/luc/.ssh/yoga-age-host-key" ];

  # The display of the yoga has a very high resolution by default which is not
  # compatible with my awesome setup
  services.xserver.resolutions = [
    { x = 1600; y = 900; }
    { x = 1920; y = 1080; }
    { x = 2048; y = 1152; }
  ];

  boot.supportedFilesystems = [ "ntfs" ];

  environment.defaultPackages = [ ];
  environment.systemPackages = with pkgs; [
    (neovim.withLS (ls: [
      ls.ansible
      ls.bash
      ls.docker
      ls.json
      ls.lua
      ls.marksman
      ls.nil
      ls.pylsp
      ls.pyright
      ls.rust
      ls.tex
      ls.typos
      ls.vim
      ls.yaml
    ]))

    # mail and chat stuff
    (pkgs.callPackage ../../packages/weechat.nix {})

    # pim tools
    khal
    # coding
    ghc cabal-install cabal2nix
    # the most basic python devel env
    (python3.withPackages (ps: [ ps.ipython ]))

    # Stuff from the old nix-env
    hunspell hunspellDicts.de-de hunspellDicts.en-gb-large
    hunspellDicts.en-us-large
    sshfs-fuse

    self.inputs.scripts.packages.${system}.default

    pkgs'.firefox-on-demand
    pkgs'.nix-help pkgs'.nixpkgs-help
  ];

  #documentation.nixos.includeAllModules = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "22.05"; # Did you read the comment?

}
