{ pkgs', ... }:
{
  environment.etc.gammurc.text = ''
    [gammu]
    port=00:25:47:10:0D:2D
    connection=bluephonet
    name=Nokia 6300 bluetooth
  '';
  environment.systemPackages = [ pkgs'.backup-phone-contacts ];
  hardware.bluetooth.enable = true;
}
