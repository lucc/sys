{pkgs, ...}: {
  environment.systemPackages = [pkgs.gomuks];

  # libolm was deprecated in
  # https://gitlab.matrix.org/matrix-org/olm/-/commit/6d4b5b07887821a95b144091c8497d09d377f985
  # gomuks dependens on mautrix which in turn dependens on libolm. The mautrix
  # people are tracking it here: https://github.com/mautrix/go/issues/262
  nixpkgs.config.permittedInsecurePackages = [
    "olm-3.2.16"
  ];
}
