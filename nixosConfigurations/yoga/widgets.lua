-- widgets for the bar
local globals = require("globals")

local battery = require("widgets.battery")(globals.battery)
local disk = require("widgets.disk")
local git = require("widgets.git")
local github = require("widgets.github")
local mail = require("widgets.notmuch"):init([[\(query:inbox_notification or query:listbox_notification\)]])
local mpd = require("widgets.mpd")
local systemd = require("widgets.systemd")
local tasks = require("widgets.taskwarrior")
local updates = require("widgets.nixos")(globals.flake)
local wifi = require("widgets.wifi")(globals.wifiDevices[1])

local home = os.getenv("HOME")
git:register(
  {path = home.."/.config", untracked = false},
  {path = home.."/.config/pass"},
  {path = home.."/.config/alot", commits = false},
  {path = home.."/src/alot"},
  {path = home.."/src/khard"},
  {path = home.."/src/nvimpager"},
  {path = home.."/src/shell", branch_count = 2},
  {path = home.."/src/sys"}
)

return {
  -- in sorted order for the widget bar
  disk,
  github,
  git,
  systemd,
  mail,
  mpd.widget,
  updates,
  battery,
  tasks,
  wifi,
  -- and with keys to be accessed elsewhere
  tasks = tasks,
  music = mpd,
  mail = mail,
  git = git,
}
