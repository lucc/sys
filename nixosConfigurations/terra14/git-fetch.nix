{ pkgs, ... }:
{
  systemd.user.services.git-fetch = {
    environment.GIT_TERMINAL_PROMPT = "0";
    environment.GIT_SSH_COMMAND = "${pkgs.openssh}/bin/ssh -o PasswordAuthentication=no";
    script = ''
      set +e
      for d in ~/src/*/.git ~/dev.vm/*/.git; do
        git -C ''${d%.git} fetch --all --prune
      done
    '';
    path = [ pkgs.git ];
  };
}
