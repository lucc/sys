{...}: {
  systemd.user.services.arbeitszeit = {
    script = ''
      journalctl --list-boots > ~/arbeitszeit.log/boots-$(date +%F).log
    '';
    wantedBy = ["default.target" "shutdown.target"];
  };
}
