{ self, config, lib, pkgs, ... }:
let
  pkgs' = self.packages.${pkgs.system};
  neovim = pkgs.callPackage ../../neovim { inherit pkgs self; };
  psysh = self.inputs.nixpkgs-gammu.legacyPackages.${pkgs.system}.phpPackages.psysh;
in
{
  imports = [
    ../../modules
    ./git-fetch.nix
    ./work.nix
    ../yoga/gomuks.nix
  ];

  lucc.mainUser = "luc";
  lucc.autoUpgrade.enable = true;
  lucc.autoUpgrade.flake = "/home/${config.lucc.mainUser}/src/sys";
  # run upgrades during lunch break as the computer is powered down at night
  system.autoUpgrade.dates = "12:30";
  lucc.gui = [ "awesome" ];
  lucc.awesome.widgets = ./widgets.lua;
  lucc.awesome.calculator = "${psysh}/bin/psysh";
  lucc.laptop.enable = true;
  lucc.laptop.battery = "BAT0";
  lucc.laptop.wifiDevices = [ "wlp0s20f3" ];
  lucc.locale = true;
  lucc.mail.enable = true;
  lucc.mail.userInterfaces = [ "alot" "astroid" ];
  age.secrets.msmtprc.file = ../../secrets/msmtprc-work;
  lucc.printing = true;
  services.printing.drivers = [ pkgs.brlaser ];
  lucc.virtualisation.docker = true;
  lucc.virtualisation.podman = true;
  lucc.virtualisation.vagrant = true;

  systemd.user.timers.backup-small.enable = false;

  # hardware configuration
  boot.initrd.availableKernelModules = [ "usb_storage" "sd_mod" ];
  boot.supportedFilesystems = [ "ntfs" ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.consoleMode = "auto";
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.luks.devices.decrypt = {
    device = "/dev/disk/by-partlabel/crypt.part";
    preLVM = true;
  };

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";

  nix.settings.max-jobs = lib.mkDefault 4;

  hardware.enableRedistributableFirmware = true;
  #hardware.acpilight.enable = true;
  #hardware.brightnessctl.enable = true;

  nixpkgs.config.allowUnfree = true;

  # docking station
  networking.interfaces.enp4s0.useDHCP = true;
  networking.resolvconf.dnsExtensionMechanism = false;

  age.identityPaths = [ "/home/luc/.ssh/terra-age-host-key" ];

  fonts.packages = [
    # used by some projects we develop
    pkgs.google-fonts
  ];

  environment.defaultPackages = [ ];
  environment.systemPackages = with pkgs; with phpPackages; [
    (neovim.withLS (ls: [
      ls.ansible
      ls.bash
      ls.css
      ls.deno
      ls.docker
      ls.go
      ls.html
      ls.json
      ls.lua
      ls.marksman
      ls.nil
      ls.php-intelephense
      ls.php-psalm
      ls.phpactor
      ls.pylsp
      ls.pyright
      ls.typos
      ls.vetur
      ls.yaml
    ]))
    httpie

    # pim tools
    khal pkgs'.khard
    # the most basic python devel env
    (python3.withPackages (ps: with ps; [
      ipython pip virtualenv hetzner
    ]))

    dbeaver-bin

    # Stuff from the old nix-env
    firefox chromium
    hunspell hunspellDicts.de-de hunspellDicts.en-gb-large
    hunspellDicts.en-us-large
    nfs-utils sshfs-fuse

    self.inputs.scripts.packages.${system}.default

    # php work stuff
    php psysh composer phpstan psalm
    openssl
    ansible-lint

    # managing servers
    hcloud
    lab glab

    autorandr
  ];

  programs.zsh.interactiveShellInit = ''
    # set the mailpath on every tty ##########################################
    mailpath=(~/.cache/notmuch/new-mail-marker ~/mail/inbox)
    set -U

    # functions for work #####################################################
    function tel { rg --ignore-case $@ /media/nextcloud/ASAM\ Intern/asam_telefonliste.txt; }
    function vm {
      [[ $PWD =~ ~/dev.vm ]] && local dir=''${PWD#~/dev.vm}
      if [[ $# -eq 0 ]]; then
        set bash
      fi
      (cd ~/src/vm-infra
       vagrant ssh --command "''${dir+cd /var/www/html/dev/$dir && }$*"
      )
    }
  '';
  environment.shellAliases.s = lib.mkForce "rg --ignore-case --hidden --glob '!.git/'";
  environment.shellAliases.s_ = ''rg --ignore-case -g "!*.css" -g "!*.js" -g "!*.xml" -g "!*.map"'';
  # make a deploy merge request in gitlab for the current git repository
  environment.shellAliases.deploy-merge-request = "lab mr create origin production --source origin:master";

  programs.htop.settings = {
    header_layout = "three_33_34_33";
    column_meters_0 = "LeftCPUs Memory";
    column_meter_modes_0 = "1 1";
    column_meters_1 = "RightCPUs Swap";
    column_meter_modes_1 = "1 1";
    column_meters_2 = "Tasks LoadAverage Uptime DiskIO NetworkIO";
    column_meter_modes_2 = "2 2 2 2 2";

  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
