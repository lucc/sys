-- widgets for the bar
local naughty = require("naughty")
local globals = require("globals")

local battery = require("widgets.battery")(globals.battery)
local disk = require("widgets.disk")
local git = require("widgets.git")
local mail = require("widgets.notmuch"):init([[\(is:inbox AND \(is:unread OR is:todo\)\)]])
local systemd = require("widgets.systemd")
local tasks = require("widgets.taskwarrior")
local updates = require("widgets.nixos")(globals.flake)
local wifi = require("widgets.wifi")(globals.wifiDevices[1])

local home = os.getenv("HOME")
git:register(
  {glob = home.."/dev.vm/*/.git"},
  {glob = home.."/src/*/.git"},
  {path = home.."/.config", untracked = false},
  {path = home.."/.config/pass"},
  -- overwrite options from globbing above
  {path = home.."/src/sys", untracked = false, branch_count = false}
)

local mpd = setmetatable({}, {
  __index = function(_table, _key)
    return function()
      naughty.notify({
	preset = naughty.config.presets.critical,
	title="MPD is not enabled!",
      })
    end
  end
})

return {
  -- in sorted order for the widget bar
  disk,
  git,
  systemd,
  mail,
  updates,
  battery,
  wifi,
  -- and with keys to be accessed elsewhere
  tasks = tasks,
  music = mpd,
  mail = mail,
  git = git,
}
