{ lib, ... }:
{
  environment.variables.EDITOR = lib.mkOverride 900 "nvim";
  programs.nano.enable = false;
}
