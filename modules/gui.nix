{ config, pkgs, pkgs', lib, ... }:

let
  inherit (lib) mkOption types mkIf mkMerge;
  cfg = config.lucc.gui;
  cfg2 = config.lucc.awesome;
  # applications specific to Xorg
  xApps = with pkgs; [
    xclip
    xdotool
    xorg.xev
  ];
  wlApps = with pkgs; [
    wl-clipboard
  ];
  xinit-awesome = pkgs.writeScript "xinit-awesome" ''
    # Fix scaling for alacritty between monitors
    # https://github.com/alacritty/alacritty/issues/3465
    export WINIT_X11_SCALE_FACTOR=1

    ${
      if config.lucc.xrandr == "srandrd"
      then ''
        ${pkgs.srandrd}/bin/srandrd auto-xrandr &
        auto-xrandr &
      ''
      else ""
    }

    # load environment variables into systemd user splice (GNUPGHOME is
    # currently not used, see password-manager.nix)
    systemctl --user import-environment DISPLAY

    systemctl --user start ssh-add xcape
    (sleep 20 && qutebrowser) &
    ${lib.strings.concatMapStrings (c: "(${c}) &\n") cfg2.xinit-extra-commands}

    # setup pulseaudio
    pactl load-module module-x11-bell

    # start the window manager itself
    exec awesome
  '';
  start-wayland = cmd: pkgs.writeScript "start-wayland" ''
    # use the german keyboard layout
    export XKB_DEFAULT_LAYOUT=de
    # use the caps lock key as ctrl
    export XKB_DEFAULT_OPTIONS=caps:ctrl_modifier

    # urge some libs to use the wayland backend as noted in
    # https://github.com/swaywm/sway/wiki/Running-programs-natively-under-wayland
    # FIXME 2017-01-05 does currently not work, either apps do not yet support it
    # or I don't have the necessary libs installed, no idea.
    export GDK_BACKEND=wayland
    export CLUTTER_BACKEND=wayland
    export QT_QPA_PLATFORM=wayland-egl
    export QT_DISABLE_WINDOWDECORATION=1

    exec ${cmd}
  '';
  awesome-config = pkgs.callPackage ../packages/awesome-config.nix {
    system-constants = let laptop = config.lucc.laptop; in
    lib.strings.optionalString laptop.enable {
      battery = laptop.battery;
      flake = config.lucc.autoUpgrade.flake;
      wifiDevices = laptop.wifiDevices;
      lainPath = "${pkgs'.lain.outPath}/share/lua/${pkgs.awesome.lua.luaversion}/lain";
      menuKey = cfg2.menu-key;
    };
    widgets = cfg2.widgets;
  };
in

{

  options.lucc.gui = mkOption {
    type = types.listOf (types.enum [ "awesome" "qtile" "sway" "extras" ]);
    default = [ ];
    example = [ "awesome" "extras" ];
    description = "the graphical user interfaces (and programs) to enable";
  };
  options.lucc.awesome = {
    calculator = mkOption {
      type = types.str;
      default = "${pkgs.ghc}/bin/ghci";
      example = "psysh";
      description = "a command to use as calculator behind mod4-c";
    };
    menu-key = mkOption {
      type = types.str;
      default = "Menu";
      example = "Print";
      description = "the name of the key to open the main menu";
    };
    widgets = mkOption {
      type = types.nullOr types.path;
      default = null;
      example = ../awesome/widgets.lua;
      description = "an alternative lua file to define the widgets for the right side";
    };
    xinit-extra-commands = mkOption {
      type = types.listOf types.str;
      default = [];
      example = [ "my-command" "other-command --with args"];
      description = ''
        A list of shell commands that should be added to the xinit file.  They
        will be executed asynchrounously.
      '';
    };
  };
  options.lucc.xrandr = mkOption {
    type = types.nullOr (types.enum [ "autorandr" "srandrd" ]);
    default = null;
    example = "autorandr";
    description = ''
      which tool to use to manage automatic xrandr settings when monitors are
      (un-) plugged
    '';
  };

  imports = [ ./rofi ];

  config = mkMerge [
    (mkIf (builtins.length cfg > 0) {
      # fonts should be enabled for any graphical system
      fonts.packages = [ pkgs.nerd-fonts.dejavu-sans-mono ];
      fonts.enableDefaultPackages = true;
      # some gui applications I want to use
      environment.systemPackages = with pkgs; [
        alacritty
        feh
        ffmpeg
        adwaita-icon-theme
        zenity # for the password fill script
        libreoffice
        mpv
        qutebrowser
        runningx
        scrot
        zathura
        zeal
      ];

      environment.etc.zathurarc.text = ''
        set window-title-home-tilde true
        set statusbar-home-tilde true

        # This is necessary because the status line is not considered and some
        # text is hidden by it.
        set scroll-full-overlap 0.05

        set page-cache-size 100
        #set synctex true
        #set synctex-editor-command "nvr -todo %{input} +%{line}"

        # for forward compatibility with future releases (the default "plain"
        # produces log messages when running zathura)
        set database sqlite
      '';

      environment.etc."feh/buttons".text = ''
        # unbind default actions for 4 5 6 7
        prev_img
        next_img

        scroll_up    4
        scroll_down  5
        scroll_left  6
        scroll_right 7
      '';
      environment.etc."feh/keys".text = ''
        # always first remove default keybinding

        zoom_in   plus
        zoom_out  minus

        scroll_up        Up
        scroll_down      Down
        scroll_up_page   Prior
        scroll_down_page Next

        jump_fwd         C-Down
        jump_back        C-Up

        #remove comma
        #delete period
        #save_filelist F
        #toggle_fullscreen f
      '';
    })
    (mkIf (builtins.elem "awesome" cfg) {
      environment.systemPackages = xApps ++ [ pkgs'.awesome pkgs.libnotify ];
      # Enable touchpad support.
      services.libinput.enable = true;
      # Enable the X11 windowing system.
      services.xserver = {
        enable = true;
        xkb.layout = "de";
        xkb.options = "caps:ctrl_modifier";

        displayManager.startx.enable = true;
        desktopManager.xterm.enable = false;
        #windowManager.awesome = {
        #  enable = true;
        #  luaModules = [vicious];
        #  #noArgb = true;
        #};
      };
      environment.etc."xdg/awesome".source = awesome-config;
      programs.zsh.loginShellInit = lib.mkAfter ''
        [[ $TTY = /dev/tty1 ]] && exec startx ${xinit-awesome}
      '';

    })
    (mkIf (builtins.elem "qtile" cfg) {
      services.xserver.windowManager.qtile.enable = true;
      environment.systemPackages = wlApps;
      programs.zsh.loginShellInit = lib.mkAfter ''
        [[ $TTY = /dev/tty1 ]] && exec ${start-wayland "qtile start --backend wayland"}
      '';

    })
    (mkIf (builtins.elem "sway" cfg) {
      programs.sway.enable = true;
      environment.systemPackages = with pkgs; wlApps ++ [
        swaybg waybar i3status i3status-rust i3pystatus wl-clipboard rootbar
        nwg-panel yambar ulauncher
      ];
      environment.etc."sway/config".text = ''
        exec term
      '';
      programs.zsh.loginShellInit = lib.mkAfter ''
        [[ $TTY = /dev/tty1 ]] && exec ${start-wayland "sway"}
      '';
    })
    (mkIf (builtins.elem "extras" cfg) {
      environment.systemPackages = with pkgs; [
        calibre
      ];
    })
    # TODO: xmonad
    #windowManager.xmonad = {
    #  enable = true;
    #  enableContribAndExtras = true;
    #  #extraPackages = haskellPackages: [];
    #};
    (mkIf (config.lucc.xrandr == "autorandr") {
      services.autorandr = {
        enable = true;
        defaultTarget = "horizontal-reverse";
        profiles = {
          work = {
            fingerprint = {
              DP-1 = "00ffffffffffff0004721602844370442f18010380351e78cabb04a159559e280d5054bfef80714f8140818081c081009500b300d1c0023a801871382d40582c4500132b2100001e000000fd00384c1f5311000a202020202020000000fc0053323432484c0a202020202020000000ff004c52393044303231383537370a01c7020324f14f01020304050607901112131415161f230907078301000067030c001000382d023a801871382d40582c4500132b2100001f011d8018711c1620582c2500132b2100009f011d007251d01e206e285500132b2100001e8c0ad08a20e02d10103e9600132b21000018000000000000000000000000000000000000007e";
              eDP-1 ="00ffffffffffff000dae081400000000251d0104a51f117802ee95a3544c99260f505400000001010101010101010101010101010101363680a0703820403020350035ad1000001a000000fe004e3134304843472d4551310a20000000fe00434d4e0a202020202020202020000000fe004e3134304843472d4551310a200084";
            };
          };
        };
      };
    })
  ];
}
