# custom configuration of the autoupgrade service for NixOS.
{ pkgs, config, lib, ... }:

let
  cfg = config.lucc.autoUpgrade;
  inherit (import ./lib.nix lib) mkFalseOption;
  inherit (lib) mkOption types mkIf mkForce escapeShellArg;
in

{
  options = {
    lucc.autoUpgrade = {
      enable = mkFalseOption ''
        Improved version of system.autoUpgrade for flakes which uses git
        branches and worktrees to communicate success and failure back to
        the backing git tracked flake.
      '';
      flake = mkOption {
        type = types.str;
        default = "/etc/nixos";
        example = "/path/to/my/system-flake/";
        description = ''
          The path to the system flake to update.
        '';
      };
    };
  };
  config = mkIf cfg.enable {
    system.autoUpgrade.enable = true;

    # we are overwriting the commands in the unit file as I have special ideas
    # how this should work ...
    systemd.services.nixos-upgrade = {
      path = with pkgs; [nixos-rebuild git su python3];
      serviceConfig = {
        Nice = 19;
        IOSchedulingClass = "idle";
        IOSchedulingPriority = 7;
        ExecStart = mkForce
          "${../packages/upgrade-script.py} --debug update --flake ${escapeShellArg cfg.flake}";
      };

      # Give the network time to reconnect when waking from sleep.
      # system.autoUpgrade.randomizedDelaySec is not a good replacement for
      # this as it uses a random value ≥0 so it might be too short.
      preStart = ''
      test 200 = $(
        ${pkgs.curl}/bin/curl \
          --silent \
          --request HEAD \
          --write-out %{http_code} \
          https://cache.nixos.org/ \
        ) || sleep 120
      '';
    };

    # newer versions of git need this setting to even read the repository when
    # it belongs to another user
    programs.git.enable = true;
    programs.git.config.safe.directory = cfg.flake;

    # symlink /etc/nixos/flake.nix to this flake in order to make plain
    # nixos-rebuild pick up this flake
    environment.etc."nixos/flake.nix".source = "${cfg.flake}/flake.nix";

    environment.systemPackages = [
      (pkgs.writeShellScriptBin "upgrade" ''
        if [ "$1" = -f ]; then
          shift
          exec journalctl --follow --unit nixos-upgrade "$@"
        fi
        lines=$(${pkgs.ncurses}/bin/tput lines)
        lines=$(( lines / 2 ))
        lines=$(( lines > 10 ? lines : 10 ))
        systemctl --no-block --no-pager ''${@:-status --lines=$lines} nixos-upgrade
      '')
    ];
    programs.zsh.interactiveShellInit = ''
      compdef upgrade=systemctl
    '';
  };

}
