{ pkgs, lib, config, ... }:

let
  inherit (import ./lib.nix lib) mkFalseOption;
  path = "/media/nas";
in

{
  options.lucc.nas = mkFalseOption ''
    whether to add a automount configuration for the nas
  '';

  config = lib.mkIf config.lucc.nas {
    systemd.automounts = [{
      where = path;
      requires = [ "network-online.target" ];
      automountConfig.TimeoutIdleSec = "20 minutes";
      wantedBy = ["multi-user.target"];
    }];
    systemd.mounts = [{
      where = path;
      what = "nas:";
      type = "nfs";
    }];

    environment.systemPackages = [ pkgs.nfs-utils ];
    environment.shellAliases.f = ''
      find ${path}/video -type f \
        | cut -d/ -f5- \
        | fzf \
        | sed "s#^#${path}/video/#" \
        | xargs --no-run-if-empty --delimiter="\n" mpv --fullscreen'';
  };
}
