{
  programs.tmux = {
    enable = true;
    baseIndex = 1;
    clock24 = true;
    historyLimit = 10000;
    keyMode = "vi";
    extraConfig = ''
      # default setup
      set-option -g default-command zsh
      set-option -ga terminal-overrides "xterm-termite:Tc,alacritty:Tc"

      # This is needed to make shifted arrow keys work.
      set-option -g xterm-keys on

      # mouse
      # The mouse configuration was changed in 2.1
      set-option -g mouse on
      # From https://github.com/tmux/tmux/issues/145
      bind -n WheelUpPane                              \
        if-shell -Ft= "#{mouse_any_flag}"              \
                      "send-keys -M"                   \
                      "if-shell -Ft= '#{pane_in_mode}' \
                                     'send-keys -M'    \
                                     'select-pane -t=; copy-mode -e; send-keys -M'"
      bind -n WheelDownPane select-pane -t= \; send-keys -M

      # resource the config file
      bind-key R source-file ~/.tmux.conf \; display-message "Config reloaded!"

      # Load a simple status line.
      set-option -g status on
      # A more complex status line
      #set-option -g status-right '#(battery.sh -bce tmux) "#H" %F %H:%M:%S'
      #set-option -g status-interval 2
      #set-option -g status-left \
      #  '#S #[fg=green,bg=black]#(tmux-mem-cpu-load --colors 2)#[default]'
    '';
  };
}
