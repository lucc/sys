{ pkgs, ... }:
let
  dictionaries = with pkgs.dictdDBs; [
    deu2eng
    eng2deu
    wiktionary
    wordnet
  ];
  fzf-dict = pkgs.callPackage ../packages/fzf-dict.nix { inherit dictionaries; };
in
{
  environment.systemPackages = [ fzf-dict ];
  environment.etc."dict.conf".text = "server localhost";
  services.dictd.enable = true;
  services.dictd.DBs = dictionaries;
}
