# nixpkgs packages and lib are needed to create this function
pkgs: lib:

let
  inherit (lib.strings) match splitString concatMapStrings escapeShellArg
                        hasInfix escapeShellArgs optionalString;
  inherit (lib.lists) partition head unique filter flatten concatMap
                      reverseList last;
  inherit (lib.attrsets) optionalAttrs;
in

# Function to create a set that can be used to create a backup systemd service
# in systemd.services or systemd.user.services.
{
  # the ssh server where the backups are stored, either a string or a list of
  # two strings.  The first name is the "local" server name if we are in the
  # home network, the second is the "remote" server name if we are not in the
  # home network.
  server,
  # the name of the backup subfolder where the backups are stored
  target-name,
  # list of files to include in the backup
  include,
  # list of files to exclude from the backup
  exclude ? [],
  # retry the backup on failure and add --link-dest arguments to the rsync
  # call for old .in_progress dirs in order to speed up big backups that might
  # get interrupted a lot
  retry ? false,
}:

  let

    # code to set up the domain name shell variable
    setup-code = if builtins.isList server then ''
      current_network=$(${pkgs.wpa_supplicant}/bin/wpa_cli status \
        | ${pkgs.gawk}/bin/awk -F= '$1=="ssid" {print $2}')
      if [ "$current_network" = WLAN-A3E45T ]; then
        domain=${escapeShellArg (head server)}
      else
        domain=${escapeShellArg (last server)}
      fi
    '' else "domain=${escapeShellArg server}";

    # Unfold a string into a list of its parent path components
    unfold = string: let new = match "(.*)/.*" string;
      in if new == null then [] else unfold (head new) ++ new;

    parts = partition (hasInfix "/") include;

    sources = parts.wrong ++ (map (x: head (splitString "/" x)) parts.right);

    tmp = map unfold parts.right;

    gen-includes = unique (filter (hasInfix "/") (flatten tmp));

    gen-excludes = unique (concatMap reverseList tmp);

    # build a list of --include or --exclude arguments
    f = opt: list: concatMapStrings (x: " --${opt} /"+escapeShellArg x) list;
    includes = f "include" (gen-includes ++ parts.right);
    excludes = f "exclude" (exclude ++ map (x: x+"/*") gen-excludes);
  in

{
  script = ''
    ${pkgs.gnumake}/bin/make -C ~/.config/notmuch dump

    ${setup-code}

    ${../files/rsync.sh} ${optionalString retry "-l"} \
    -d "$domain":backup/${escapeShellArg target-name} -- --verbose \
    ${escapeShellArgs sources} ${includes} ${excludes}
  '';
  path = with pkgs; [bash rsync openssh x11_ssh_askpass notmuch gzip];
  environment = {
    NOTMUCH_CONFIG = "%h/.config/notmuch/default/config";
    SSH_AUTH_SOCK = "%t/ssh-agent";
  };
  serviceConfig = optionalAttrs retry {
    Restart = "on-failure";
    RestartSec = "1h";
  };
}
