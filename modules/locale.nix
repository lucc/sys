{ config, lib, ... }:
let
  inherit (import ./lib.nix lib) mkFalseOption;
in
{
  options.lucc.locale =
    mkFalseOption "whether to enable default locale settings and keymaps";
  config = lib.mkIf config.lucc.locale {
    # use the same keymap as the xserver if the xserver is enabled, otherwise
    # just use "de"
    console.useXkbConfig = config.services.xserver.enable;
    console.keyMap = lib.mkIf (!config.services.xserver.enable) "de";

    i18n.defaultLocale = "en_US.UTF-8";

    time.timeZone = "Europe/Berlin";
  };
}
