# password manager, gpg and ssh-agent and similar stuff
{ pkgs, lib, ... }:
let inherit (import ./lib.nix lib) xdgConfig; in
{
  programs.gnupg.agent.enable = true;
  programs.gnupg.agent.pinentryPackage = pkgs.pinentry-qt;
  programs.gnupg.agent.settings = {
    default-cache-ttl = 7200; # 2 hours
    max-cache-ttl = 86400; # one day
  };
  programs.ssh.startAgent = true;
  environment.systemPackages = with pkgs; [
    git
    lxqt.lxqt-openssh-askpass
    pass
    pwgen
  ];
  environment.variables = {
    PASSWORD_STORE_DIR = xdgConfig "pass";
    PASSWORD_STORE_ENABLE_EXTENSIONS = "true";
    # We have to use a symlink from ~/.gnupg to ~/.config/gpg because this
    # variable messes with with socket path that gpg uses and then the agent
    # defined above will not be used:
    # https://discourse.nixos.org/t/how-to-make-gpg-use-the-agent-from-programs-gnupg-agent/11834
    # https://github.com/jtojnar/nixfiles/commit/ebd6118dccf5762955aff75b6033fc142d282ae8
    # https://github.com/gpg/gnupg/blob/c6702d77d936b3e9d91b34d8fdee9599ab94ee1b/common/homedir.c#L663-L666
    #GNUPGHOME = xdgConfig "gpg";
  };

  services.passSecretService.enable = true;
  systemd.user.services."dbus-org.freedesktop.secrets"={
    environment.PASSWORD_STORE_DIR = "%h/.config/pass";
    serviceConfig.ExecStart= "${pkgs.pass-secret-service}/bin/pass_secret_service --path \"$PASSWORD_STORE_DIR\"";
  };
}
