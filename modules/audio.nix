{ pkgs, config, ... }:
let
  inherit (pkgs.lib.lists) optionals;
  audio-control = with pkgs; [
    pavucontrol
    (runCommand "audio-mixer" {} ''
      mkdir -p $out/bin
      ln -s ${pavucontrol}/bin/pavucontrol $out/bin/audio-mixer
    '')
  ];
in
{
  services.pipewire.enable = true;
  services.pipewire.alsa.enable = true;
  services.pipewire.pulse.enable = true;

  environment.systemPackages = with pkgs; [
    # pulse audio command line stuff because I do not understand pw-cli
    pulsemixer
    pulseaudio
  ] ++ optionals config.services.xserver.enable audio-control;
}
