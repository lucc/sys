# configuration of nix itself
{ self, pkgs, config, ... }:

let
  low-prio = {
    Nice = 19;
    IOSchedulingClass = "idle";
    IOSchedulingPriority = 7;
  };
in
{
  nix.settings.experimental-features = "nix-command flakes ca-derivations";

  # Garbage collection with clean up of old generations (and ultimately /boot)
  nix.gc.automatic = true;
  nix.gc.dates = "weekly";
  nix.optimise.automatic = true;
  nix.optimise.dates = ["weekly"];
  # set some options to reduce priority for generated services
  systemd.services.nix-gc.serviceConfig = low-prio;
  systemd.services.nix-optimise.serviceConfig = low-prio;
  # Remove some old system generations before running gc
  # Remove very old result links lying around
  systemd.services.nix-gc.preStart = ''
    ${config.nix.package}/bin/nix-store --gc --print-roots \
    | ${pkgs.gawk}/bin/awk '/^\/nix\/var\/nix\/profiles\// {print $1}' \
    | sort -t - -k 2 -n -r \
    | xargs --no-run-if-empty stat --format '%n %W' \
    | ${pkgs.gawk}/bin/awk -v date="$(date --date="1 month ago" +%s)" '
        NR < 10 { next }       # keep at least 10 backups
        $2 < date { print $1 } # remove everything older than date
    ' \
    | xargs --no-run-if-empty rm -v
    ${config.nix.package}/bin/nix-store --gc --print-roots \
    | ${pkgs.gawk}/bin/awk '/^\/[^ ]*\/result(-[0-9]*)? -> .*/ && ! /^\/nix\// {print $1}' \
    | xargs --no-run-if-empty stat --format '%n %W' \
    | ${pkgs.gawk}/bin/awk -v date="$(date --date="1 year ago" +%s)" '
        $2 < date { print $1 } # remove everything older than date
    ' \
    | xargs --no-run-if-empty rm -v
  '';
  # Check if nix-gc or nixos-upgrade is also running and wait until it finished
  systemd.services.nix-optimise.after = ["nix-gc.service" "nixos-upgrade.service"];
  systemd.services.nix-gc.after = ["nixos-upgrade.service"];

  # convenience for flakes
  nix.registry.sys.flake = self;

  # Add the commit hash of the system config to the label of the nixos
  # generation
  system.nixos.tags = ["config.${self.sourceInfo.shortRev or self.sourceInfo.dirtyShortRev}"];
  environment.etc."nixos-active.json".text =
    builtins.toJSON (pkgs.lib.attrsets.filterAttrs (n: _: n != "outPath") self.sourceInfo);
  environment.etc.nixos-active.source = self;
  system.configurationRevision = pkgs.lib.mkIf (self ? rev) self.rev;
}
