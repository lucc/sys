{
  pkgs,
  lib,
  ...
}: {
  programs.htop.enable = true;
  programs.htop.settings = lib.mkDefault {
    # for column_meter_modes_* 1 means bars, 2 means text
    header_layout = "two_50_50";
    column_meters_0 = "AllCPUs Memory Swap";
    column_meter_modes_0 = "1 1 1";
    column_meters_1 = "Tasks LoadAverage Uptime Battery DiskIO NetworkIO";
    column_meter_modes_1 = "2 2 2 2 2 2";
    hide_kernel_threads = true;
    hide_userland_threads = true;
    shadow_other_users = false;
    show_thread_names = false;
    show_program_path = false;
    highlight_base_name = true;
    highlight_deleted_exe = true;
    highlight_megabytes = true;
    highlight_threads = true;
    highlight_changes = true;
    find_comm_in_cmdline = true;
    strip_exe_from_cmdline = true;
    show_merged_command = false;
    header_margin = true;
    screen_tabs = false;
    detailed_cpu_time = true;
    cpu_count_from_one = true;
    show_cpu_usage = true;
    show_cpu_frequency = true;
    show_cpu_temperature = true;
    degree_fahrenheit = false;
    color_scheme = 6;
    enable_mouse = true;
    hide_function_bar = true;
    tree_view = true;
    sort_direction = 1;
    tree_sort_direction = 1;
    tree_view_always_by_pid = false;
  };
  environment.systemPackages = [
    (pkgs.makeDesktopItem {
      name = "sudo-htop";
      desktopName = "Htop";
      genericName = "Process Viewer (root)";

      type = "Application";
      comment = "Show System Processes (root)";
      icon = "htop";
      exec = "sudo htop";
      terminal = true;
      categories = ["System" "Monitor" "ConsoleOnly"];
      keywords = ["system" "process" "task"];
    })
  ];
}
