{
  environment.etc."xdg/user-dirs.defaults".text = ''
    XDG_DESKTOP_DIR="$HOME/"
    XDG_DOCUMENTS_DIR="$HOME/"
    XDG_DOWNLOAD_DIR="$HOME/"
    XDG_MUSIC_DIR="$HOME/audio"
    XDG_PICTURES_DIR="$HOME/img"
    XDG_PUBLICSHARE_DIR="$HOME/pub"
    XDG_TEMPLATES_DIR="$HOME/"
    XDG_VIDEOS_DIR="$HOME/vid"
  '';

  xdg.mime.defaultApplications = {
    "application/epub+zip" = "org.pwmt.zathura.desktop";
    "application/ogg" = "mpv.desktop";
    "application/pdf" = "org.pwmt.zathura.desktop";
    "application/postscript" = "org.pwmt.zathura.desktop";
    "application/ps" = "org.pwmt.zathura.desktop";

    "application/vnd.oasis.opendocument.text" = "writer.desktop";
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document" = "writer.desktop";
    "application/x-extension-htm" = "org.qutebrowser.qutebrowser.desktop";
    "application/x-extension-html" = "org.qutebrowser.qutebrowser.desktop";
    "application/x-extension-shtml" = "org.qutebrowser.qutebrowser.desktop";
    "application/x-extension-xht" = "org.qutebrowser.qutebrowser.desktop";
    "application/x-extension-xhtml" = "org.qutebrowser.qutebrowser.desktop";
    "application/x-iso9660-image" = "vlc.desktop";
    "application/x-ogg" = "mpv.desktop";
    "application/xhtml+xml" = "org.qutebrowser.qutebrowser.desktop";

    "audio/AMR" = "mpv.desktop";
    "audio/aac" = "mpv.desktop";
    "audio/ac3" = "mpv.desktop";
    "audio/flac" = "mpv.desktop";
    "audio/m4a" = "mpv.desktop";
    "audio/mp1" = "mpv.desktop";
    "audio/mp2" = "mpv.desktop";
    "audio/mp3" = "mpv.desktop";
    "audio/mp4" = "mpv.desktop";
    "audio/mpeg" = "mpv.desktop";
    "audio/mpegurl" = "mpv.desktop";
    "audio/mpg" = "mpv.desktop";
    "audio/ogg" = "mpv.desktop";
    "audio/rn-mpeg" = "mpv.desktop";
    "audio/scpls" = "mpv.desktop";
    "audio/vnd.rn-realaudio" = "mpv.desktop";
    "audio/wav" = "mpv.desktop";
    "audio/x-aac" = "mpv.desktop";
    "audio/x-ape" = "mpv.desktop";
    "audio/x-flac" = "mpv.desktop";
    "audio/x-m4a" = "mpv.desktop";
    "audio/x-mp1" = "mpv.desktop";
    "audio/x-mp2" = "mpv.desktop";
    "audio/x-mp3" = "mpv.desktop";
    "audio/x-mpeg" = "mpv.desktop";
    "audio/x-mpegurl" = "mpv.desktop";
    "audio/x-mpg" = "mpv.desktop";
    "audio/x-ms-wma" = "mpv.desktop";
    "audio/x-pls" = "mpv.desktop";
    "audio/x-pn-realaudio" = "mpv.desktop";
    "audio/x-pn-windows-pcm" = "mpv.desktop";
    "audio/x-realaudio" = "mpv.desktop";
    "audio/x-scpls" = "mpv.desktop";
    "audio/x-shorten" = "mpv.desktop";
    "audio/x-tta" = "mpv.desktop";
    "audio/x-vorbis+ogg" = "mpv.desktop";
    "audio/x-wav" = "mpv.desktop";
    "audio/x-wavpack" = "mpv.desktop";

    "image/bmp" = "feh.desktop";
    "image/gif" = "feh.desktop";
    "image/jpeg" = "feh.desktop";
    "image/jpg" = "feh.desktop";
    "image/pjpeg" = "feh.desktop";
    "image/png" = "feh.desktop";
    "image/tiff" = "feh.desktop";
    "image/x-bmp" = "feh.desktop";
    "image/x-pcx" = "feh.desktop";
    "image/x-png" = "feh.desktop";
    "image/x-portable-anymap" = "feh.desktop";
    "image/x-portable-bitmap" = "feh.desktop";
    "image/x-portable-graymap" = "feh.desktop";
    "image/x-portable-pixmap" = "feh.desktop";
    "image/x-tga" = "feh.desktop";
    "image/x-xbitmap" = "feh.desktop";

    "img/jpeg" = "feh.desktop";
    "img/jpg" = "feh.desktop";
    "img/png" = "feh.desktop";

    "text/htm" = "org.qutebrowser.qutebrowser.desktop";
    "text/html" = "org.qutebrowser.qutebrowser.desktop";

    "video/mp2t" = "mpv.desktop";
    "video/mp4" = "mpv.desktop";
    "video/mpeg" = "mpv.desktop";
    "video/msvideo" = "mpv.desktop";
    "video/ogg" = "mpv.desktop";
    "video/quicktime" = "mpv.desktop";
    "video/vnd.rn-realvideo" = "mpv.desktop";
    "video/webm" = "mpv.desktop";
    "video/x-avi" = "mpv.desktop";
    "video/x-fli" = "mpv.desktop";
    "video/x-flv" = "mpv.desktop";
    "video/x-matroska" = "mpv.desktop";
    "video/x-mpeg" = "mpv.desktop";
    "video/x-mpeg2" = "mpv.desktop";
    "video/x-ms-afs" = "mpv.desktop";
    "video/x-ms-asf" = "mpv.desktop";
    "video/x-ms-wmv" = "mpv.desktop";
    "video/x-ms-wmx" = "mpv.desktop";
    "video/x-ms-wvxvideo" = "mpv.desktop";
    "video/x-msvideo" = "mpv.desktop";
    "video/x-ogm+ogg" = "mpv.desktop";
    "video/x-theora" = "mpv.desktop";

    "x-scheme-handler/chrome" = "firefox.desktop";
    "x-scheme-handler/file" = "org.qutebrowser.qutebrowser.desktop";
    "x-scheme-handler/ftp" = "org.qutebrowser.qutebrowser.desktop";
    "x-scheme-handler/http" = "org.qutebrowser.qutebrowser.desktop";
    "x-scheme-handler/https" = "org.qutebrowser.qutebrowser.desktop";
    "x-scheme-handler/mailto" = "alot-mailto.desktop";
  };
}
