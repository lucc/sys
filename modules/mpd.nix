{
  pkgs,
  config,
  lib,
  ...
}: let
  inherit (import ./lib.nix lib) mkFalseOption;
  cfg = config.lucc.mpd;
  username = config.lucc.mainUser;
  user = config.users.users.${username};
  dir = "${user.home}/audio";
in {
  options.lucc.mpd = mkFalseOption ''
    whether to set up mpd with some custom settings
  '';
  config = lib.mkIf cfg {
    environment.systemPackages = [
      pkgs.mpc_cli
    ];

    services.mpd.enable = true;
    services.mpd.musicDirectory = dir;
    services.mpd.startWhenNeeded = true;
    services.mpd.user = username;
    services.mpd.group = user.group;
    services.mpd.extraConfig = ''
      audio_output {
        type    "pulse"
        name    "My MPD PulseAudio Output"
      }
    '';
    #server  "localhost"   # optional
    #sink    "alsa_output" # optional

    # https://askubuntu.com/a/639863/358033
    # TODO use config.users.users.${config.lucc.mainUser}.uid for this
    systemd.services.mpd.environment.XDG_RUNTIME_DIR = "/run/user/1000";

    home-manager.users.${config.lucc.mainUser} = {
      programs.ncmpcpp = {
        enable = true;
        mpdMusicDir = dir;
        settings = {
          fetch_lyrics_for_current_song_in_background = "yes";
          store_lyrics_in_song_dir = "yes";
        };
      };
      xdg.desktopEntries.music-player = {
        name = "MPD client";
        genericName = "Music player";
        terminal = true;
        exec = "ncmpcpp";
        categories = ["Application" "Music"];
        icon = "${pkgs.mpd}/share/icons/hicolor/scalable/apps/mpd.svg";
      };
    };
  };
}
