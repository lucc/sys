{
  pkgs,
  lib,
  ...
}: {
  environment.systemPackages = with pkgs; [
    colordiff
  ];

  programs.zsh.interactiveShellInit = lib.mkAfter ''
    compdef colordiff=diff
  '';

  environment.shellAliases = {
    diff = "colordiff";
    d = "diff-wrapper.bash";
  };

  # This is adapted from the example file in the colordiff distribution.
  # Available colours are: white, yellow, green, blue, cyan, red, magenta,
  # black, darkwhite, darkyellow, darkgreen, darkblue, darkcyan, darkred,
  # darkmagenta, darkblack
  #
  # Can also specify 'none', 'normal' or 'off' which are all aliases for the
  # same thing, namely "don't colour highlight this, use the default output
  # colour"
  environment.etc.colordiffrc.text = ''
    banner=no
    plain=off
    newtext=darkgreen
    oldtext=darkred
    diffstuff=blue
    cvsstuff=darkyellow
    difffile=darkcyan
  '';
}
