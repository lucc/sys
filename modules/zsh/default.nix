{ self, pkgs, config, lib, ... }:
let
  inherit (import ../lib.nix lib) xdgConfig xdgData xdgCache;
  nixpkgs-grep = pkgs.writeScriptBin "nixpkgs" ''
    ${pkgs.ripgrep}/bin/rg --ignore-case "$@" ${self.inputs.nixpkgs}
  '';
  nix-store-ls = pkgs.writeScriptBin "store-ls" ''
    ls -d --color=auto /nix/store/*-$1*
  '';
  zsh-functions = pkgs.runCommand "zsh-functions" {} ''
    mkdir $out
    ${pkgs.zsh}/bin/zsh -c 'zcompile $out/functions.zwc ${./functions}/*'
  '';
  lua-env = pkgs.lua5_4.withPackages (p: [p.inspect]);
  lua-repl = pkgs.writeScriptBin "luarepl" ''
    ${lua-env}/bin/lua -l inspect -e 'i = inspect.inspect' -i "$@"
  '';
  bindkey = args: ''
    bindkey -M viins ${args}
    bindkey -M vicmd ${args}
  '';
in
{
  imports = [
    ./diff.nix
    ./prompt.nix
  ];

  programs.zsh = {
    enable = true;
    enableGlobalCompInit = false;
    # We set a *very* large history limit and never delete any duplicates from
    # it.  We can then analyse it later.  Duplicates are saved but not
    # presented during history search.
    histSize = 1500000;
    histFile = xdgCache "zsh/history";
    setOptions = [
      ## history related options
      "HIST_IGNORE_DUPS"
      "HIST_FCNTL_LOCK"
      #"HIST_IGNORE_ALL_DUPS"
      #"HIST_EXPIRE_DUPS_FIRST"
      "HIST_FIND_NO_DUPS"
      "HIST_REDUCE_BLANKS"
      #"HIST_SAVE_NO_DUPS"
      "HIST_IGNORE_SPACE"
      #"HIST_VERIFY"
      "SHARE_HISTORY"
      "EXTENDED_HISTORY"

      ## misc options
      "EXTENDED_GLOB"
      "NO_NO_MATCH"
      "PROMPT_SUBST"

      ## interesting options
      "AUTO_CD"
      "GLOB_DOTS"
      #"PRINT_EXIT_VALUE"
      "NO_LIST_AMBIGUOUS"
      #"CORRECT_ALL"
    ];

    loginShellInit = ''
      # add some user folders to $PATH if they are not already present
      function {
        local dir
        for dir; if [[ -d "$dir" ]]; then
          PATH=$dir:$PATH
        fi
      } \
        ~/.config/composer/vendor/bin \
        ~/.cabal/bin                  \
        ~/.cargo/bin                  \
        ~/.gem/ruby/*/bin(N[1])       \
        ~/.luarocks/bin               \
        ~/.local/bin                  \
        ~/bin                         \

      typeset -U path PATH

      # set DISPLAY on the server side of ssh connections
      if [[ -z $DISPLAY && -n $SSH_CLIENT ]]; then
        export DISPLAY=''${SSH_CLIENT%% *}:0.0
      fi

      # force some programs to load their configuration from ~/.config
      [[ -r "${xdgCache "netrc"}" ]] && export NETRC=${xdgCache "netrc"}
    '';
    #syntaxHighlighting.enable = true;

    interactiveShellInit = ''
      # autoloading user defined functions ###################################
      fpath=(${zsh-functions}/functions.zwc $fpath)
      autoload -Uz ${./functions}/*(:t)

      # syntax highlighting ##################################################
      # should be placed before syntax highlighting
      source ${pkgs.zsh-autopair}/share/zsh/zsh-autopair/autopair.zsh
      source ${pkgs.zsh-fast-syntax-highlighting}/share/zsh/site-functions/fast-syntax-highlighting.plugin.zsh
      zle_highlight=(
        region:bg=green
        special:bg=blue
        suffix:fg=red
        isearch:fg=yellow
      )

      # autojump #############################################################
      source ${pkgs.autojump}/share/zsh/site-functions/autojump.zsh
      #export AUTOJUMP_KEEP_SYMLINKS=1

      # fzf ##################################################################
      # set up fzf keybindings
      FZF_ALT_C_COMMAND='sort --reverse --numeric-sort ~/.local/share/autojump/autojump.txt | cut --fields=2'
      eval "$(${lib.getExe pkgs.fzf} --zsh)"
      function fzf-cd-widget-2 () {
        FZF_DEFAULT_OPTS="$FZF_DEFAULT_OPTS --bind=ctrl-space:close" fzf-cd-widget
      }
      zle -N fzf-cd-widget-2
      ${bindkey "'^ ' fzf-cd-widget-2"}

      # legacy zshrc file ####################################################
      ${builtins.readFile ./zshrc}
      ''
      #source ${pkgs.zsh-nix-shell}/share/zsh-nix-shell/nix-shell.plugin.zsh
      ;

  };
  # force some programs to load their configuration from ~/.config
  environment.variables = with pkgs; {
    FZF_DEFAULT_OPTS = "--inline-info --cycle";
    # in case I ever use wine
    WINEPREFIX = xdgData "wine";
    # force some programs to load their configuration from ~/.config
    ELINKS_CONFDIR = xdgConfig "elinks";
    RLWRAP_HOME = xdgData "rlwrap";
    # pager stuff
    HTMLPAGER = "${elinks}/bin/elinks --dump";
    PAGER = "${self.packages.${pkgs.system}.nvimpager}/bin/nvimpager";
    SYSTEMD_LESS = "FRXMK";
  };
  environment.systemPackages = with pkgs; [
    # zsh plugins
    zsh-completions
    #zsh-nix-shell
    #zsh-history-substring-search zsh-autosuggestions
    nix-zsh-completions
    # basic shell stuff to feel compfy
    atool
    autojump
    elinks
    fd
    fdupes
    file
    fzf
    killall
    moreutils
    pstree
    renameutils
    ripgrep
    rlwrap
    tree
    unzip
    zip

    nixpkgs-grep
    nix-store-ls
    lua-repl
  ];

  users.users.${config.lucc.mainUser}.shell = pkgs.zsh;

  environment.shellAliases = {
    # directory listings
    ls = "ls --color=auto";
    l = "ls -l";
    lh = "l -h";
    la = "ls -A";
    lla = "l -A";

    # basic command line tools
    cp = "cp -ip";
    mv = "mv -i";
    grep = "grep --extended-regexp --color=auto --ignore-case";
    mkdir = "mkdir -p";
    scp = "scp -p";
    qmv = "qmv --format=destination-only";

    # pim utils
    a = "khard";
    k = "ikhal";
    m = "alot";
    mm = "purebred";

    # dates and timestamps
    D = "date +%F";
    T = "date +%T";
    isodate = "date '+%F %T'";
    timestamp = "date +%Y%m%d%H%M%S";

    # coding and git stuff
    s = "rg --smart-case";
    g = "hub";
    tt = "tig --all";
    "?" = ''
      if [[ $(git rev-parse --is-inside-work-tree 2>/dev/null) = true ]]; then
        git status --short
      else
        echo Not under version control.
      fi'';

    # special for nix and nixos
    nix-roots = ''
      nix-store --gc --print-roots \
        | egrep -v "^(/nix/var|/run/current-system|/run/booted-system|/proc|{memory|{censored)"'';
    nix-homepage = "nix eval --raw --apply 'x: x.meta.homepage' > >(xargs xdg-open &>/dev/null)";

    # misc
    histhead = ''
      history -n 1 \
        | awk '{ a[$1]++ } END { for (i in a) { print a[i]" "i } }' \
        | sort -rn \
        | head'';
    py = "ipython";
    hs = "ghci";
    ffprobe = "ffprobe -hide_banner";
    myip = "dig +short myip.opendns.com A @resolver1.opendns.com";
    blk = "lsblk -o NAME,FSTYPE,MOUNTPOINT,PARTLABEL,SIZE,RO";
    lima = "ftp --netrc=$NETRC luc42.lima-ftp.de";
    music = "ncmpcpp -q";
    hgrep = "history 1 | grep";
    quoted-printeable-decode = ''python -c "import os,quopri,sys;quopri.decode(sys.stdin,os.fdopen(sys.stdout.fileno(), 'wb'))"'';
  };

  programs.direnv.enable = true;
}
