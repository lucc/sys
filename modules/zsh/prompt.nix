{ lib, ... }:
{
  # This is just concatenated into the /etc/zshrc file like
  # interactiveShellInit but we still put our prompt setup here for semantical
  # reasons (and to overwrite the default).
  programs.zsh.promptInit = builtins.readFile ./prompt.zsh;

  # Setup for startup profiling and the execution timer in the right prompt.
  # Get the current time for startup profiling at the top of /etc/zshrc
  environment.etc.zshrc.text = lib.mkBefore ''
    _start=''${(%):-%D{%s.%.}}
    _diff=
  '';
}
