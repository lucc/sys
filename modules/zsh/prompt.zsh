# set up the vcs_info plugin for the prompt
zstyle ':vcs_info:*' actionformats '%F{cyan}%s%F{green}%c%u%b%F{blue}%a%f'
####TODO
zstyle ':vcs_info:*' formats       '%F{cyan}%s%F{green}%c%u%b%f'
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{3}%r'
zstyle ':vcs_info:*' enable git svn cvs hg
# change color if changes exist (with %c and %u)
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' stagedstr '%F{yellow}'
zstyle ':vcs_info:*' unstagedstr '%F{red}'

autoload -Uz add-zsh-hook
autoload -Uz vcs_info
add-zsh-hook precmd vcs_info

# register the functions with the correct hooks
zstyle ':vcs_info:git+set-message:*' hooks \
  git-add-untracked-files                  \
  git-string                               \

zstyle ':vcs_info:hg+set-message:*' hooks hg-string

# Right hand side prompt
local time_segment='%F{yellow}⌛$_diff'
typeset -gA _keymap_prompt_strings
_keymap_prompt_strings=(
  main       ''
  vicmd      '%F{yellow}CMD%f '
  visual     '%F{blue}VIS%f '
  viopp      '%F{red}OPP%f '
  listscroll '%F{red}LIST%f '
  command    '%F{red}CMD?%f '
  .safe      '%F{red}SAFE%f '
  menuselect '%F{red}SEL%f '
  isearch    '%F{red}I/%f '
  viins      '%F{red}INS%f '
  emacs      '%F{red}EMACS%f '
)
KEYTIMEOUT=1
RPROMPT='$_keymap_prompt_strings[$KEYMAP]'       # INSERT|CMD|...
RPROMPT+='%(?|'                                  # if $? == 0
RPROMPT+='%(${_threshold}S|'                     #   if _threshold < SECONDS
RPROMPT+=$time_segment                           #     duration of command
RPROMPT+='|)'                                    #   else, fi
if [[ ${(L)TERM} = linux ]]; then                #   if console
  RPROMPT+=' $(battery.sh -bce zsh)'             #     battery information
fi                                               #   fi
RPROMPT+='|'                                     # else
RPROMPT+='%(127?.'                               #   if $? == 127
RPROMPT+='%F{red}'                               #     switch color
RPROMPT+='∄'                                     #     command not found
RPROMPT+='.'                                     #   else
RPROMPT+="$time_segment "                        #     duration of command
RPROMPT+='%F{red}'                               #     switch color
RPROMPT+='✘%?'                                   #     error message
RPROMPT+=')'                                     #   fi
RPROMPT+=')'                                     # fi

# Finally we prepare the dependencies for these prompt segments
autoload -Uz add-zsh-hook
# define the needed variables
typeset -ig _threshold
typeset -g _start
typeset -g _diff
# define the needed functions
function zle-keymap-select zle-line-init {
  zle reset-prompt
}
zle -N zle-keymap-select
zle -N zle-line-init
_threshold=0
# add the function to a hook
add-zsh-hook preexec execution-time-helper-function
add-zsh-hook precmd execution-time-formatter

# Setup for the main prompt
PS1=
if [[ -n $SSH_CONNECTION ]] || systemd-detect-virt --quiet; then
  PS1+='%(!.%F{red}.%F{green})'                   # user=green, root=red
  PS1+='%n%F{cyan}@%F{blue}%m%f:'                 # user and host info
else
  PS1+='%(!.%F{red}%n%F{cyan}@%F{blue}%m%f:.)'    # user=green, root=red
fi
PS1+='%F{cyan}%1~%f'                              # working directory
PS1+='${vcs_info_msg_0_:+($vcs_info_msg_0_)}'     # VCS info with delim.
PS1+='%# '
