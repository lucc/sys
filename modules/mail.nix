# Settings for the main user to do mail stuff
{ pkgs, pkgs', config, lib, self, ... }:

let
  inherit (import ./lib.nix lib) mkFalseOption;
  inherit (lib) mkOption types mkIf attrsets;
  inherit (builtins) elem attrNames attrValues;
  cfg = config.lucc.mail;
  emacs = pkgs.emacs-nox.pkgs.withPackages(e: [e.notmuch]);
  uis = with pkgs; {
    alot = pkgs'.alot;
    inherit astroid;
    bower = notmuch-bower;
    purebred = self.inputs.purebred.packages.${system}.purebred-with-packages-icu;
    emacs  = pkgs.writeScriptBin "notmuch.el" "${emacs}/bin/emacs -f notmuch";
  };
in

{
  options.lucc.mail = {
    enable = mkFalseOption ''
      whether to enable the mail config with fetchmail, msmtp and notmuch
    '';
    userInterfaces = mkOption {
      type = types.listOf (types.enum (attrNames uis));
      default = [ "alot" ];
      example = [ "astroid" "bower" ];
      description = "the notmuch user interfaces to install";
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      afew
      msmtp
      notmuch
      runningx  # for the mailcap file
      self.inputs.abq.defaultPackage.${system}
      urlscan
    ] ++ attrValues (attrsets.filterAttrs (n: v: elem n cfg.userInterfaces) uis);

    systemd.user.services.fetchmail = {
      description = "Fetchmail Daemon";
      script = ''
      exec ${pkgs.fetchmail}/bin/fetchmail --daemon 60 --nodetach --nosyslog \
      --fetchmailrc - < <(${pkgs.pass}/bin/pass fetchmailrc)
      '';
      serviceConfig = {
        ExecStop = "${pkgs.fetchmail}/bin/fetchmail --quit";
        # this does not reload the config file but trigger fetchmail to look
        # for new mail outside of the daemon interval
        ExecReload = "${pkgs.fetchmail}/bin/fetchmail";
        Restart = "always";
        RestartSec = 10;
        SyslogIdentifier = "fetchmail";
      };
      # I think dbus should be included if I specify awesome as awesome-client
      # depends on dbus-send but it seems not to be.
      path = with pkgs; [ notmuch afew gawk pass ]
      ++ (if elem "awesome" config.lucc.gui then [ awesome dbus ] else []);
      environment = {
        PASSWORD_STORE_DIR = "%h/.config/pass";
        PASSWORD_STORE_ENABLE_EXTENSIONS = "true";
        FETCHMAILHOME = "%t";
        NOTMUCH_CONFIG = "%h/.config/notmuch/default/config";
      };
      wantedBy = [ "default.target" ];
      requires = [ "gpg-agent.socket" ];
    };

    environment.etc.mailcap.source =
      pkgs.runCommand "mailcap" {} ''
        substitute ${../files/mailcap} $out \
          --replace mailcap.sh ${../files/mailcap.sh} \
          --replace mutt-ics ${pkgs.mutt-ics}/bin/mutt-ics
    '';

    age.secrets.msmtprc.owner = config.lucc.mainUser;
    environment.etc.msmtprc.source = config.age.secrets.msmtprc.path;
  };
}
