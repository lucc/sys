{ pkgs, config, lib, ... }:
let
  cfg = config.lucc.printing;
  inherit (lib) mkIf mkEnableOption;
in
{
  options.lucc.printing = mkEnableOption "printing";
  config = mkIf cfg {
    # Enable CUPS to print documents.
    services.printing.enable = true;
    services.printing.drivers = [ pkgs.hplip ];
    users.users.${config.lucc.mainUser}.extraGroups = [ "lp" ];
    environment.systemPackages = [ pkgs.cups ];
  };
}
