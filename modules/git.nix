{ pkgs, ... }:
let
  inherit (pkgs.lib) getExe;
  nvimdiff = pkgs.writeShellApplication {
    name = "nvimdiff";
    text = ''nvim -d "$@"'';
  };
  msmtp-sendmail = pkgs.writeShellApplication {
    name = "msmtp-sendmail";
    text = ''msmtp -v --file "$HOME/.config/msmtp/msmtprc" "$@"'';
  };
  gitignore = pkgs.writeText "gitignore" ''
    # global gitignore file by luc

    # swap files for vim
    .*.sw?

    # folder of latexrun
    latex.out/

    # python cache and bytecode
    __pycache__/
    *.egg-info/
    .eggs/
    .mypy_cache/
    # from python linters and stuff
    .ropeproject/

    # ignore files for ag(1)
    .agignore

    # Java class files
    *.class

    # Ignore direnv files as other people might use a different direnv
    # provider or no direnv at all
    .envrc
    .direnv/
  '';
in
{
  programs.git.enable = true;
  environment.systemPackages = with pkgs; [
    gh
    git-crypt
    git-ftp
    hub
    tig
  ];

  programs.git.config.user.name = "Lucas Hoffmann";

  programs.git.config.color.ui = true;
  programs.git.config."color \"grep\"".match = "red bold";
  programs.git.config."color \"grep\"".filename = "magenta";
  programs.git.config.core.excludesfile = gitignore;
  programs.git.config.credential.helper = "pass";

  programs.git.config.pull.rebase = false;
  programs.git.config.push.default = "simple";

  programs.git.config.alias = {
    pick = "cherry-pick";
    amend = "commit --amend --no-edit";
    ap = "add -p";
    cm = "commit -m";
    co = "checkout";
    root = "rev-parse --show-toplevel";
    fa = "fetch --all --prune";
    ls = "ls-files";
    nice-merge = ''!git merge --no-ff --no-ed "$@" && git branch -d'';
    stash2branch = ''!branch=$* && branch=''${branch// /-} && git stash branch "$branch" && git commit --message="$*" . && :'';
    repush = "!git fa && git rebase @{upstream} && git push";
    ra = "rebase --abort";
    rc = "rebase --continue";
    tig = "!tig";
    t = "!tig --all";
  };

  programs.git.config.advice = {
    mergeConflict = false;
    skippedCherryPicks = false;
    forceDeleteBranch = false;
  };

  programs.git.config.diff.tool = getExe nvimdiff;
  programs.git.config."diff \"gpg\"".binary = true;
  programs.git.config."diff \"gpg\"".textconv = "gpg --decrypt --quiet --batch";
  programs.git.config."diff \"image\"".command = "git-imgdiff";
  programs.git.config."difftool \"diffpdf\"".cmd = ''diffpdf "$LOCAL" "$REMOTE"'';
  programs.git.config."difftool \"diff-pdf\"".cmd = ''diff-pdf --view "$LOCAL" "$REMOTE"'';

  programs.git.config.format.attach = true;
  programs.git.config.format.thread = true;

  programs.git.config."filter \"lfs\"".smudge = "git-lfs smudge -- %f";
  programs.git.config."filter \"lfs\"".process = "git-lfs filter-process";
  programs.git.config."filter \"lfs\"".required = true;
  programs.git.config."filter \"lfs\"".clean = "git-lfs clean -- %f";
  programs.git.config."filter \"zeal-window-geometry\"".smudge = "cat";
  programs.git.config."filter \"zeal-window-geometry\"".clean = "sed -e '/^window_geometry=/d;/^splitter_geometry=/d;/^version=/d'";
  programs.git.config."trailer \"see\"".key = "See-also";
  programs.git.config."trailer \"see\"".command = ''git log -1 --oneline --format="%h (%s)" --abbrev-commit --abbrev=14 $ARG'';
  programs.git.config."trailer \"see\"".ifMissing = "doNothing";

  programs.git.config."trailer \"sign\"".key = "Signed-off-by";
  programs.git.config."trailer \"help\"".key = "Helped-by";
  programs.git.config.trailer.ifmissing = "doNothing";

  programs.git.config.sendmail.smtpserver = msmtp-sendmail;

  programs.git.config.hub.protocol = "ssh";

  # git attributes
  environment.etc.gitattributes.text = ''
    *.gpg diff=gpg
    *.gif diff=image
    *.jpg diff=image
    *.png diff=image
  '';

  # settings for tig
  programs.git.config.tig = {
    mouse = true;
    ignore-case = true;
    main-view = "line-number:no,interval=5 id:no date:default author:full,width=20 commit-title:yes,graph,refs,overflow=no";
  };
  programs.git.config."tig \"color\"" = {
    main-head = "red default bold";
    main-tracked = "red default";
    #main-remote = green default normal
  };
  programs.git.config."tig \"bind\"" = {
    generic = [
      ''<Ctrl-T> @git tag "%(prompt tag name: )" %(commit)''
      "C ?git cherry-pick %(commit)"
    ];
  };

}
