{ config, ... }:
let
  base0 = "#839496";
  base03 = "#002b36";
in
{
  home-manager.users.${config.lucc.mainUser} = { pkgs, ... }: {
    programs.alacritty.enable = true;
    programs.alacritty.settings = {
      # Solarized dark colors
      colors = {
        primary = {
          background = base03;
          foreground = base0;
        };

        cursor = {
          text = base03;
          cursor = base0;
        };

        normal = {
          black = "#073642"; # base02
          red = "#dc322f"; # red
          green = "#859900"; # green
          yellow = "#b58900"; # yellow
          blue = "#268bd2"; # blue
          magenta = "#d33682"; # magenta
          cyan = "#2aa198"; # cyan
          white = "#eee8d5"; # base2
        };

        bright = {
          black = base03;
          red = "#cb4b16"; # orange
          green = "#586e75"; # base01
          yellow = "#657b83"; # base00
          blue = base0;
          magenta = "#6c71c4"; # violet
          cyan = "#93a1a1"; # base1
          white = "#fdf6e3"; # base3
        };
      };

      font = {
        size = 9;
        #family: monospace
        #family: Liberation Mono
        #family: DejaVuSansMono Nerd Font Mono
        #family: DejaVu Sans Mono
        normal.family = "DejaVuSansM Nerd Font";
      };
    };

    # The state version is required and should stay at the version you
    # originally installed.
    home.stateVersion = "23.11";
  };
}
