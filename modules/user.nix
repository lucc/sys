# definition of the main user account
{ config, lib, ... }:

let
  inherit (lib) mkOption types mkIf;
  cfg = config.lucc.mainUser;
in

{
  options = {
    lucc.mainUser = mkOption {
      type = types.nullOr types.str;
      default = null;
      example = "lucc";
      description = ''
      The name of the main user on the system.  If this is null no main user
      will be installed.
      '';
    };
  };
  config = mkIf (!isNull cfg) {
    users.users.${cfg} = {
      isNormalUser = true;
      extraGroups = [
        # Enable ‘sudo’ for the user.
        "wheel"
      ];
    };
    services.getty.autologinUser = cfg;
    networking.wireless.userControlled.enable = true;
  };
}
