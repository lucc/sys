lib:
let inherit (lib) mkOption types; in
{
  mkFalseOption = description: mkOption {
    type = types.bool;
    default = false;
    example = true;
    inherit description;
  };
  xdgCache = path: "\${XDG_CACHE_HOME:-$HOME/.cache}/" + path;
  xdgConfig = path: "\${XDG_CONFIG_HOME:-$HOME/.config}/" + path;
  xdgData = path: "\${XDG_DATA_HOME:-$HOME/.local/share}/" + path;
}
