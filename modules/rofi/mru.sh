#!/bin/sh

rofi_cache=$HOME/.cache/rofi/xdg-open-mru-list.txt
zathura_cache=$HOME/.local/share/zathura/history

# update a cache file ($1) from a source ($2) file by calling a shell
# function ($3) iff the cache file is out of date
update () {
  if [ "$2" -nt "$1" ]; then
    "$3" < "$2" > "$1"
  fi
}
read_xdg_cache () {
  tac "$rofi_cache" \
    | sort --uniq --stable --key 2 \
    | sort -n -r
}
read_zathura_cache () {
  sed 's/^\[\(.*\)\]$/file=\1/p; /^time=[0-9]*$/p' -n "$zathura_cache" \
    | sed -n '/^file=\// { N; s/^file=\(.*\)\ntime=\([0-9]*\)$/\2 \1/;p; }' \
    | sort -n -r
}

if [ -z "$1" ]; then
  mkdir -p ~/.cache/rofi
  update ~/.cache/rofi/xdg-open.sorted "$rofi_cache" read_xdg_cache
  update ~/.cache/rofi/zathura.sorted "$zathura_cache" read_zathura_cache
  sort -n -r --merge ~/.cache/rofi/*.sorted \
    | cut -f2- -d ' ' \
    | stest -f
else
  xdg-open "$1" >/dev/null 2>&1 &
fi
