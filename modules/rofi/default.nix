{ config, pkgs, lib, ... }:
let
  ifMpd = lib.strings.optionalString config.lucc.mpd;
  guiEnabled = builtins.length config.lucc.gui > 0;
in
lib.mkIf guiEnabled {
  environment.systemPackages = with pkgs; [
    dmenu # for stest, used in the mru script
    man-db # for man and apropos
    moreutils # for sponge
    rofi
    xdg-utils # for xdg-open
  ];
  environment.etc."xdg/rofi.rasi".text = ''
    configuration {
      modi: "combi,${ifMpd "mpc:${./mpc.sh},"}shell:${./shell},window,run,mru:${./mru.sh},ssh";
      combi-modi: "window,mpc,drun,mru,run";

      matching: "fuzzy";
      show-icons: true;
      terminal: "term";

      kb-cancel: "Escape,Menu,Print,Super+BackSpace";

      timeout {
        delay: 15;
        action: "kb-cancel";
      }
    }

    @theme "solarized"
  '';
}
