{ config, pkgs, lib, ... }:

let
  inherit (import ./lib.nix lib) mkFalseOption;
  inherit (pkgs.lib.lists) optional;
  cfg = config.lucc.virtualisation;
  mainUser = config.lucc.mainUser;
  # workaround for https://github.com/NixOS/nixpkgs/issues/348938
  vagrant = pkgs.vagrant.override { withLibvirt = false; };
in

{
  options.lucc.virtualisation = {
    podman = mkFalseOption "enable podman and tools";
    docker = mkFalseOption "enable docker and tools";
    vagrant = mkFalseOption "enable vagrant with virtualbox";
  };
  config = {
    environment.systemPackages =
      optional cfg.podman pkgs.podman-compose
      ++ optional cfg.docker pkgs.docker-compose
      ++ optional cfg.vagrant vagrant;
    virtualisation.podman.enable = cfg.podman;
    virtualisation.docker.enable = cfg.docker;
    virtualisation.virtualbox.host = lib.mkIf cfg.vagrant {
      enable = true;
      headless = true;
    };
    users.extraGroups.vboxusers.members =
      optional (cfg.vagrant && !isNull mainUser) mainUser;
  };
}
