{ self, pkgs, ... }:
{
  environment.systemPackages = [
    self.inputs.agenix.packages.${pkgs.system}.agenix
  ];
  age.secrets."wpa_supplicant.conf".file = ../secrets/wpa_supplicant.conf;
  age.secrets."wpa_supplicant.conf".path = "/etc/wpa_supplicant.conf";
  age.secrets."wpa_supplicant.conf".symlink = true;
  networking.wireless.allowAuxiliaryImperativeNetworks = true;
  # Some open wifis
  networking.wireless.networks = {
    "@BayernWLAN" = {};
    FlixBus = {};
    "FlixBus Wi-Fi" = {};
    "FREE_WIFI@BAHNHOF" = {};
    "freeWIFIahead!" = {};
    "M-WLAN Free Wi-Fi" = {};
    OEBB = {};
    "WIFI@DB" = {};
    "WiFi@DB" = {};
    WIFIonICE = {};
    "38C3".auth = ''
      key_mgmt=WPA-EAP
      eap=TTLS
      identity="38C3"
      password="38C3"
      ca_cert="${builtins.fetchurl {
        url = "https://letsencrypt.org/certs/isrgrootx1.pem";
        sha256 = "sha256:1la36n2f31j9s03v847ig6ny9lr875q3g7smnq33dcsmf2i5gd92";
      }}"
      altsubject_match="DNS:radius.c3noc.net"
      phase2="auth=PAP"
    '';
  };
}
