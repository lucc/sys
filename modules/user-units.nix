# a collection of user systemd units
{
  pkgs,
  lib,
  config,
  ...
}: let
  backup-unit = import ./backup-unit.nix pkgs lib;
  common-excludes = [
    ".cabal"
    ".cache"
    ".local/share/Zeal"
    ".local/share/containers"
    ".stack"
    "mail/.notmuch/xapian"
  ];
in {
  systemd.user = {
    services = {
      backup-small = backup-unit {
        target-name = config.networking.hostName;
        server = ["pi" "home"];
        # list of folders in ~/ that should be included in the backup
        include = [
          ".config"
          ".local/share/khal"
          ".local/share/khard"
          ".local/share/qutebrowser"
          ".local/share/task"
          "bank"
          "cook"
          "doc"
          "etc"
          "mail"
          "src"
          "wohnung"
        ];
        exclude = common-excludes;
      };
      backup-full = backup-unit {
        target-name = "${config.networking.hostName}-full";
        include = ["."];
        server = "nas";
        retry = true;
        exclude = common-excludes;
      };
      ssh-add = let
        passdir = "%h/.config/pass";
      in {
        environment.PASSWORD_STORE_DIR = passdir;
        environment.SSH_ASKPASS = passdir + "/.extensions/ssh-askpass";
        environment.SSH_AUTH_SOCK = "%t/ssh-agent";
        script = ''
          for file in ~/.ssh/{*id_rsa,id_ecdsa,id_ed25519}; do
            if [ -f "$file" ]; then
              ssh-add "$file" < /dev/null
            fi
          done
        '';
        path = [pkgs.openssh pkgs.pass];
        requires = ["gpg-agent.socket" "ssh-agent.service"];
      };
      xcape.serviceConfig.ExecStart = "${pkgs.xcape}/bin/xcape -fe Caps_Lock=Escape -t 200";
    };
    timers = {
      backup-small = {
        timerConfig = {
          OnStartupSec = "10101"; # <3h
          OnUnitActiveSec = "3456"; # nearly an hour
        };
        wantedBy = ["timers.target"];
      };
      backup-full = {
        timerConfig = {
          OnCalendar = "monthly";
          Persistent = "true";
        };
        wantedBy = ["timers.target"];
      };
    };
  };
}
