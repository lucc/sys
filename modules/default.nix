{ pkgs, ... }:
{
  imports = [
    ./alacritty.nix
    ./audio.nix
    ./auto-upgrade.nix
    ./dict.nix
    ./editor.nix
    ./git.nix
    ./gui.nix
    ./htop.nix
    ./laptop.nix
    ./locale.nix
    ./mail.nix
    ./mpd.nix
    ./nas.nix
    ./networks.nix
    ./nix.nix
    ./password-manager.nix
    ./printing.nix
    ./tmux.nix
    ./user-units.nix
    ./user.nix
    ./virtualisation.nix
    ./xdg.nix
    ./zsh
  ];

  # some default packages
  environment.systemPackages = with pkgs; [
    # basic shell stuff to feel compfy
    gnumake graphviz jq pandoc wget rsync
    # networking and sysadmin stuff
    bind ddrescue lsof nload nmap parted
    # coding
    ghc hyperfine inotify-tools
    # desktop like stuff
    diffpdf imagemagick pdftk pdfgrep termtosvg yt-dlp
  ];

}
