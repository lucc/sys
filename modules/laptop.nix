{ pkgs, config, lib, ... }:

let
  inherit (import ./lib.nix lib) mkFalseOption;
  inherit (lib) mkOption types mkIf mkDefault attrsets lists;
  cfg = config.lucc.laptop;
  wpa-add-network = pkgs.writeScriptBin "add-network" ''
    set -u
    network=$(wpa_cli add_network | tail -n 1)
    wpa_cli <<EOF
    set_network $network ssid "''${1:?Please provide an SSID}"
    set_network $network psk "''${2:?Please provide a password}"
    select_network $network
    EOF
  '';
in

{
  options.lucc.laptop = {
    enable = mkFalseOption "wether to enable some laptop specific options";
    battery = mkOption {
      type = types.str;
      default = "BAT0";
      example = "BAT1";
      description = "the internal name of the main battery";
    };
    wifiDevices = mkOption {
      type = types.listOf types.str;
      default = [ ];
      example = [ "wlan0" "wlp1s0" ];
      description = "the names of the wifi devices to use";
    };
  };

  config = mkIf cfg.enable {

    # low battery handling taken from
    # https://gitlab.com/xaverdh/my-nixos-config/-/blob/619e8212808ad3a7ed7bddddccf878de1f0ef4ab/per-box/tux/default.nix#L85-104
    systemd.timers.suspend-on-low-battery = {
      wantedBy = [ "multi-user.target" ];
      timerConfig = {
        OnUnitActiveSec = "120";
        OnBootSec= "120";
      };
    };
    systemd.services.suspend-on-low-battery = {
      serviceConfig.Type = "oneshot";
      onFailure = [ "hybrid-sleep.target" ];
      script = ''
        test "$(cat /sys/class/power_supply/${cfg.battery}/status)" != Discharging \
          || test "$(cat /sys/class/power_supply/${cfg.battery}/capacity)" -ge 5
      '';
    };

    services.logind.extraConfig = "HandlePowerKey=lock";
    services.physlock.enable = true;
    services.physlock.allowAnyUser = true;
    # This should provide a lock unit for logind to handle the power key.
    systemd.services.lock.serviceConfig.ExecStart = "${pkgs.physlock}/bin/physlock";

    # modern laptops probably have a hdpi monitor
    #hardware.video.hidpi.enable = true;
    # for the backlight on the keyboard
    services.illum.enable = true;
    powerManagement.cpuFreqGovernor = mkDefault "powersave";

    networking = mkIf (builtins.length cfg.wifiDevices > 0) {
     # Enables wireless support via wpa_supplicant.
      wireless.enable = true;
      wireless.interfaces = cfg.wifiDevices;
      # The global useDHCP flag is deprecated, therefore explicitly set to
      # false here.  Per-interface useDHCP will be mandatory in the future, so
      # this generated config replicates the default behaviour.
      useDHCP = false;
      interfaces = attrsets.genAttrs cfg.wifiDevices (_: { useDHCP = true; });
    };

    environment.systemPackages = lists.optional config.networking.wireless.enable wpa-add-network;

    services.udisks2.enable = true;
  };
}
