-- the main menu

local awful = require("awful")
local beautiful = require("beautiful")
local hotkeys_popup = require("awful.hotkeys_popup").widget
local globals = require("globals")
local editor, terminal = globals.editor, globals.terminal

local awesomemenu = {
  { "hotkeys", function() return false, hotkeys_popup.show_help end },
  { "manual", terminal .. " -e man awesome" },
  { "edit config", editor .. " " .. awesome.conffile },
  { "restart", awesome.restart },
  { "quit", function() awesome.quit() end }
}
local keyboardmenu = {
  { "german", "setxkbmap de" },
  { "german neo", "setxkbmap de neo" },
}
local mainmenu = awful.menu({
  items = {
    { "awesome", awesomemenu, beautiful.awesome_icon },
    { "open terminal", terminal },
    { "switch keyboard layout", keyboardmenu }
  }
})

return mainmenu
