-- theme and color set up

local beautiful = require("beautiful") -- Theme handling library
local gears = require("gears")

beautiful.init(require"awesome-solarized/dark/theme")
beautiful.border_width = 1

-- Wallpaper {{{1
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
