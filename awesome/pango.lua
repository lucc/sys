-- helper functions to deal with pango markup

local theme = require("beautiful")

local pango = {}

---Mark up text with a pango tag
---@param tag string the tag name followed by attributes
---@param text string
local function markup(tag, text)
  local first = tag
  local index = string.find(tag, ' ')
  if index ~= nil then
    first = string.sub(tag, 1, index - 1)
  end
  return '<' .. tag .. '>' .. text .. '</' .. first .. '>'
end

---Escape pango markup characters
---@param text string
function pango.escape(text)
  local replacements = {
    ["&"] = "&amp;",
    ["<"] = "&lt;",
    [">"] = "&gt;",
    ["\""] = "&quot;",
    ["'"] = "&apos;",
  }
  return string.gsub(text, "[&<>'\"]", replacements)
end

---Change forground color of text
---@param col string the color name
---@param text string
function pango.color(col, text)
  if theme.colors[col] then
    col = theme.colors[col]
  end
  return markup('span color="' .. col .. '"', text)
end

---Change font of text
---@param font string the font specification
---@param text string
function pango.font(font, text)
  return markup('span font="' .. font .. '"', text)
end

---Display text with a monospace font
---@param text string
function pango.mono(text)
  return markup('span font_desc="monospace"', text)
end

---Display text with a icon enabed font (nerd font)
---@param text string
function pango.iconic(text)
  return pango.mono(pango.font('DejaVuSansM Nerd Font 11', text) .. " ")
end

---@type table<string,string>
local ansi_table = {}
for key, val in pairs { ["31"] = "red",
  ["32"] = "green",
  ["33"] = "yellow",
  ["34"] = "blue",
  ["35"] = "magenta",
  ["36"] = "cyan" } do
  ansi_table["\027[" .. key .. "m"] = "</span><span color='" .. theme.colors[val] .. "'>"
end
ansi_table["\027[0m"] = "</span><span color='" .. theme.fg_normal .. "'>"
ansi_table["\027[1m"] = "</span><span font_weight='bold'>"
ansi_table["\027[7m"] = "</span><span foreground='" .. theme.bg_normal .. "' background='" .. theme.fg_normal .. "'>"

---@param r number
---@param g number
---@param b number
local function hexformat_rgb_numbers(r, g, b)
  return string.format("#%06x", r * 2 ^ 16 + g * 2 ^ 8 + b)
end
---@param foreground boolean
local function replace_ansi(foreground)
  local attr = foreground and "fgcolor" or "bgcolor"
  return function(red, green, blue)
    return "</span><span " ..
      attr .. "='" ..
      hexformat_rgb_numbers(tonumber(red), tonumber(green), tonumber(blue)) ..
      "'>"
  end
end

---Convert ansi escape sequences to pango markup
---@param text string
function pango.fromAnsi(text)
  return "<span>" ..
    text:gsub("(\027%[%d*m)", ansi_table)
    :gsub("\027%[38;2;(%d*);(%d*);(%d*)m", replace_ansi(true))
    :gsub("\027%[48;2;(%d*);(%d*);(%d*)m", replace_ansi(false)) ..
    "</span>"
end

return setmetatable(pango, { __call = function(_, ...) return markup(...) end })


-- 38;2;r;g;bm
