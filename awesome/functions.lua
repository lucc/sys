-- general helper functions

local awful = require("awful")
local naughty = require("naughty")
local map = require("gears.table").map

local inspect = require("inspect").inspect
local json = require("lain.util.dkjson")

local terminal = require("globals").terminal

local client = client

---Make the givenor the current client floating in the center of the screen
---@param c Client
local function float_center(c)
  c = c or client.focus
  c.floating = true
  awful.placement.centered(c)
  c:jump_to()
end

---Shell escape a string
---@param input string
local function escape(input)
  return "'" .. input:gsub("'", "'\\''") .. "'"
end

---Open a floating terminal and run a command inside
---@vararg string the command and arguments to run (defaults to $SHELL)
local function floating_terminal(...)
  if select('#', ...) == 0 then
    -- default if no argument was given
    return floating_terminal(awful.util.shell)
  end
  local shell = [[awesome-client "require'functions'.float_center()" > /dev/null;]] ..
      table.concat(map(escape, { ... }), " ")
  local cmd = { terminal, '-e', 'sh', '-c', shell }
  local prop = {
    floating = true,
    callback = float_center,
  }
  awful.spawn(cmd, prop)
end

---Join an array of strings into a single string
---@param array string[]
---@param glue string
local function join(array, glue)
  local result = ''
  if #array > 0 then
    result = array[1]
    for i = 2, #array do
      result = result .. glue .. array[i]
    end
  end
  return result
end

---Format lua values and display them as notification for debugging
---@param data any the data to display
---@param title string|nil the title of the notification
local function var_dump(data, title)
  data = type(data) == "string" and data or inspect(data)
  naughty.notify({ title = title or "Debug", text = data, timeout = -1 })
end

local function password(key, callback)
  awful.spawn.easy_async({ "pass", "show", "--", key },
    function(stdout)
      local index = string.find(stdout, '\n')
      if index ~= nil then
        callback(string.sub(stdout, 1, index - 1))
      end
    end)
end

---@class ApiContainer
---@field key string? a key to look for a token in the password store
---@field token string? a token to use for the api requests
---@field cache any the result of the last api call (json decoded)

---Make an api call
---The api call is expected to return a json document
---@param self ApiContainer
---@param url string the url to call
---@param callback fun(json:any) a callback to handle the result of the api call
local function api_update(self, url, callback)
  if self.token == nil then
    return password(self.key, function(token)
      self.token = token
      api_update(self, url, callback)
    end)
  end
  local cmd = { "curl", url, "--header",
    "Authorization: token " .. self.token }
  awful.spawn.easy_async(cmd, function(stdout)
    self.cache = json.decode(stdout)
    callback(self.cache)
  end)
end

---An API client that can caches passwords/access tokens in memory for
---consecutive api calls
---@param options ApiContainer
---@return ApiContainer
local function api(options)
  if options.token == nil and options.key == nil then
    error("You need to provide a token or a key from the password store")
  end
  return setmetatable({ key = options.key, token = options.token },
    { __call = api_update })
end

---Get all the keys of a table
---@param t table
local function keys(t)
  local result = {}
  for k, _ in pairs(t) do table.insert(result, k) end
  return result
end

return {
  api = api,
  float_center = float_center,
  join = join,
  keys = keys,
  run_in_central_terminal = floating_terminal,
  var_dump = var_dump,
}
