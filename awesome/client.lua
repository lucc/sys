local awful = require("awful")
local beautiful = require("beautiful")
local awesome = awesome  -- luacheck: ignore
local client = client  -- luacheck: ignore

-- Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
  if awful.layout.get(c.screen) == awful.layout.suit.magnifier then
    if #c.screen.clients > 0 then
      client.focus = c.screen.clients[1]
    end
  elseif awful.client.focus.filter(c) then
    client.focus = c
  end
end)

local update_client_border = function (client, color)
    if client.maximized or (not client.floating and #client.screen.tiled_clients == 1) then
        client.border_width = 0
    else
	client.border_color = color
	client.border_width = beautiful.border_width
    end
end

client.connect_signal("focus", function(c)
    update_client_border(c, beautiful.border_focus)
end)
client.connect_signal("unfocus", function(c)
    update_client_border(c, beautiful.border_normal)
end)
