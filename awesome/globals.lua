-- some global and system dependent variables

-- This is used later as the default terminal and editor to run.
local terminal = "term"
local editor = os.getenv("EDITOR") or "vim"
local editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt. If you do not
-- like this or do not have such a key, I suggest you to remap Mod4 to another
-- key using xmodmap or other tools. However, you can use another modifier
-- like Mod1, but it may interact with others.
local modkey = "Mod4"

-- the name of some system dependent devices
local system_constants = require("system-constants")

return {
  battery = system_constants.battery,
  calculator = system_constants.calculator or "ghci",
  editor = editor_cmd,
  flake = system_constants.flake,
  lainPath = system_constants.lainPath,
  menuKey = system_constants.menuKey,
  modkey = modkey,
  terminal = terminal,
  wifiDevices = system_constants.wifiDevices,
}
