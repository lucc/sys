-- widgets for the bar
local globals = require("globals")

local battery = require("widgets.battery")(globals.battery)
local disk = require("widgets.disk")
local git = require("widgets.git")
local mail = require("widgets.notmuch"):init("tag:inbox")
local mpd = require("widgets.mpd")
local systemd = require("widgets.systemd")
local tasks = require("widgets.taskwarrior")
local wifi = require("widgets.wifi")(globals.wifiDevices[1])

local home = os.getenv("HOME")
git:register(
  {path = home.."/.config", untracked = false},
  {glob = home.."/src/*/.git"}
)

return {
  -- in sorted order for the widget bar
  disk,
  git,
  systemd,
  mail,
  mpd.widget,
  battery,
  tasks,
  wifi,
  -- and with keys to be accessed elsewhere
  tasks = tasks,
  music = mpd,
  mail = mail,
  git = git,
}
