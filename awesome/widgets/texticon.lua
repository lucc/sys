
local awful = require("awful")
local wibox = require("wibox")

---Update the tooltop attached to a widget
---@param self table the widget
---@param text string the text for the tooltip
---@param markup boolean if the text should be interpreted as pango markup
local function set_tooltip(self, text, markup)
  if markup then self.tooltip.markup = text
  else self.tooltip:set_text(text)
  end
end

---Create a widget that holds a text icon
---@param options table?
local function texticon(options)
  options = options or {}
  options.tooltip = options.tooltip or true
  options.align = "center"
  options.valign = "center"
  --options.forced_width = options.width or 20
  options.widget = wibox.widget.textbox
  local widget = wibox.widget(options)
  if options.tooltip then
    widget.tooltip = awful.tooltip{ objects = { widget } }
    widget.set_tooltip = set_tooltip
  end
  --widget._width = options.forced_width
  return widget
end

return texticon
