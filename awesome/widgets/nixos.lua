-- check for available updates on nixos
local run = require("awful.spawn").easy_async
local json = require("lain.util.dkjson")
local pango = require("pango")
local symbols = require("symbols")
local texticon = require("widgets/texticon")
local gears = require("gears")

return function(system_flake)
  local nixos = texticon()
  local icon = pango.iconic(symbols.nixos) .. " "

  function nixos.update()
    -- FIXME calling nix flake update without rev means we can have new or
    -- removed flake inputs and the comparison later on may fail. But nix
    -- flake update with rev fails with an assertion.
    local cmd = {"sh", "-ec", [[
      rev=$(jq --raw-output .rev < /run/current-system/etc/nixos-active.json)
      nix flake metadata --json "$0?rev=$rev"
      nix flake update --output-lock-file /dev/stdout "$0?rev=$rev" || true
      ]], system_flake}
    run(cmd, function(stdout, stderr, exitreason, exitcode)
      if exitreason == "exit" and exitcode == 0 then
	local before, pos = json.decode(stdout)
	local after = json.decode(stdout, pos).nodes
	before = before.locks.nodes
	before.root = nil
	local lines = {}
	for name, info in pairs(before) do
	  if info.locked.lastModified ~= after[name].locked.lastModified then
	    table.insert(lines, name .. ": \t" ..
	      os.date("%F %T", info.locked.lastModified) .. " -> " ..
	      os.date("%F %T", after[name].locked.lastModified))
	  end
	end
	if #lines > 0 then
	  table.insert(lines, 1, pango("b", "Available Updates"))
	  nixos:set_tooltip(table.concat(lines, "\n"), true)
	  nixos:set_markup(pango.color("blue", icon))
	else
	  nixos:set_markup("")
	end
      else
	local text = pango("b", "Errors") .. "\n" .. pango.escape(stderr)
	nixos:set_tooltip(pango.color("red", text), true)
	nixos:set_markup(pango.color("red", icon))
      end
    end)
  end

  gears.timer{
    timeout = 60 * 60 * 3, -- three hours
    autostart = true,
    callback = function() nixos.update() end,
    call_now = true,
  }

  return nixos
end
