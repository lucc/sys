local awful = require("awful")
local filesystem = require("gears.filesystem")
local gears = require("gears")
local spawn = require("awful.spawn").easy_async
local wibox = require("wibox")

local stat = require("posix.sys.stat").stat
local time = require("posix").time

local pango = require("pango")
local terminal = require("functions").run_in_central_terminal

local db = filesystem.get_xdg_data_home() .. "/khal/khal.db"

local khal = wibox.widget.textclock(" %a %b %d, %H:%M:%S ", 1)
khal.tooltip = awful.tooltip{ objects = { khal } }

function khal:update()
  local timestamp = stat(db).st_mtime
  local now = time()
  if self.timestamp == nil or self.timestamp < timestamp or self.timestamp + 3600 < now then
    self.timestamp = now
    spawn({"khal", "--color", "calendar", "--format",
	   "{calendar-color}{cancelled}{start-end-time-style} " ..
	   "{title}{repeat-symbol}{reset}", "today", "10d"},
      function(stdout)
	self.tooltip.markup = pango.font('monospace" size="large',
	  "\n " .. pango.fromAnsi(pango.escape(stdout:gsub("\n", " \n "))))
      end)
  end
end

local function update()
  khal:update()
end

khal:connect_signal("mouse::enter", update)
khal:buttons(awful.util.table.join(
  awful.button({}, 1, function() terminal("ikhal") end)
))

gears.timer{
  timeout = 499,
  autostart = true,
  callback = update,
}

return khal
