local vicious = require("vicious")

local pango = require("pango")
local symbols = require("symbols")
local texticon = require("widgets.texticon")

---wifi info box
---@param wifi_device string the name of the network device to monitor
return function (wifi_device)
  local wifi_widget = texticon()
  vicious.register(wifi_widget, vicious.widgets.wifi,
    function (widget, args)
      local color = 'red'
      local ssid = args['{ssid}']
      if ssid == 'N/A' then
	widget.tooltip:set_text('\n Not connected! \n')
      else
	if args['{linp}'] >= 50 then
	  color = 'green'
	else
	  color = 'yellow'
	end
	widget.tooltip:set_text(ssid)
      end
      return pango.color(color, pango.iconic(symbols.wifi)) .. ' '
    end,
    120,
    wifi_device
  )
  return wifi_widget
end
