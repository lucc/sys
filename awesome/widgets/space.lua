local wibox = require("wibox")

-- spacing between widgets
local space = wibox.widget.textbox()
space:set_text(" ")

return space
