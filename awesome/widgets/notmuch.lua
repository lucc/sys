-- An awesome widget to display information about new and unread mail from
-- notmuch.

local awful = require("awful")
local naughty = require("naughty")
local spawn = require("awful.spawn")
local shell = spawn.easy_async_with_shell
local json = require("lain.util.dkjson")
local pango = require("pango")

local symbols = require("symbols")
local terminal = require("functions").run_in_central_terminal
local texticon = require("widgets/texticon")

-- Define the widget that will hold the info about new mail (summary in
-- tooltip) and all related data.
local notmuch = texticon()

-- The default query will be used if no other query is given.
notmuch.query = [[\(tag:inbox and tag:unread\)]]

--- Initialize the widget with a query and update it.
--- @param self self
--- @param query string
--- @return self
function notmuch.init(self, query)
  if query ~= nil then
    self.query = query
  end
  self:update()
  return self
end

--- @param summary table a list of tables, each table needs a "authors" and a "subject" key
--- @return string
local function format_summary(summary)
  local str = pango('b', pango.color('green', 'Summary of new mail:'))
  --local keys = {'date_relative', 'authors', 'subject', 'tags'}
  for _, entry in ipairs(summary) do
    local authors = entry['authors']
    if #authors >= 100 then
      authors = string.sub(authors, 1, 99) .. '…'
    end
    str = str .. '\n' .. pango.color('blue', pango.escape(authors)) .. ':\t' ..
        pango.color('red', pango.escape(entry['subject']))
  end
  return str
end

--- @param self self
--- @param force boolean|nil
function notmuch.update(self, force)
  local script = ''
  if force then
    script = 'notmuch new;'
  end
  script = script ..
      'notmuch count -- ' .. self.query .. ';' ..
      'notmuch search --format=json --sort=newest-first -- ' .. self.query
  shell(script, function(stdout, _stderr, _exitreason, _exitcode)
    local i, _ = string.find(stdout, '\n', 1, true)
    local count = tonumber(string.sub(stdout, 1, i))
    local summary = ""
    local markup = ""
    if count ~= 0 then
      markup = pango.iconic(symbols.envolope2)
      if count > 1 then
        markup = count .. ' ' .. markup
      end
      markup = pango.color('red', markup) .. ' '
      summary = string.sub(stdout, i + 1)
      summary = json.decode(summary)
      summary = format_summary(summary)
    else
      local notification = naughty.getById(self.last_id)
      naughty.destroy(notification)
    end
    self:set_markup(markup)
    self.tooltip.markup = summary
  end)
end

function notmuch.notify(self)
  shell('notmuch search ' .. self.query .. ' | cut -f2- -d " "',
    function(stdout, _stderr, _reason, exit_code)
      if exit_code == 0 and stdout ~= '' then
        local notification = naughty.notify {
          text = stdout,
          title = 'New mail',
          timeout = 0,
          icon = '/run/current-system/sw/share/icons/Adwaita/96x96/status/mail-unread-symbolic.symbolic.png',
          replaces_id = self.last_id
        }
        self.last_id = notification.id
      end
    end)
end

-- some nice helper functions
notmuch.tui = function() terminal("alot") end
notmuch.tui2 = function() terminal("sh", "-c", [[purebred; awesome-client 'require("widgets/notmuch"):update()']]) end
notmuch.gui = function()
  spawn.with_line_callback(
    "astroid", { exit = function() notmuch:update() end }
  )
end

notmuch:buttons(awful.util.table.join(
  awful.button({}, 1, notmuch.tui),
  awful.button({}, 3, notmuch.tui2)
))

return notmuch
