local vicious = require("vicious")

local pango = require("pango")
local symbols = require("symbols")
local texticon = require("widgets.texticon")

-- filesystem info
local disk = texticon()
vicious.register(disk, vicious.widgets.fs,
  function (widget, args)
    local interesting_filesystems = {"/", "/boot", "/home"}
    local percent = 100
    for _, fs in pairs(interesting_filesystems) do
      local p = args["{"..fs.." avail_p}"]
      if p ~= nil and p < percent then percent = p end
    end
    local color = ""
    if percent < 1 then color = "red"
    elseif percent < 2 then color = "yellow"
    elseif percent < 5 then color = "green"
    end
    if color ~= "" then
      local tooltip = "Free disk space"
      for _, fs in pairs(interesting_filesystems) do
	local p = args["{"..fs.." avail_p}"]
	if p ~= nil then
	  tooltip = tooltip .. "\n" .. fs .. "\t"
	  ..  args["{"..fs.." avail_gb}"] .. "GB free (" .. p .. "%)"
	end
      end
      widget.tooltip:set_markup(tooltip)
      return pango.color(color, pango.iconic(symbols.disk2))
    end
    return ""
  end, 307)

return disk
