local vicious = require("vicious")
local naughty = require("naughty")

local pango = require("pango")
local symbols = require("symbols")
local texticon = require("widgets.texticon")

-- battery
local function make_battery_widget(battery)
  local baticon = texticon()
  vicious.register(baticon, vicious.widgets.bat,
    function (widget, args)
      local icon = symbols.battery0
      local col = 'red'
      local percent = args[2]
      if percent > 33 then
	col = 'yellow'
	icon = symbols.battery1
      end
      if percent > 50 then
	icon = symbols.battery2
      end
      if percent > 66 then
	col = 'green'
	icon = symbols.battery3
      end
      if percent > 95 then
	icon = symbols.battery4
      end
      --"<span color='green'>power@$2%=$3</span>"
      local time = args[3]
      local rate = args[5] == 'N/A' and 'N/A' or (args[5]..'W')
      baticon.tooltip:set_text(
	string.format('Connected: %s\nLevel: %s%%\nTime: %s\nRate: %s',
		      args[1], percent, time, rate))
      if args[1] == '-' and (percent < 10 or
	  time == '00:00' or time == '00:01' or time == '00:02' or
	  time == '00:03' or time == '00:04' or time == '00:05') then
	local notification = naughty.notify({
	    preset = naughty.config.presets.critical,
	    title="Battery low!",
	    replaces_id = widget.last_id,
	    text='Only '..time..' remaining!'
	})
	widget.last_id = notification.id
      else
	local notification = naughty.getById(widget.last_id)
	naughty.destroy(notification)
      end
      return pango.color(col, pango.iconic(icon)) .. ' '
    end,
    67, battery)
   return baticon
end

return make_battery_widget
