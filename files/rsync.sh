#!/usr/bin/env bash

set -o errexit
set -o noglob
set -o nounset
set -o pipefail
shopt -s lastpipe

print_help () {
  cat <<-'EOF'
	This script wraps rsync(1) to create incremental snapshots to a given
	destination.  Disk space is saved by hard linking unchanged files with
	rsync's --link-dest.

	options:

	  -h        print this help and exit

	  -d dest   provide the destination path (or host:path for ssh
		    destinations) where to copy to

	  -l        add --link-dest arguments to the rsync call for all
		    .in_progress directories that were created since the last
		    successful backup
	EOF
}
print_usage () {
  echo "Usage: ${0##*/} [-l] -d dest [--] sources and any rsync options"
  echo "       ${0##*/} -h"
}

dest=
link_in_progress=false

while getopts d:hl FLAG; do
  case $FLAG in
    d) dest=$OPTARG;;
    h) print_usage; print_help; exit;;
    l) link_in_progress=true;;
    *) print_usage >&2; exit 2;;
  esac
done
shift $((OPTIND - 1))

if [[ -z $dest ]]; then
  print_usage >&2
  exit 2
elif [[ $dest =~ ::|^rsync:// ]]; then
  cat >&2 <<-'EOF'
	The rsync protocol is not supported by this script, only ssh and
	local transfers.
	EOF
  exit 1
fi

timestamp=$(date +%Y%m%d%H%M%S)
declare -ir timestamp

# Extract host and path part of dest in order to decide if we are copying over
# ssh or locally.  If host is set we copy over ssh.
host=${dest%%:*}
path=${dest#*:}

extra_link_dest=()
if "$link_in_progress"; then
  if [[ $host ]]; then
    ssh "$host" ls -t "${path@Q}"
  else
    ls -t "$path"
  fi | mapfile -t in_progress
  # we only add 10 --link-dest arguments at maximum because rsync has an
  # internal limit of 20
  for p in "${in_progress[@]:0:10}"; do
    if [[ $p = latest ]]; then
      break
    fi
    extra_link_dest+=(--link-dest "../$p")
  done
fi

# Hide the exit code because rsync "fails" for some errors that I'm not
# interested in.
# TODO which errors are these?
rsync --archive --one-file-system --link-dest=../latest \
  "${extra_link_dest[@]}" "$@" "$dest/$timestamp.in_progress"

if [[ $host ]]; then
  ssh "$host" cd "${path@Q}" \
    '&&' mv $timestamp.in_progress $timestamp \
    '&&' ln -fns $timestamp latest
else
  cd "$path"
  mv $timestamp.in_progress $timestamp
  ln -fns $timestamp latest
fi
