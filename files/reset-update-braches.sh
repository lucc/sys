#!/usr/bin/env bash

set -euo pipefail

prog=${0##*/}
update=false
dryrun=
tags_date=
rebase=false
one_off=false

usage () {
  echo "Usage: $prog [-d date] [-nru]"
  echo "       $prog -h"
}
help () {
  echo
  echo "  -h    display help"
  echo "  -d    delete update tags older than date"
  echo "  -n    dry run"
  echo "  -r    use git rebase instead of git reset"
  echo "  -u    update all flake inputs to latest version"
}

while getopts d:hnru1 FLAG; do
  case $FLAG in
    h) usage; help; exit;;
    d) tags_date=$(date -d "$OPTARG" +%F);;
    n) dryrun=true;;
    r) rebase=true;;
    u) update=true;;
    1) one_off=true;;
    *) usage >&2; exit 2;;
  esac
done
shift $((OPTIND - 1))

if [[ -n $tags_date && "$tags_date" > $(date +%F) ]]; then
  echo "The given date is in the future." >&2
  exit 2
fi

git switch main

# update all flake inputs
if $update; then
  if [[ -n $dryrun ]]; then
    nix flake update --output-lock-file /dev/null
  else
    nix flake update --commit-lock-file
  fi
fi

target=main
if "$one_off"; then
  target=main~1
fi

# move the updates branch and delete old update/* branches
if $rebase; then
  if [[ -n $dryrun ]]; then
    echo git rebase --interactive "$target" updates
  else
    if ! EDITOR="sed -in '/flake.lock: Update/p'" git rebase --interactive "$target" updates; then
      until git rebase --skip; do
	:
      done
    fi
  fi
  git switch main
else
  ${dryrun:+echo} git branch --force updates "$target"
fi
git branch --list "update/20??-??-??-*" \
  | xargs --no-run-if-empty ${dryrun:+echo} git branch --delete --force

if [[ -n $tags_date ]]; then
  git tag --list "update/20??-??-??-*" \
    | sed 's#update/\(....-..-..\).*#\1 &#' \
    | awk -v date="$tags_date" '$1 < date { print $2 }' \
    | xargs --no-run-if-empty ${dryrun:+echo} git tag --delete
fi
