-- language server configurations

local lspconfig = require 'lspconfig'

local function on_attach(client, bufnr)
  local function map(left, right)
    vim.keymap.set("n", left, right, { buffer = bufnr })
  end

  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_set_option_value("omnifunc", 'v:lua.vim.lsp.omnifunc', { buf = bufnr })

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  map('gD', vim.lsp.buf.declaration)
  map('gd', vim.lsp.buf.definition)
  map('K', vim.lsp.buf.hover)
  map('gi', vim.lsp.buf.implementation)
  map('<C-k>', vim.lsp.buf.signature_help)
  map('<F5>', vim.lsp.buf.code_action)
  -- mapping for the symbols-outline plugin
  if vim.fn.maparg('gO') == "" then
    map('gO', '<CMD>SymbolsOutline<CR>')
  end
end
local capabilities = require('cmp_nvim_lsp').default_capabilities()

--local opts = { noremap = true, expr = true }
--vim.api.nvim_set_keymap('i', '<Tab>', [[pumvisible() ? {_, x -> x}(v:lua.vim.lsp.buf.hover(), "\<C-N>") : "\<Tab>"]], opts)
--vim.api.nvim_set_keymap('i', '<S-Tab>', [[pumvisible() ? {_, x -> x}(v:lua.vim.lsp.buf.hover(), "\<C-P>") : "\<Tab>"]], opts)
--vim.api.nvim_set_keymap('i', '<CR>', [[pumvisible() ? {_, x -> x}(v:lua.vim.lsp.buf.hover(), "\<C-Y><CR>") : "\<CR>"]], opts)
vim.api.nvim_create_user_command("CodeAction", function() vim.lsp.buf.code_action() end, {})
