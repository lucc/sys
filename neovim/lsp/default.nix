{pkgs, ...}: let
  inherit (pkgs.lib.generators) mkLuaInline toLua;
  # an attrset of available language servers
  servers = import ./servers.nix {inherit pkgs;};
  toSnippet = spec: let
    default-args = {
      capabilities = mkLuaInline "capabilities";
      on_attach = mkLuaInline "on_attach";
    };
    args = default-args // (spec.args or {});
  in ''
    ${spec.prelude or ""}
    lspconfig[${toLua {} spec.name}].setup ${toLua {} args}
  '';
  joinSnippets = pkgs.lib.strings.concatMapStringsSep "\n" toSnippet;
  # Create a list of language server derivations and a neovim plugin
  # derivation
  plugin = ls-list: {
    packages = pkgs.lib.lists.flatten (pkgs.lib.attrsets.catAttrs "package" ls-list);
    plugin = pkgs.writeTextFile {
      name = "lsp.lua";
      destination = "/plugin/lsp.lua";
      text = ''
        ${builtins.readFile ./header.lua}
        ${joinSnippets ls-list}
      '';
      checkPhase = "${pkgs.lua}/bin/luac $out/plugin/lsp.lua";
    };
  };
in {
  inherit plugin servers;
  build = callback: plugin (callback servers);
}
