{pkgs, ...}: {
  ansible = {
    name = "ansiblels";
    package = [pkgs.ansible-language-server pkgs.ansible-lint];
  };
  bash = {
    name = "bashls";
    package = pkgs.nodePackages.bash-language-server;
  };
  css = {
    name = "cssls";
    package = pkgs.vscode-langservers-extracted;
  };
  deno = {
    name = "denols";
    package = pkgs.deno;
  };
  docker = {
    name = "dockerls";
    package = pkgs.nodePackages.dockerfile-language-server-nodejs;
  };
  go = {
    name = "gopls";
    package = [pkgs.gopls pkgs.go];
  };
  haskell = {
    name = "hls";
    package = pkgs.haskell-language-server;
  };
  html = {
    name = "html";
    package = pkgs.vscode-langservers-extracted;
  };
  json = {
    name = "jsonls";
    package = pkgs.vscode-langservers-extracted;
  };
  lua = {
    name = "lua_ls";
    package = pkgs.sumneko-lua-language-server;
    prelude = ''
      local runtime_path = vim.split(package.path, ';')
      table.insert(runtime_path, "lua/?.lua")
      table.insert(runtime_path, "lua/?/init.lua")
    '';
    args.settings = {
      Lua = {
        format.defaultConfig = {
          indent_style = "space";
          indent_size = "2";
        };
        runtime = {
          # Tell the language server which version of Lua you're using (most
          # likely LuaJIT in the case of Neovim)
          version = "LuaJIT";
          # Setup your lua path
          path = pkgs.lib.generators.mkLuaInline "runtime_path";
        };
        diagnostics = {
          # Get the language server to recognize the `vim` global
          globals = ["vim"];
          neededFileStatus.codestyle-check = "Any";
        };
        # Make the server aware of Neovim runtime files
        workspace.library = pkgs.lib.generators.mkLuaInline ''vim.api.nvim_get_runtime_file("", true)'';
        # Do not send telemetry data containing a randomized but unique
        # identifier
        telemetry.enable = false;
      };
    };
  };
  # for markdown
  marksman = {
    name = "marksman";
    package = pkgs.marksman;
  };
  nil = {
    name = "nil_ls";
    package = pkgs.nil;
    args.settings.nil.formatting.command = ["${pkgs.alejandra}/bin/alejandra"];
  };
  phpactor = {
    name = "phpactor";
    package = pkgs.phpactor;
  };
  php-intelephense = {
    name = "intelephense";
    package = pkgs.nodePackages.intelephense;
  };
  php-psalm = {
    name = "psalm";
    args.cmd = ["${pkgs.phpPackages.psalm}/bin/psalm" "--language-server"];
  };
  pyright = {
    name = "pyright";
    package = pkgs.pyright;
  };
  pylsp = {
    name = "pylsp";
    package = pkgs.python3.withPackages (p:
      with p; [
        #pyls-flake8
        pyls-isort
        #pylsp-rope
        python-lsp-black
        python-lsp-ruff
        python-lsp-server
      ]);
  };
  python-jedi = {
    name = "jedi_language_server";
    package = pkgs.python3Packages.jedi-language-server;
  };
  # removed from nixpkgs because it is unmaintained
  #rnix = {
  #  name = "rnix";
  #  package = pkgs.rnix-lsp;
  #};
  rust = {
    name = "rust_analyzer";
    package = pkgs.rust-analyzer;
  };
  sql = {
    name = "sqls";
    package = pkgs.sqls;
  };
  tex = {
    name = "texlab";
    package = pkgs.texlab;
    args.settings.texlab = {
      auxDirectory = "latex.out";
      build = {
        executable = "latexrun";
        args = ["%f"];
      };
    };
  };
  typos = {
    name = "typos_lsp";
    package = pkgs.typos-lsp;
  };
  vim = {
    name = "vimls";
    package = pkgs.nodePackages.vim-language-server;
  };
  vetur = {
    name = "vuels";
    package = pkgs.nodejs;
    args.cmd = ["${pkgs.vscode-extensions.octref.vetur}/share/vscode/extensions/octref.vetur/server/bin/vls"];
  };
  volar = {
    name = "volar";
    args = {
      cmd = [
        "${pkgs.nodePackages.volar}/bin/vue-language-server"
        "--stdio"
      ];
      init_options.typescript.tsdk = "${pkgs.typescript}/lib/node_modules/typescript/lib";
    };
  };
  yaml = {
    name = "yamlls";
    package = pkgs.yaml-language-server;
    args.settings.yaml.keyOrdering = false;
  };
}
