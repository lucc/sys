-- Use cargo or rustc as compiler, depending on the presence of a Cargo.toml
-- file.
if vim.fs.root(0, "Cargo.toml") then
  vim.cmd.compiler "cargo"
else
  vim.cmd.compiler "rustc"
end
