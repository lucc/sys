" custom filetype settings by luc
setlocal spell

" Add "portable" comments like discribed in
" https://stackoverflow.com/a/20885980/4102092
setlocal comments+=b:[//]:\ #

" Add a special syntax match for "portable markdown line comments" like
" described in https://stackoverflow.com/a/20885980/4102092
syntax match LucPandocComment /\v^\[\/\/\]: # .*$/
"containedin=pandocReferenceLabel,pandocOperator
highlight link LucPandocComment Comment
