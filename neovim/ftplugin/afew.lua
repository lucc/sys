-- ftplugin stuff for afew config

-- a helper function to extract the value of a "message" ini key from a
-- section given by the line numbers start and stop
local function get_message(buffer, start, stop)
  local lines = vim.api.nvim_buf_get_lines(buffer, start, stop,  1)
  local message = vim.fn.matchstr(lines, '^\\s*message')
  return message:gsub("%s*message%s*=%s*(.*)", " %1")
end

-- Simple wrapper to use get_message as a foldtext function
function AfewFoldText()
  return vim.fn.foldtext() .. get_message(0, vim.v.foldstart, vim.v.foldend)
end

vim.wo.foldtext = "v:lua.AfewFoldText()"
