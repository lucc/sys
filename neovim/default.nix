{ pkgs, self, ... }:

let
  # import some lib functions into the local scope
  inherit (pkgs.lib.attrsets) mapAttrsToList filterAttrs;
  inherit (pkgs.vimUtils) buildVimPlugin;
  inherit (pkgs.lib.strings) hasPrefix hasSuffix removePrefix;

  # turn this folder into a neovim plugin that can be used in the neovim
  # derivation from nixpkgs
  lucc-neovim-config = buildVimPlugin {
    name = "lucc-neovim-config";
    src = pkgs.lib.cleanSourceWith {
      filter = path: type:
        # remove the packer files for the compiled-plugins
        (baseNameOf path != "plugins.lua")
        # remove all nix code
        && (! hasSuffix ".nix" path);
      src = ./.;
    };
    dependencies = [pkgs.vimPlugins.telescope-nvim] ++ packaged-start-plugins;
  };

  lsp = pkgs.callPackage ./lsp {};

  # helper function to transform flake inputs into vim plugins
  filterAndMapPlugins = prefix: list:
    (mapAttrsToList (n: src: buildVimPlugin { inherit src; name = removePrefix prefix n; }) (
      filterAttrs (n: v: hasPrefix prefix n) list));
  # list of plugins that should be sourced on start
  start-plugins = filterAndMapPlugins "nvim-plugin-" self.inputs ++ [
    (buildVimPlugin {
      name = "nvim-plugin-pycfg";
      src = self.inputs.pycfg;
      path = "syntax";
    })
    (buildVimPlugin {
      name = "nvim-plugin-vcard";
      src = self.inputs.vcard;
      preInstall = "${pkgs.dos2unix}/bin/dos2unix syntax/vcard.vim";
    })
  ];

  # Some plugins with extra lazy loading logic compiled by packer.nvim
  compiled-plugins = import ./packer.nix { inherit self pkgs; };

  # A list of plugins that should be added to the start plugins section. All
  # these plugins are in nixpkgs.
  packaged-start-plugins = with pkgs.vimPlugins; [
    # one can also use .withAllGrammars or see a list of available grammars
    # with
    # builtins.attrNames nvim-treesitter.passthru.grammarPlugins
    (nvim-treesitter.withPlugins (p: with p; [
      bash
      bibtex
      comment
      css
      html
      javascript
      latex
      lua
      markdown
      markdown_inline
      nix
      php
      python
      rust
      vimdoc
      yaml
    ]))
    nvim-ts-autotag # https://github.com/windwp/nvim-ts-autotag
    #autoclose-nvim # https://github.com/m4xshen/autoclose.nvim
    nvim-treesitter-endwise # https://github.com/RRethy/nvim-treesitter-endwise
    nvim-autopairs # https://github.com/windwp/nvim-autopairs
    # https://github.com/altermo/ultimate-autopair.nvim

    unicode-vim
    mkdir-nvim
    thesaurus_query-vim
    vim-scriptease
    vim-indent-object
    vim-mundo
    vim-illuminate
    kommentary
    nvim-surround
    vim-matchup
    easy-align
    ultisnips
    vim-snippets
    FTerm-nvim
    nvim-colorizer-lua

    ansible-vim
    agda-vim
    bats
    context-filetype
    csv
    vim-nix
    mediawiki-vim
    vim-gnupg
    haskell-vim
    vim-nftables
    vim-pandoc
    #vim-pandoc-syntax
    vimtex

    # vcs stuff
    fugitive   # git integration
    gitgutter   # change indicator in sign column

    # telescope and friends
    plenary-nvim
    telescope-ui-select-nvim
    sqlite-lua
    telescope-frecency-nvim

    # ui
    vim-devicons # icons
    lualine-nvim
    #NeoSolarized # "https://github.com/overcache/NeoSolarized/"
    nvim-solarized-lua # https://github.com/ishan9299/nvim-solarized-lua/

    # language server
    nvim-lspconfig
    symbols-outline-nvim

    # completion
    nvim-cmp # https://github.com/hrsh7th/nvim-cmp/
    cmp-nvim-lsp
    cmp-buffer
    cmp-path # https://github.com/hrsh7th/cmp-path/
    cmp-cmdline
    cmp-nvim-ultisnips # https://github.com/quangnguyen30192/cmp-nvim-ultisnips/
    cmp-nvim-lsp-signature-help # https://github.com/hrsh7th/cmp-nvim-lsp-signature-help/
    cmp-git # https://github.com/petertriho/cmp-git/
    cmp-rg # https://github.com/lukas-reineke/cmp-rg/

    # these look nice
    #cmp-async-path # https://github.com/FelipeLema/cmp-async-path/
    #cmp-clippy # https://github.com/vappolinario/cmp-clippy/ #- open source github-copulot
    cmp-cmdline-history # https://github.com/dmitmel/cmp-cmdline-history/
    #cmp-conventionalcommits # https://github.com/davidsierradz/cmp-conventionalcommits/
    #cmp-dap # https://github.com/rcarriga/cmp-dap/
    #cmp-dictionary # https://github.com/uga-rosa/cmp-dictionary/
    #cmp-digraphs # https://github.com/dmitmel/cmp-digraphs/
    #cmp-greek # https://github.com/max397574/cmp-greek/
    #cmp-latex-symbols # https://github.com/kdheepak/cmp-latex-symbols/
    #cmp-npm # https://github.com/David-Kunz/cmp-npm/
    cmp-nvim-lua # https://github.com/hrsh7th/cmp-nvim-lua/
    #cmp-omni # https://github.com/hrsh7th/cmp-omni/
    #cmp-pandoc-nvim # https://github.com/aspeddro/cmp-pandoc.nvim/
    #cmp-spell # https://github.com/f3fora/cmp-spell/
    cmp-treesitter # https://github.com/ray-x/cmp-treesitter/
    cmp-under-comparator # https://github.com/lukas-reineke/cmp-under-comparator/

  ];

  make-neovim-package = callback:
    let
      inherit (lsp.build callback) packages plugin;
      custom-neovim = pkgs.wrapNeovimUnstable pkgs.neovim-unwrapped {
        luaRcContent = ''
          vim.loader.enable()

          vim.cmd "set rtp+=${compiled-plugins}"
          vim.cmd "set pp+=${compiled-plugins}"

          vim.cmd "packadd! ${lucc-neovim-config.name}"

          ${builtins.readFile ./init.lua}
        '';
        plugins = [
          {plugin = lucc-neovim-config; optional = true;}
          {plugin = pkgs.vimPlugins.packer-nvim; optional = true;}
          plugin
        ] ++ start-plugins
          ++ packaged-start-plugins;
        withPython3 = true;
        # unidecode is needed by ultisnips with tex files
        extraPython3Packages = (p: with p; [ pynvim unidecode ]);
      };
      # v wrapper script around neovim. We can not use "nvim -OS" in the
      # shebang line as it would require the final script to have a .lua
      # extension.
      v = pkgs.writeShellScriptBin "v" ''
        if [ -n "$NVIM" ]; then
          exec nvr "$@"
        else
          exec ${custom-neovim}/bin/nvim -OS ${./bin/v.lua} "$@"
        fi
      '';
    in
    pkgs.buildEnv {
      name = "neovim-env";
      paths = with pkgs; [
        custom-neovim
        neovim-remote
        v
        proselint
      ] ++ packages;
    };
in
  # custom neovim package with many plugins
  (make-neovim-package (_: [])).overrideAttrs (oa: {
    passthru = oa.passthru // {
      inherit compiled-plugins;
      languageServers = lsp.servers;
      withLS = make-neovim-package;
    };
  })
