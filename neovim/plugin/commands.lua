-- user defined commands by luc

-- From the example vimrc file.
vim.api.nvim_create_user_command("DiffOrig", "call luc#commands#DiffOrig()", {})

-- Open the file under the cursor or the current file with xdg-open.
vim.api.nvim_create_user_command("O",
  function(opts) require "commands".open(opts.bang, opts.args) end,
  {
    desc = "Open files with xdg-open",
    bang = true,
    complete = "file",
    nargs = "?",
  }
)

vim.cmd.cabbrev "man vertical Man"

vim.api.nvim_create_user_command("SSH", "call luc#commands#ssh(<q-args>)", { nargs = "*" })
vim.api.nvim_create_user_command("Todo",
  function(opts) require "commands".todo(opts.bang) end,
  { bang = true }
)

-- Write a file with sudo previleges without restarting vim.
vim.api.nvim_create_user_command("Suw",
  function() require"commands".sudo_write() end,
  {}
)
