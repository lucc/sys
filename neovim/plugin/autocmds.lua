-- user defined autocommands by luc

vim.api.nvim_create_autocmd({ "BufWrite" }, {
  desc = "Remove all whitespace at the end of lines",
  pattern = "*",
  callback = function(ev)
    if not (ev.file:match("%.patch$") or ev.file:match("%.diff$")) then
      local position = vim.fn.getpos(".")
      vim.cmd "silent keepjumps keeppatterns %snomagic/\\s\\+$//e"
      vim.fn.setpos(".", position)
    end
  end
})

vim.api.nvim_create_autocmd({ "BufWritePost" }, {
  desc = "Make all scripts executable",
  pattern = "*",
  callback = function(ev)
    if vim.fn.getline(1):match("^#!.*/bin/") and not vim.fn.executable(ev.file) then
      vim.fn.jobstart({ "chmod", "+x", ev.file })
    end
  end
})

vim.api.nvim_create_autocmd({ "BufWritePre" }, {
  desc = "Set options to not store state for sensitive temp files",
  pattern = { "/tmp/*", "/dev/shm/*" },
  callback = function()
    vim.opt.undofile = false
  end
})
