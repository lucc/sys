local function tig(args)
  local tig_cmd = { "tig" }
  vim.list_extend(tig_cmd, args.fargs)

  local buf = vim.api.nvim_create_buf(false, true)
  local win = vim.api.nvim_open_win(buf, true, { vertical = true })
  vim.fn.termopen(tig_cmd, {
    on_exit = function(id, data, event)
      if event == "exit" and data == 0 then
        vim.api.nvim_win_close(win, false)
      end
    end,
  })
  vim.cmd.startinsert()
end

vim.api.nvim_create_user_command("Tig", tig, {
  desc = "Run tig in a split window",
  complete = vim.fn["fugitive#LogComplete"],
  force = true,
  nargs = "*",
})
