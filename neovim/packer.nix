{
  pkgs,
  self,
}: let
  inherit (pkgs.vimUtils) buildVimPlugin;

  # This version of neovim is only used to create the compiled plugin file for
  # the packer managed plugins.
  nvim-with-packer = pkgs.neovim.override {
    configure.packages.packer.start = [pkgs.vimPlugins.packer-nvim];
  };

  # Create the packer_compiled.lua file for the few plugins that are managed
  # by packer.  All plugins that are specified here have custom lazy loading
  # settings for packer defined in ./packer.lua.
  compiled-plugins =
    pkgs.runCommand "compiled" {
      # some plugins are available in nixpkgs
      inherit
        (pkgs.vimPlugins)
        git-messenger-vim
        iron-nvim
        vim-dispatch
        vim-flog # git history browser
        vim-hoogle
        vim-test
        ;
      # some plugins are flake inputs
      vdebug = buildVimPlugin {
        name = "vdebug";
        src = self.inputs.vdebug;
      };
    } ''
      mkdir -p $out/plugin
      export HOME=$(mktemp -d)
      export NVIM_RPLUGIN_MANIFEST=$out/rplugin.vim
      # --clean breaks :UpdateRemotePlugins
      ${nvim-with-packer}/bin/nvim --headless -S ${./plugins.lua} +q
    '';
in
  compiled-plugins
