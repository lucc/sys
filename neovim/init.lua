-- init.vim file by luc

require "options"
require "maps"

-- Disable some buildin plugins
vim.g.loaded_matchit = 1
vim.g.loaded_matchparen = 1
vim.g.loaded_2html_plugin = 1
vim.g.loaded_tutor_mode_plugin = 1

-- configure treesitter
require'nvim-treesitter.configs'.setup {
  highlight = {
    enable = true,
    disable = { "latex" },
    additional_vim_regex_highlighting = { "lua", "latex" },
  },
  indent = { enable = true },
  matchup = { enable = true },
  incremental_selection = { enable = true },
  textobjects = { enable = true },
  autotag = { enable = true },
  endwise = { enable = true },
}
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"

-- configure telescope
local get_root_dir = function()
  local cwd = vim.fn.FugitiveGitDir(vim.fn.bufnr(""))
  if cwd == "" then
    return require"telescope.utils".buffer_dir()
  end
  return cwd:sub(1, -5)  -- remove the .git suffix
end
local cycle = require "telescope.cycle"(
  function(opts)
    local sorter = require 'telescope.config'.values.file_sorter()
    opts = vim.tbl_extend("force", { sorter = sorter }, opts)
    require 'telescope'.extensions.frecency.frecency(opts)
  end,
  function(opts)
    opts = vim.tbl_extend("force", { cwd = get_root_dir() }, opts)
    require "telescope.builtin".find_files(opts)
  end,
  function(opts)
    opts = vim.tbl_extend("force", { sort_mru = true }, opts)
    require "telescope.builtin".buffers(opts)
  end,
  function(opts)
    opts = vim.tbl_extend("force", { show_all_buffers = true, sort_mru = true }, opts)
    require "telescope.builtin".buffers(opts)
  end
)
local actions = require 'telescope.actions'
require'telescope'.setup {
  defaults = {
    mappings = {
      i = {
	["<esc>"] = actions.close,
	["<C-Space>"] = function() cycle.next() end,
	["<C-h>"] = actions.which_key,
	["<C-s>"] = actions.select_horizontal,
	["<C-Down>"] = require('telescope.actions').cycle_history_next,
	["<C-Up>"] = require('telescope.actions').cycle_history_prev,
      },
    },
  },
  pickers = {
    find_files = {
      root = get_root_dir,
    },
  },
  extensions = {
    ["ui-select"] = {
      require "telescope.themes".get_dropdown(),
    },
    frecency = {
      db_safe_mode = false,
    },
  },
}

require "telescope".load_extension "frecency"
require "telescope".load_extension "ui-select"

local opts1 = {silent = true}
local ctrl_space_function = function() cycle() end
vim.keymap.set({"n", "i"}, "<C-Space>", ctrl_space_function, opts1)
vim.keymap.set({"n", "i", "v"}, "<C-S>", function()
  local default_text
  if require("utils").is_visual_mode() then
    default_text = require("utils").get_visual_selection()
  else
    default_text = vim.fn.expand("<cword>")
  end
  require"telescope.builtin".live_grep{
    default_text = default_text,
    cwd = get_root_dir(),
  }
end, opts1)

-- configure kommentary
-- TODO maybe remove this with neovim 0.10, see https://github.com/neovim/neovim/pull/28176
require('kommentary.config').configure_language("default", {
  prefer_single_line_comments = true,
  use_consistent_indentation = true,
})

require "nvim-surround".setup()

vim.g.matchup_matchparen_offscreen = { method = "popup" }

-- configure vim-pandoc
vim.g['pandoc#modules#disabled'] = {"menu"}
vim.g['pandoc#command#latex_engine'] = 'pdflatex'
vim.g['pandoc#folding#fold_yaml'] = 1
vim.g['pandoc#folding#fdc'] = 0
--vim.g['pandoc#folding#fold_fenced_codeblocks'] = 1
if vim.fn.exists('g:pandoc#biblio#bibs') == 0 then
  vim.g['pandoc#biblio#bibs'] = {'~/bib/main.bib'}
else
  vim.fn.insert(vim.g['pandoc#biblio#bibs'], '~/bib/main.bib')
end
vim.g['pandoc#command#autoexec_on_writes'] = 0
vim.g['pandoc#command#autoexec_command'] = "Pandoc pdf"
vim.g['pandoc#formatting#mode'] = 'h'

-- LaTeX
-- original vim settings for latex
-- vim.g.tex_fold_enabled = 1
vim.g.tex_flavor = 'latex'

-- configure vimtex
vim.g.vimtex_fold_enabled = 1
vim.g.vimtex_fold_types = {
  sections = {
    sections = {
      'part',
      'chapter',
      'section',
      'subsection',
      'subsubsection',
      'paragraph',
    },
  }
}
--vim.g.vimtex_fold_types.comments = {}
--vim.g.vimtex_fold_types.comments.enabled = 1
vim.g.vimtex_compiler_method = 'latexrun'
vim.g.vimtex_compiler_progname = 'nvr'
vim.g.vimtex_view_method = 'zathura'
vim.g.vimtex_toc_config = { split_pos = 'vertical' }
vim.g.vimtex_toc_todo_labels = {
  FIXME = 'FIXME: ',
  TODO = 'TODO: ',
  XXX = 'FIXME: ',
}

-- configure EasyAlign
vim.keymap.set("v", "<Enter>", "<Plug>(EasyAlign)", { silent = true, remap = true })
vim.keymap.set("n", "<Leader>a", "<Plug>(EasyAlign)", { silent = true, remap = true })

local solarized = require "solarized_colors"
require"lualine".setup{
  options = {
    theme = 'solarized_dark',
    --section_separators = '',
    --component_separators = '',
  },
  sections = {
    lualine_b = { "branch" },
    lualine_c = {
      "filename",
      "diff",
      "diagnostics",
    },
  },
}
-- do not display the current mode in the command line
vim.opt.showmode = false

require("symbols-outline").setup()

-- neosolarized setup
vim.g.neosolarized_italic = 1
vim.g.neosolarized_termBoldAsBright = 0
-- neosolarized config
vim.cmd.colorscheme "solarized"
local function update_hl(name, attributes)
  local old = vim.api.nvim_get_hl(0, { name = name })
  local new = vim.tbl_extend("force", old, attributes)
  vim.api.nvim_set_hl(0, name, new)
end
update_hl("VertSplit", {fg=0x657b83, bg=0x657b83})
update_hl("MatchParen", {italic=true, bold=true, bg="none"})

--vim.api.nvim_set_hl(0, "NormalFloat", {fg=solarized.base01, bg=solarized.base2})
vim.api.nvim_set_hl(0, "NormalFloat", {fg=solarized.base0, bg=solarized.base02})
-- from https://github.com/craftzdog/solarized-osaka.nvim/blob/f6e66670e31c09cfb7142a16d5dc2f26d2a31a40/lua/solarized-osaka/theme.lua#L66
vim.api.nvim_set_hl(0, "Pmenu", {fg=solarized.base0, bg=solarized.base02}) -- Popup menu: normal item.
vim.api.nvim_set_hl(0, "PmenuSbar", {fg=solarized.base02, reverse = true}) -- Popup menu: scrollbar.
vim.api.nvim_set_hl(0, "PmenuThumb", {fg=solarized.base0, reverse=true}) -- Popup menu: Thumb of the scrollbar.

-- setup for UltiSnips
vim.g.UltiSnipsExpandTrigger = '<C-F>'
vim.g.UltiSnipsJumpForwardTrigger = '<C-F>' -- <C-J>
vim.g.UltiSnipsJumpBackwardTrigger = '<C-G>' -- <C-K>
--vim.g.UltiSnipsExpandTrigger = '<tab>'
vim.g.UltiSnipsListSnippets = '<C-L>'

-- fterm config
vim.keymap.set("n", "<C-Z>", require"FTerm".toggle, { silent = true })
local augroup = vim.api.nvim_create_augroup("LucFtermConfig", { clear = true })
vim.api.nvim_create_autocmd("FileType", {
  pattern = "FTerm",
  group = augroup,
  callback = function(args)
    local opts = { silent = true, buffer = args.buf }
    vim.keymap.set({"t", "n"}, "<C-Space>", function()
      if vim.api.nvim_win_get_config(0).relative ~= "" then
	require"FTerm".close()
      end
      ctrl_space_function()
    end, opts)
    vim.keymap.set({"t", "n"}, "<C-Z>", require"FTerm".toggle, opts)
  end,
})

-- config for nvim-colorizer.lua
require'colorizer'.setup{}

-- completion config
vim.opt.completeopt:append("noinsert")
vim.opt.completeopt:append("menuone")
vim.opt.completeopt:append("noselect")
vim.opt.completeopt:remove("preview")

-- Set up nvim-cmp.
local has_words_before = function()
  unpack = unpack or table.unpack
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local cmp = require'cmp'

-- helper function to generate a key mapping function for the tab key with cmp
local function tab(advance_selection)
  return function(fallback)
    if cmp.visible() then
      if #cmp.get_entries() == 1 then
	cmp.confirm({ select = true })
      else
	advance_selection()
      end
      --[[ Replace with your snippet engine (see above sections on this page)
      elseif snippy.can_expand_or_advance() then
	snippy.expand_or_advance() ]]
    elseif has_words_before() then
      cmp.complete()
      if #cmp.get_entries() == 1 then
	cmp.confirm({ select = true })
      end
    else
      fallback()
    end
  end
end

local in_home = vim.fn.getcwd() == vim.fn.expand("~")
cmp.setup({
  view = {
    -- can be "custom", "wildmenu" or "native"
    entries = {name = "custom", selection_order = 'near_cursor'},
    docs = { auto_open = true },
  },
  snippet = {
    expand = function(args) vim.fn["UltiSnips#Anon"](args.body) end,
  },
  window = {
    completion = nil,    --cmp.config.window.bordered(),
    documentation = nil, --cmp.config.window.bordered(),
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    --['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = cmp.mapping.confirm({ select = false }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.

    ['<Tab>'] = cmp.mapping(tab(cmp.select_next_item), { "i", "s" }),
    ['<S-Tab>'] = cmp.mapping(tab(cmp.select_prev_item), { "i", "s" }),
  }),
  sources = cmp.config.sources({
      { name = 'nvim_lsp_signature_help' },
      { name = 'nvim_lsp' },
      { name = 'ultisnips' },
      { name = "nvim_lua" },
      { name = 'treesitter' },
      { name = 'buffer' },
    }, {
      { name = 'rg',
	-- The rg source searches from the current directory and that is to much
	-- work in folders like $HOME.
	keyword_length = in_home and 5 or 3,
	option = {
	  additional_arguments = in_home and "--max-depth 3 --hidden" or "--hidden",
	},
      }
    })
})

-- Set configuration for specific filetype.
cmp.setup.filetype('gitcommit', {
  sources = cmp.config.sources({ { name = 'git' } }, { { name = 'buffer' } })
})

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline({ '/', '?' }, {
  mapping = cmp.mapping.preset.cmdline(),
  sources = {
    { name = 'buffer' }
  }
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
      { name = 'path' },
      { name = 'cmdline' }
    }, {
      { name = 'cmdline_history' },
  })
})

require("cmp_git").setup()

-- Colors for the cmp completion menu
-- gray
vim.api.nvim_set_hl(0, 'CmpItemAbbrDeprecated', { bg='NONE', strikethrough=true, fg='#808080' })
-- blue
vim.api.nvim_set_hl(0, 'CmpItemAbbrMatch', { bg='NONE', fg='#569CD6' })
vim.api.nvim_set_hl(0, 'CmpItemAbbrMatchFuzzy', { link='CmpIntemAbbrMatch' })
-- light blue
vim.api.nvim_set_hl(0, 'CmpItemKindVariable', { bg='NONE', fg='#9CDCFE' })
vim.api.nvim_set_hl(0, 'CmpItemKindInterface', { link='CmpItemKindVariable' })
vim.api.nvim_set_hl(0, 'CmpItemKindText', { link='CmpItemKindVariable' })
-- pink
vim.api.nvim_set_hl(0, 'CmpItemKindFunction', { bg='NONE', fg='#C586C0' })
vim.api.nvim_set_hl(0, 'CmpItemKindMethod', { link='CmpItemKindFunction' })
-- front
vim.api.nvim_set_hl(0, 'CmpItemKindKeyword', { bg='NONE', fg='#D4D4D4' })
vim.api.nvim_set_hl(0, 'CmpItemKindProperty', { link='CmpItemKindKeyword' })
vim.api.nvim_set_hl(0, 'CmpItemKindUnit', { link='CmpItemKindKeyword' })

--guifg=#839496

-- setup markdown folding plugin
vim.g.markdown_fold_style = 'nested'

-- php stuff
-- taken from http://stackoverflow.com/a/7490288
-- Set PHP folding of classes and functions.
vim.g.php_folding = 0

require("nvim-autopairs").setup {}
