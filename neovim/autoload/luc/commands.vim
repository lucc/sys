" Autoload functions for commands that I defined myself

" From the example vimrc file.
function! luc#commands#DiffOrig()
  vertical new
  set buftype=nofile
  read ++edit #
  0delete _
  diffthis
  wincmd p
  diffthis
endfunction

function! luc#commands#ssh(args)
  if empty(a:args)
    echoerr "You must give a remote path to edit."
    return 1
  endif
  let [host; path] = split(a:args, '/')
  execute 'edit scp://' . host . '//' . join(path, '/')
endfunction
