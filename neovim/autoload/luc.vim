function! luc#time(cmd1, cmd2, count) abort
  " execute two ex commands count times each and print the duration
  let header = [
	\ 'let s:time1 = reltime()',
	\ 'for i in range('.a:count.')'
	\ ]
  let footer = [
	\ 'endfor',
	\ 'let s:time2 = reltime(s:time1)'
	\ ]
  let length = min([max([len(a:cmd1), len(a:cmd2)]) + 5, &columns - 20])
  let fmt = '%-'.length.'.'.length.'s'
  let echostr = 'echo %s." -> ".reltimestr(s:time2)." sec"'
  let echo1 = printf(echostr, string(printf(fmt, a:cmd1)))
  let echo2 = printf(echostr, string(printf(fmt, a:cmd2)))
  echo 'Running' a:count 'repetitions of ...'
  let file = tempname()
  call writefile(header+[a:cmd1]+footer+[echo1], file)
  execute 'source' file
  call writefile(header+[a:cmd2]+footer+[echo2], file)
  execute 'source' file
  call delete(file)
endfunction

function! luc#capitalize(text) abort
  return substitute(a:text, '\v<(\w)(\w*)>', '\u\1\L\2', 'g')
endfunction

function! luc#capitalize_operator_function(type) abort
  " This does not work in block moode
  ""%s/\%'<\_.*\%'>/\=substitute(submatch(0), '\v<(\w)(\w*)>', '\u\1\L\2', 'g')/
  " this function is partly copied from the vim help about g@
  let sel_save = &selection
  let saved_register = @@
  let &selection = "inclusive"
  if a:type == 'line'
    silent execute "normal! '[V']y"
  elseif a:type == 'block'
    silent execute "normal! `[\<C-V>`]y"
  else
    silent execute "normal! `[v`]y"
  endif
  let @@ = luc#capitalize(@@)
  normal gvp
  let &selection = sel_save
  let @@ = saved_register
endfunction

function! luc#get_visual_selection() abort
  " see:
  "http://vim.wikia.com/wiki/Making_Parenthesis_And_Brackets_Handling_Easier
  let saved_register = @@
  let current = getpos('.')
  call setpos('.', getpos("'<"))
  execute 'normal' visualmode()
  call setpos('.', getpos("'>"))
  normal y
  let return_value = @@
  let @@ = saved_register
  call setpos('.', current)
  return return_value
endfunction

" Prefix a selection or motion with the &commentstring
" This function can be called with visualmode() as argument or as an operator
" function.
function! luc#prefix(type) range abort
  if a:type == ""
    let first = line("'<")
    let last = line("'>")
    let minindent = min([getpos("'<")[2], getpos("'>")[2]]) - 1
  else
    if a:type == 'line' || a:type == 'char'
      let first = line("'[")
      let last = line("']")
    elseif a:type == 'visual' || a:type == 'v' || a:type == 'V'
      let first = line("'<")
      let last = line("'>")
    else
      echoerr 'Wrong argument:' a:type
    endif
    let minindent = min(map(range(first, last), 'indent(v:val)'))
  endif
  let range = first . ',' . last
  let regex = '\v(.{' . minindent . '})(.*)'
  let fmt = escape('%s' . &commentstring, '"\')
  let subst = '\=printf("' . fmt . '", submatch(1), submatch(2))'
  execute range . 's/' . regex . '/' . subst . '/'
endfunction
