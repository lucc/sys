if exists("current_compiler")
  finish
endif
let current_compiler = 'mypy'

CompilerSet makeprg=mypy
CompilerSet errorformat=\%f:\%l:\ \%m
