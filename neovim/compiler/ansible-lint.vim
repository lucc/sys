if exists("current_compiler")
  finish
endif
let current_compiler = 'ansible-lint'

CompilerSet makeprg=ansible-lint\ --parseable
CompilerSet errorformat=\%f:\%l:\ \%m
