if exists("current_compiler")
  finish
endif
let current_compiler = 'latexrun'

CompilerSet makeprg=latexrun
CompilerSet errorformat=\%f:\%l:\ \%m
