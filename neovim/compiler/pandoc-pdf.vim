if exists("current_compiler")
    finish
endif
let current_compiler = 'pandoc-pdf'

CompilerSet makeprg=pandoc\ %\ -o\ %:r.pdf\ -t\ latex
" TODO is the errorformat correct for latex?
CompilerSet errorformat="%f",\ line\ %l:\ %m
