if exists("current_compiler")
    finish
endif
let current_compiler = 'pandoc'

CompilerSet makeprg=pandoc\ %\ -o\ %:r.html
CompilerSet errorformat="%f",\ line\ %l:\ %m
