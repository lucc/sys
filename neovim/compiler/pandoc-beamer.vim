if exists("current_compiler")
    finish
endif
let current_compiler = 'pandoc-beamer'

CompilerSet makeprg=pandoc\ %\ -o\ %:r.pdf\ -t\ beamer
" TODO is the errorformat correct for latex?
CompilerSet errorformat="%f",\ line\ %l:\ %m
