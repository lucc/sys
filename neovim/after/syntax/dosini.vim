" a syntax folding to dosini files
syn region dosiniSection start="\s*\[.*\]" end="\ze\s*\[.*\]" fold contains=dosiniLabel,dosiniValue,dosiniNumber,dosiniHeader,dosiniComment
