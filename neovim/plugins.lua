local packer = require("packer")
local util = require("packer.util")
local env = os.getenv

packer.startup {
  function(use)

    use { env'iron',
      cmd = {"IronRepl", "IronLoadFile"},
      setup = function()
	vim.g.iron_repl_open_cmd = 'vsplit'
      end,
      config = function()
	require("iron.core").setup {
	  config = {
	    repl_definition = {
	      -- forcing a default
	      prolog = require("iron.fts.prolog").swipl,
	      python = require("iron.fts.python").ipython,
	      -- define a new repl
	      nix = { command = {"nix", "repl"}},
	      php = { command = {"psysh"}},
	    },
	  },
	}
	-- load the current file in the REPL.
	vim.api.nvim_create_user_command('IronLoadFile', function()
	  local ft = vim.bo.filetype
	  local load
	  if ft == 'haskell' then
	    load = ':load ' .. vim.fn.bufname('%')
	  elseif ft == 'prolog' then
	    load = '["' .. vim.fn.bufname('%') .. '"].'
	  else
	    error("Don't know how to load files for " .. ft)
	  end
	  require("iron").core.send(ft, load)
	end, { desc = "load the current file in the repl" })
      end,
    }

    -- async make
    use { env'vim-dispatch',
      cmd = { "Make" },
      setup = function()
	vim.g.dispatch_no_maps = 1
	vim.g.dispatch_no_tmux_make = 1
      end,
    }
    use { env'vim-test',
      cmd = { "TestFile", "TestSuite", "TestNearest" },
      setup = function()
	vim.g["test#strategy"] = 'neovim'
	vim.g["test#neovim#term_position"] = "vertical"
      end,
    }

    -- debugging
    use { env'vdebug',
      cmd = {'VdebugStart'},
      setup = function()
	vim.g.vdebug_options = {
	  break_on_open = 0,
	  continuous_mode = 1,
	  watch_window_style = 'compact',
	  window_commands = {
	    DebuggerWatch = 'vertical belowright new',
	    DebuggerStack = 'belowright new +res5',
	    DebuggerStatus = 'belowright new +res5'
	  },
	  --debug_window_level = 2,
	}
      end,
    }

    use { env'vim-flog', cmd = { "Flog", "Flogsplit" } } -- git history browser
    use { env'git-messenger-vim', cmd = { "GitMessenger" } } -- float win with last commit

    use { env'vim-hoogle', cmd = { "Hoogle" } }
  end,

  config = {
    compile_path = util.join_paths(env("out"), "plugin", "packer_compiled.lua"),
    package_root = util.join_paths(env("out"), "pack"),
  },
}

packer.sync()
packer.compile()
