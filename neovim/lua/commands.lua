local utils = require("utils")

--- Open a file with xdg-open in the background
---
--- @param filename string the file to open
local function open_file_with_xdg(filename)
  if not vim.fn.filereadable(filename) and not filename:match("^https?://") then
    filename = filename:gsub("\n*$", "")
  end
  local id = vim.fn.jobstart(
    { "xdg-open", filename },
    {
      detach = true,
      stdin = "null"
    }
  )
  if id == 0 then
    error("Can not open file: Invalid arguments to jobstart.")
  elseif id == -1 then
    error("Can not open file: xdg-open is not executable.")
  end
end

--- Helper function for the :O user command
---
--- @param bang boolean|nil if a ! was given to :O
--- @param args string|nil the file to open
local function open_command(bang, args)
  if bang then
    open_file_with_xdg(vim.fn.expand("%"))
  elseif args then
    open_file_with_xdg(args)
  elseif utils.is_visual_mode() then
    open_file_with_xdg(utils.get_visual_selection())
  else
    open_file_with_xdg(vim.fn.expand("<cfile>"))
  end
end

--- Search for todo markers
---
--- @param grep boolean wether to search with :grep or :vimgrep
local function todo(grep)
  if grep then
    vim.cmd.grep "\\b(todo\\|fixme\\|xxx)\\b"
  else
    vim.cmd.vimgrep '/\\v\\c<(todo|fixme|xxx)>/ %'
  end
end

-- Write the current buffer to its filename with sudo priveledges
-- Use SSH_ASKPASS to query the sudo password
local function sudo_write()
  -- neovim does not provide a terminal for !
  -- We can use SUDO_ASKPASS to still query the password
  local askpass = os.getenv("SSH_ASKPASS")
  if askpass ~= nil and askpass ~= ""  then
    vim.env.SUDO_ASKPASS = askpass
  end
  vim.cmd "silent write ! sudo dd of=%"
  vim.cmd "edit!"
  vim.cmd.redraw()
end

return {
  open = open_command,
  sudo_write = sudo_write,
  todo = todo,
}
