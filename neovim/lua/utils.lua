-- Notes on getting the visual sesection:
-- https://github.com/theHamsta/nvim-treesitter/blob/a5f2970d7af947c066fb65aef2220335008242b7/lua/nvim-treesitter/incremental_selection.lua#L22-L30
-- https://gitlab.com/jrop/dotfiles/-/blob/3a723444ebb3ec2098c7d37ca95605de81b6eded/.config/nvim/lua/my/utils.lua#L13
-- https://github.com/neovim/neovim/discussions/26092


--- Check if neovim is currently in any visual mode
local function is_visual_mode()
  local mode = vim.fn.mode()
  return mode == "v" or mode == "V" or mode == "\22" -- \22 is ctrl-v
end

--- Get the text in the current or last visual selection from neovim
---
--- @return string
local function get_visual_selection()
  local pos = vim.fn.getpos(".")
  local vmode = is_visual_mode()
  local old = vim.fn.getreg("@")
  if vmode then
    vim.cmd("keepjumps normal! ygv")
  else
    vim.cmd("keepjumps normal! gvy")
  end
  local value = vim.fn.getreg("@")
  vim.fn.setreg("@", old)
  vim.fn.setpos(".", pos)
  return value
end

return {
  get_visual_selection = get_visual_selection,
  is_visual_mode = is_visual_mode,
}
