-- user defined maps by lucc

-- Don't use Ex mode, use Q for formatting (from the example file)
vim.keymap.set("n", "Q", "gq")

-- make Y behave like D,S,C ...
vim.keymap.set("n", "Y", "y$")

-- CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
-- so that you can undo CTRL-U after inserting a line break.
vim.keymap.set("i", "<C-U>", "<C-G>u<C-U>")
vim.keymap.set("i", "<C-W>", "<C-G>u<C-W>")

-- easy spell checking
vim.keymap.set("n", "<leader>s", function()
  vim.opt_local.spell = true
  vim.cmd.normal { "]s", bang = true }
  vim.cmd.normal "zv"
end)
vim.keymap.set("n", "<leader>k", "1z=")

-- capitalize text
vim.keymap.set("v", "gc", "\"=luc#capitalize(luc#get_visual_selection())<CR>p")
vim.keymap.set("n", "gc", "<cmd>set operatorfunc=luc#capitalize_operator_function<CR>g@")
vim.keymap.set("n", "gcc", "gciw", { noremap = false })

-- try to make the current window as tall as the content
vim.keymap.set("n", "<C-W>.", function() vim.cmd.resize(vim.api.nvim_buf_line_count(0)) end)

-- prefix lines with &commentstring
vim.keymap.set("v", "<leader>p", function() vim.fn["luc#prefix"](vim.fn.visualmode()) end)
vim.keymap.set("n", "<leader>p", "<cmd>set operatorfunc=luc#prefix<CR>g@")

-- moving around
vim.keymap.set("n", "<C-W><C-F>", "<C-W>f<C-W>L")
vim.keymap.set("n", "<SwipeUp>", "gg")
vim.keymap.set("i", "<SwipeUp>", "gg")
vim.keymap.set("n", "<SwipeDown>", "G")
vim.keymap.set("i", "<SwipeDown>", "G")
vim.keymap.set("n", "g<CR>", "<C-]>")

vim.keymap.set("n", "'", "`")
vim.keymap.set("n", "`", "'")

-- https://github.com/javyliu/javy_vimrc/blob/master/_vimrc
vim.keymap.set("v", "*", 'y/\\V<C-r>=escape(getreg(\'"\'), "/\\\\")<CR><CR>')
vim.keymap.set("v", "#", 'y?<C-r>"<CR>')

-- Use ESC to leave terminal mode
vim.keymap.set("t", "<Esc>", "<C-\\><C-n>")

-- open the file under the cursor with xdg-open
vim.keymap.set({ "n", "v" }, "gF", function() require "commands".open() end)

-- jump to diagnostics
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next)
