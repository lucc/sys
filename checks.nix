{ self, system, ... }:
let
  pkgs = self.inputs.nixpkgs.legacyPackages.${system};
  runHelp = pkg:
    pkgs.runCommandLocal "check-${pkg.name}--help" { HOME = "/build"; }
    "${pkgs.lib.getExe pkg} --help && touch $out";
in {
  afew = runHelp pkgs.afew;
  fetchmail = runHelp pkgs.fetchmail;
}
