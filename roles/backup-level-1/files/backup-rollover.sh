#!/bin/bash

set -e -o pipefail

cd ~/backup

# only loop over latest links that exist
for link in */latest; do
  dir=${link%/latest}
  latest=$(readlink "$link")
  echo Saving "$dir/$latest" to the NAS ...
  rsync -av --link-dest=../latest "$dir/$latest/" "data@nas:backup/$dir/$latest"
  ssh data@nas ln -fnsv "$latest" "backup/$dir/latest"
done

echo Deleting old backups ...
threshold=$(date --date -5days +%Y%m%d000000)
for dir in ~/backup/*; do
  cd "$dir"
  ls -r | awk -v date="$threshold" '
	    /^latest$/ { next } # keep the "latest" symlink
	    NR < 20 { next }    # keep at least 20 backups
	    $0 < date { print } # delete everything older than 10 days
	    ' \
	| xargs -t -r rm -rf
done
