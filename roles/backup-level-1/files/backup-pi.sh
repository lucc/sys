#!/bin/bash

set -e

# save the list of installed packages
pacman -Qteq > /root/packages.txt
pacman -Qii | awk '/^MODIFIED/ {print $2}' > /root/modified.txt

rsync.sh -d data@nas:backup/pi -- -v / \
  --include /etc \
  --include /root \
  --exclude '/*'
