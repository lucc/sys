#!/usr/bin/env python3

import mimetypes
import os
import os.path
import pathlib
import re
import sys
from xml.etree import ElementTree as ET

HTML1 = ('<!DOCTYPE html><html><head><meta charset="UTF-8" />'
         '<meta name="viewport" content="width=device-width" />'
         '<title>Video Player</title><script>{script}</script></head>'
         '<body><div style="text-align: center;">'
         '<video id="v" controls>No video selected.</video></div><div><ul>')
HTML2 = '</ul></div></body></html>'
SCRIPT = ('function start(item) {'
          'var video = document.getElementById("v");'
          'video.src = item.getAttribute("url");'
          'video.play();'
          '}')

def walk(path: str):
    """Generate all file names"""
    for root, dirs, files in os.walk(path):
        dirs.sort()
        for f in sorted(files):
            yield os.path.join(root, f)

def read_metadata(filepath: pathlib.Path):
    """Read the necessary metadata for a video file"""
    year = re.match(r"\d\d\d\d", filepath.name) \
        or re.match(r"\d\d\d\d$", filepath.parent.name)
    if year:
        year = year.group(0)
    return (
        str(filepath),
        filepath.name,
        mimetypes.guess_type(filepath)[0],
        year,
    )

base = pathlib.Path("/data/video")
#base = pathlib.Path("/media/nas/video")  # for local tests

print("Content-type: text/html")
print()
print(HTML1.format(script=SCRIPT), end="")

for f in walk(base):
    f = os.path.relpath(f, base)
    anchor = ET.Element("a", attrib={"href": "#", "onclick":"start(this)",
                                     "url": os.path.join("/video", f)})
    anchor.text = f
    list_item = ET.Element("li")
    list_item.append(anchor)
    ET.ElementTree(list_item).write(sys.stdout, encoding='unicode',
                                    method='html')

print(HTML2, end="")
