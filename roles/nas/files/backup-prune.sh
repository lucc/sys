#!/bin/sh

set -e

cd ~data/backup/yoga-full

# Only delete in_progress backups that are older than the youngest successful
# backup.
ls -r \
  | sed -n '/^[0-9]\{14\}$/,$ { /\.in_progress$/p }' \
  | xargs -t -r rm -rf
