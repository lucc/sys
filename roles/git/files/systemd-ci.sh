#!/bin/sh

start_ci () {
  export GIT_DIR=$(pwd)
  export GIT_WORK_TREE=$(mktemp -d)
  git checkout --quiet --force "$1"
  cd "$GIT_WORK_TREE"
  if [ -f makefile ]; then
    systemd-run --same-dir make ci \; rm -rf "$GIT_WORK_TREE"
  fi
}

for head; do
  if [ "$head" = master ]; then start_ci "$head"; fi
done
