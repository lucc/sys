"""A small dropbox like page"""

import argparse
import cgitb
import cgi
import contextlib
import enum
import hashlib
import pathlib
import sqlite3
from typing import Optional

basedir = pathlib.Path(__file__).parent
db = sqlite3.connect(basedir / "db.sqlite")
c = db.cursor()


def init_db() -> None:
    c.execute("CREATE TABLE dirs (uid text, pass text)")


def check(uid: str, password: Optional[str]) -> Optional[bool]:
    """Check if an uid may be used with the given password or without one

    :param uid: the uid/folder
    :param password: the password
    :returns: None if the uid is invalid, True if the uid may, False if it may
        not be used with the given password
    """
    if not uid:
        return None
    if not password:
        c.execute("SELECT * FROM dirs WHERE uid = ? AND pass = ''", (uid,))
        return c.rowcount == 0
    # TODO insecure hash
    hashed = hashlib.sha512(password.encode()).hexdigest()
    c.execute("SELECT * FROM dirs WHERE uid = ? AND pass = ?", (uid, hashed))
    item = c.fetchone()
    if not item:
        return False
    return item["pass"] == hashed or not item["pass"]


def list_files(uid: str) -> None:
    for entry in (basedir/uid).rglob("*"):
        print(entry)


@contextlib.contextmanager
def html(title: str = "Mini Dropbox"):
    print("Content-type:text/html\r\n\r\n")
    print("<!DOCTYPE html>")
    print("<html>")
    print("<head>")

    print("</head>")
    print("<body>")
    try:
        yield
    finally:
        print("</body>")
        print("</html>")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    cmd = parser.add_subparsers(dest="command")
    cmd.add_parser("list")
    cmd.add_parser("rm")
    add = cmd.add_parser("add")
    args = parser.parse_args()

    if args.command == "list":
        pass
    elif args.command == "rm":
        pass
    elif args.command == "add":
        pass
    else:
        cgitb.enable()

        form = cgi.FieldStorage()
        uid = form.getvalue("uid", default=None)
        if not uid:
            with html():
                print("<h1>Mini Dropbox</h1>")
                print("<p>You did not specify a folder</p>")
        else:
            checked = check(uid, form.getvalue("pass"))
            if checked is None:
                with html():
                    print("INVALID")
            elif checked:
                pass
            else:
                with html():
                    print("<form><input type='password' name='pass'></form>")

