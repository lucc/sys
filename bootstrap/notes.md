% Notes for installing new systems
% lucc
% 2016-12 / 2019-12

# BIOS

- read all possible options
- on the x220 there was a setting to prefer BIOS over UEFI boot which lead me
  to reinstall Linux three times and going crazy about boot loader installation.

# Linux installation

- scripts in the ./bootstrap directory
- installation guide from the Arch wiki
- guide for full disk encryption from the Arch wiki
- /etc/mkinitcpio.conf: added HOOKS for lvm on luks
- lvm on luks page on the Arch wiki
- resulting partitions on X220 with Arch Linux:

	  NAME               PARTLABEL  LABEL   FSTYPE        SIZE MOUNTPOINT
	  sda                                               298.1G
	  ├─sda2             crypt.part         crypto_LUKS 297.9G
	  │ └─decrypt                           LVM2_member 297.9G
	  │   ├─decrypt-swap                    swap           16G [SWAP]
	  │   ├─decrypt-root            root.fs ext4           20G /
	  │   └─decrypt-home            home.fs ext4          240G /home
	  └─sda1             boot.part          vfat          200M /boot

- partitions on yoga with NixOS:

	  NAME                   PARTLABEL  LABEL FSTYPE        SIZE MOUNTPOINT
	  sda                                                 238.5G
	  ├─sda1                 boot.part        vfat          512M /boot
	  └─sda2                 crypt.part       crypto_LUKS   238G
	    └─decrypt                             LVM2_member   238G
	      ├─yoga.intern-swap                  swap           16G [SWAP]
	      └─yoga.intern-root                  xfs           222G /

# Post install setup

- install software, package lists are included in the backups
* install autologin, keymap, console font ...
- copy config from backup or github or what not:

	  mv restore/ubuntu-home/src .
	  mv restore/ubuntu-home/.config/{fetchmail,pass,git,alot,kha*,msmtp,notmuch,nvim,ssh,systemd,tmux,vdirsyncer,vimperator} .config/
	  mv restore/ubuntu-home/mail .
	  mv restore/ubuntu-home/.gnupg .config/gpg
	  mv restore/ubuntu-home/from-bk/.config/awesome .config
	  mv restore/ubuntu-home/from-bk/.config/xinit .config
	  mv restore/ubuntu-home/from-bk/.config/mozilla/ .config

- install nvim plugins
* start fetchmail service
* start ssh service
* alot depends on the mailcap file
* the mailcap file depends on the mailcap.sh script
