#!/bin/sh

# Shell script to write an operating system for my raspberry to an SD card.
# See https://archlinuxarm.org/platforms/armv6/raspberry-pi#installation

set -e

tar=ArchLinuxARM-rpi-latest.tar.gz
fetch=false
dev=

help_function () {
  echo "Usage: ${0##*/} [-f | -i tar-file] -d device"
  echo "       ${0##*/} -h"
  echo
  echo Options:
  echo "-i  specify an alternate tar ball (default=$tar)"
  echo "-f  fetch the tar ball from os.archlinuxarm.org"
}

while getopts d:fhi: FLAG; do
  case $FLAG in
    h) help_function; exit;;
    i) tar=$OPTARG;;
    d) dev=$OPTARG;;
    f) fetch=true;;
    *) help_function; exit 2;;
  esac
done

# download the operating system tar ball
if $fetch; then
  url=http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-latest.tar.gz
  curl --location --remote-name-all "$url" "$url.md5"
  md5sum --check < "${url##*/}.md5"
fi

if ! [ -e "$tar" ]; then
  echo The operating system tar ball does not exist. >&2
  exit 1
fi

if [ -z "$dev" ]; then
  echo We need a device >&2
  exit 1
fi

# partition the device
# 200MB for  /boot
# the rest for /
parted --script -- "$dev" \
  mklabel msdos \
  mkpart primary fat32 2048s 200.0 \
  mkpart primary ext2 200.0 -1s

# format the partitions
mkfs.vfat "$dev"1
mkfs.ext4 "$dev"2

# create a tmp folder for mounting
tmp=$(mktemp -d)
trap 'umount "$tmp/boot"; umount "$tmp"; rmdir "$tmp"' INT TERM QUIT

# mount the partitions
mount "$dev"2 "$tmp"
mkdir "$tmp/boot"
mount "$dev"1 "$tmp/boot"

# extract the operating system
tar -xpf "$tar" -C "$tmp"

# unmount partitions
umount "$tmp/boot"
umount "$tmp"

# clean up tmp folder
rmdir "$tmp"
