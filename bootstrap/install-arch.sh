#!/bin/sh

# This script gives the commands advertised in the Arch Wiki for system
# installation: https://wiki.archlinux.org/index.php/Installation_guide
# Further interesting pages for Arch Linux system installation:
# https://wiki.archlinux.org/index.php/Category:Boot_loaders
# https://wiki.archlinux.org/index.php/Network_configuration
# https://wiki.archlinux.org/index.php/Disk_encryption

set -e

echo Setting keyboard layout to de.
loadkeys de

echo Checking internet connection ...
if ! timeout 1s ping -c 1 -W 1 -q archlinux.org >/dev/null 2>&1; then
  echo No connection available.  Please select a wireless network.
  wifi-menu
fi

echo Updating system clock.
timedatectl set-ntp true

echo Setting the hardware clock.
hwclock --systohc

cat <<EOF
Please use the script ./setup-full-disk-encryption.sh to partition your disk.
Entering subshell, exit to continue.
EOF
bash

# Proboably some kind of swap device is now available
swapon

echo Installing the base system.
# Further ideas: netctl intel-ucode grub refind-efi (for mac)
pacstrap /mnt base linux linux-firmware dialog wpa_supplicant neovim zsh

echo Generating fstab.
genfstab -U /mnt >> /mnt/etc/fstab

echo Linking timezone.
ln -s /usr/share/zoneinfo/Europe/Berlin /mnt/etc/localtime

echo Enableling english locale.
sed -i '/^#en_US.UTF-8 UTF-8/s/^#//' /mnt/etc/locale.gen

echo Setting system locale to english.
echo LANG=en_US.UTF-8 > /mnt/etc/locale.conf

echo Setting keyboard layout in the console to de-latin1.
echo KEYMAP=de-latin1 > /mnt/etc/vconsole.conf

echo Generate locales.
arch-chroot /mnt locale-gen

echo Please enter a host name for the new system:
head -n 1 > /mnt/etc/hostname

echo Set root password.
arch-chroot /mnt passwd

echo Configure the boot image.
$EDITOR /mnt/etc/mkinitcpio.conf

echo Generate boot image.
arch-chroot /mnt mkinitcpio -p linux

cat <<EOF
Please install and enable a bootloader:
systemd-boot can be enabled via "bootctl install", it has already been
installed with systemd.  You might need to create a loader config file in
/boot/loader/entries/*.conf, for example

  title   Arch Linux
  linux   /vmlinuz-linux
  initrd  /intel-ucode.img
  initrd  /initramfs-linux.img
  options cryptdevice=/dev/disk/by-partlabel/crypt.part:decrypt root=/dev/mapper/decrypt-root quiet rw resume=/dev/mapper/decrypt-swap

Entering subshell, exit to continue.
EOF
arch-chroot /mnt

echo System installation complete, you can now reboot.
