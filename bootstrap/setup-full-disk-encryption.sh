#!/bin/sh

set -e

usage () {
  echo "Usage ${0##*/} -d DEVICE -b SIZE -s SIZE"
  echo "      ${0##*/} -h"
}
help () {
  cat <<EOF
This script will format your hard drive and set up a partition layout for full
disk encryption like this:
NAME                   FSTYPE      MOUNTPOINT PARTLABEL
sda
├─sda1                 vfat        /boot      boot.part
└─sda2                 crypto_LUKS            crypt.part
  └─decrypt            LVM2_member
    ├─intern-swap      swap        [SWAP]
    └─intern-root      ext4        /

Available options

-d DEVICE  the block device to format; ALL DATA ON THIS DEVICE WILL BE LOST
-b SIZE    the size of the /boot partition
-s SIZE    the size of the swap partition
EOF
}

device=
boot=
swap=
while getopts hb:d:s: FLAG; do
  case $FLAG in
    h) usage; help; exit;;
    b) boot=$OPTARG;;
    d) device=$OPTARG;;
    s) swap=$OPTARG;;
    *) usage; exit 2;;
  esac
done
if [ -z "$device" ] || [ -z "$boot" ]; then usage; exit 2; fi

echo "Partitioning $device ..."
parted --script "$device" mklabel gpt
parted --script "$device" mkpart primary fat32 0 "$boot"
parted --script "$device" name 1 boot.part
parted --script "$device" set 1 esp on
# TODO find the right syntax for this
parted --script "$device" mkpart primary "$boot" 100%
parted --script "$device" name 2 crypt.part
parted --script "$device" print

# wait for the changes to be visible
sync

echo Creating LUKS container ...
cryptdev=/dev/disk/by-partlabel/crypt.part
cryptname=decrypt
group=intern
cryptsetup --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 1000 luksFormat "$cryptdev"
echo Opening encrypted block device ...
cryptsetup open --type luks "$cryptdev" "$cryptname"

echo Creating LVM setup inside the encrypted device ...
pvcreate /dev/mapper/"$cryptname"
vgcreate "$group" /dev/mapper/"$cryptname"
lvcreate --size "$swap" "$group" --name swap
lvcreate --extents 100%FREE "$group" --name root

echo Creating swap device ...
mkswap /dev/"$group"/swap

echo Formatting root partition ...
mkfs --type ext4 /dev/"$group"/root

echo Formatting boot partition ...
mkfs --type vfat /dev/disk/by-partlabel/boot.part

echo Mounting onto /mnt ...
mount /dev/"$group"/root /mnt
mkdir /mnt/boot
mount /dev/disk/by-partlabel/boot.part /mnt/boot

# wait for the changes to be visible and show the result
sync
lsblk --output NAME,FSTYPE,MOUNTPOINT,PARTLABEL,SIZE,RO
