- name: Assert the relevant variables
  hosts: localhost
  tags: assert
  tasks:
    - name: Assert that the ip of the raspberry is defined
      assert:
        that:
          - ip is defined
        fail_msg: You need to set the ip of the raspberry.
    - name: Assert that the password for the main user is defined
      assert:
        that:
          - user_password is defined
        fail_msg: You need to set user_password to run this playbook.
    - name: Define the host that we are currently bootstraping
      add_host:
        name: currently_bootstraping
        ansible_host: "{{ ip }}"

- name: Bootstrap and initial setup for the rasperry pi
  hosts: currently_bootstraping
  user: alarm
  become: true
  become_method: su
  gather_facts: false
  tags: setup
  vars:
    # Well known credentials from the default image
    ansible_password: alarm
    ansible_become_pass: root
  tasks:
    - name: Bootstrap pacman and python on the rasperry pi
      # inetutils is needed for the hostname task
      raw: >
        pacman-key --init
        && pacman-key --populate archlinuxarm
        && pacman -Syu --noconfirm python inetutils
    - name: Gather facts after installing python
      setup:
    - name: Set the hostname
      hostname:
        name: pi
        use: systemd   # only with ansible >= 2.9
    - name: Install sudo
      package:
        name: sudo
    - name: Add the main user
      user:
        name: luc
        state: present
        password: "{{ user_password | password_hash('sha512', 'ebc3262920b90367') }}"
        generate_ssh_key: true
        ssh_key_comment: luc@pi
      when: user_password is defined
    - name: Add a key to the authorized keys of the default user
      ansible.posix.authorized_key:
        user: luc
        key: "{{ lookup('file', '~/.ssh/id_rsa.pub') }}"
    - name: Add the main user to the sudoers file
      lineinfile:
        line: luc ALL=(ALL) ALL
        state: present
        path: /etc/sudoers
        validate: visudo -cf %s
    - name: Lock the root user
      user:
        name: root
        password: "!*"

- name: Clean up some old stuff
  hosts: currently_bootstraping
  user: luc
  become: true
  become_method: sudo
  tags: cleanup
  vars:
    ansible_password: "{{ user_password }}"
    ansible_become_pass: "{{ user_password }}"
  tasks:
    - name: Remove the initial alaram user
      user:
        name: alarm
        state: absent
        force: true
    - name: Remove some preinstalled software
      package:
        name:
          - nano
        state: absent
