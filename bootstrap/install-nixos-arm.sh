#!/bin/sh

set -e

image=sd-image-armv6l-linux.img.xz

help_function () {
  echo "Usage: ${0##*/} [-i image-file] -d device"
  echo "       ${0##*/} -h"
  echo
  echo Options:
  echo "-i  specify an alternate image file (default=$image)"
}

while getopts d:hi: FLAG; do
  case $FLAG in
    h) help_function; exit;;
    i) image=$OPTARG;;
    d) dev=$OPTARG;;
    *) help_function; exit 2;;
  esac
done

if [ -z "$dev" ]; then
  echo We need a device >&2
  exit 1
fi

# Write the image to the sd card
case "$image" in
  *.bz2) bzip2 -d;;
  *.gz) gzip -d;;
  *.img) cat;;
  *.xz) xz -d;;
esac \
  < "$image" \
  | dd of="$dev" status=progress
