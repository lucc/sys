#!/bin/sh
if [ $# -ne 1 ]; then
  echo "Give a key from the nvram xml output." >&2
  exit 2
fi
nvram -xp | \
  sed -n '/<key>'"$1"'<\/key>/,/<\/data>/{s/^[[:space:]]*//;/^</d;p;}' | \
  openssl base64 -d
