#!/bin/sh
set -e
set -x
mount -t msdos /dev/disk0s1 /mnt
bless --setBoot --folder /mnt/EFI/refind --file /mnt/EFI/refind/refind_x64.efi
