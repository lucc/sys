#!/bin/sh

set -e

ID=
. /etc/os-release

case $ID in
  arch) pacman -Syu ansible;;
  nixos) nix-shell -p ansible;;
  *) echo Not implemented >&2; exit 1;;
esac
